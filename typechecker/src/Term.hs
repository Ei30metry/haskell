-- | Syntax of Source Terms

module Term (
  -- Term
  Term(..,TVar,TAnn,TWildCard), -- Syntax of "Haskell"
  Typ,                          -- A Term that represents a user written type
  Expr,                         -- A Term that represents a user written expression
  Pat,                          -- A Term that represents a user written pattern
  pprTerm ,app0,

  -- Application head
  Head(..), pprHead,

  -- Variables and Named WildCards
  Var(..) ,mkVar ,mkNamedWC ,unVar ,pprVar,
  isNamedWC ,isVarHead,

  -- Literals
  Lit(..),

  -- Match expressions
  Match(..) ,unMatch ,matchLhs,

  -- Arguments
  Arg(..),
  APat, -- An argument that appears in a pattern
  actualArg,
  isVisArg ,isInvisArg, isInvisWildCardArg,
  pprArg ,pprArgs,

  -- Quantifier sort and forall-bound variables in a term
  QuantifierFlag(..), Binder(..),

  -- Constraint in a Typ
  TConstraint(..),

  -- Constructor Likes
  ConLike(..),
  TTyCon,   -- A ConLike that represents a user written type constructor
  TDataCon, -- A ConLike that represents a user written data constructor
  TPatSyn,  -- A ConLike that represents a user written pattern synonym
  TClass,   -- A ConLike that represents a user writen  class
  pprConLike,

  -- Signatures
  Sig(..) ,unSig,

  -- Value declarations
  ValDecls(..),

  -- Value bindings
  ValBind(..)
  ) where

import Language.MicroHs.Ident
import Prettyprinter

-- User written type
-- Types are parsed as terms; the typechecker rejects
-- stuff that doesn't look like a type
--    e.g.   f :: do { ... }
type Typ  = Term

-- User written expression
type Expr = Term

-- User written pattern
-- Patterns are parsed as terms; the typechecker rejects
-- stuff that doesn't look like a pattern
--    e.g.   f (do { ...}) = 87
type Pat  = Term

-- A Term, or the syntax of source "Haskell"
data Term
  = TSuperLambda [Match]
  | TApp Head [Arg]
  | TCase Expr [Match]
  | TLet ValDecls Expr
  | TArrow Term Term
  | TFatArrow Head [Arg] Typ
  | TForall QuantifierFlag Binder Typ
  | TAs Var Pat
  | TIf Term Term Term
  | TLit Lit
  deriving (Show, Eq)

data QuantifierFlag
  = QRequired
  | QSpecified
  deriving (Show, Eq)

-- Forall-bound quantifiee
data Binder
  = UnAnn Var    -- A forall-bound variable
  | Ann Var Term -- A forall-bound variable with a kind
  deriving (Show, Eq)

-- Literals
data Lit
  = LitChar   -- Character literal
  | LitInt    -- Int literal
  | LitString -- String literal
  deriving (Show, Eq)

data Var = MkVar Ident     -- A varible
         | MkNamedWC Ident -- A variable that starts with an underscore
  deriving (Show, Eq, Ord)

pattern TVar :: Var -> Term
pattern TVar x = TApp (HVar x) []

pattern TAnn :: Term -> Typ -> Term
pattern TAnn x ty = TApp (HAnn x ty) []

pattern TWildCard :: Term
pattern TWildCard = TApp HWildCard []

-- An application head
data Head
  = HVar Var          -- Variable
  | HConLike ConLike  -- ConLike
  | HTerm Term        -- not an application
  | HWildCard         -- WildCard
  | HAnn Term Term    -- Annotated Term
  deriving (Show, Eq)

data TConstraint =
  MKTConstraint Head [Arg]
  deriving (Show, Eq)

-- Value declarations
data ValDecls = MkValDecls [Sig] [ValBind]
  deriving (Show, Eq)

-- Value binding
data ValBind
  = FunBind Var [Match] -- Function binding
  | PatBind Pat Expr    -- Pattern binding
  deriving (Show, Eq)

-- TODO: Ask Simon and Richard. Should I turn this into a record?
-- Match expression
data Match =
  MkMatch
  [APat] -- Left hand side
  Expr   -- Right hand side
  deriving (Show, Eq)

-- TODO: Ask Simon and Richard. Should I turn this into a record?
-- Type signature
data Sig =
  MkSig Var Typ
  deriving (Eq, Show)

-- An argument that appears on the LHS of a match expression
type APat = Arg

-- Argument
data Arg
  = VisArg Term    -- Visible argument: x, 5 2, Int
  | InvisArg Term  -- Invisible argumen: @x, @Int, @(Maybe a)
  deriving (Show, Eq)

actualArg :: Arg -> Term
actualArg (VisArg term) = term
actualArg (InvisArg term) = term

unMatch :: Match -> ([APat], Expr)
unMatch (MkMatch aps e) = (aps, e)

isNamedWC :: Var -> Bool
isNamedWC (MkNamedWC _) = True
isNamedWC _             = False

isVarHead :: Head -> Bool
isVarHead (HVar _) = True
isVarHead _        = False

isInvisWildCardArg :: Arg -> Bool
isInvisWildCardArg (InvisArg TWildCard) = True
isInvisWildCardArg _                    = False

isVisArg :: Arg -> Bool
isVisArg (VisArg _) = True
isVisArg _          = False

isInvisArg :: Arg -> Bool
isInvisArg = not . isVisArg

app0 :: Head -> Term
app0 h = TApp h []

matchLhs :: Match -> [Pat]
matchLhs (MkMatch aps _) = foldr step [] aps
  where
    step (VisArg te) xs = te : xs
    step _ xs           = xs

mkVar :: String -> Var
mkVar = MkVar . mkIdent

mkNamedWC :: String -> Var
mkNamedWC = MkNamedWC . mkIdent

unVar :: Var -> Ident
unVar (MkVar i)     = i
unVar (MkNamedWC i) = i

unSig :: Sig -> (Var, Typ)
unSig (MkSig v typ) = (v, typ)

pprVar :: forall a. Var -> Doc a
pprVar = pretty . unIdent . unVar

pprHead :: forall a. Head -> Doc a
pprHead = pretty . show -- TODO: Fix me

pprArg :: forall a. Arg -> Doc a
pprArg = pretty . show -- TODO: Fix me

pprArgs :: forall a. [Arg] -> Doc a
pprArgs = pretty . show -- TODO: Fix me

pprTerm :: forall a. Term -> Doc a
pprTerm = pretty . show -- TODO: Fix me

-- A capitalized identifier: Contructor Like
newtype ConLike =
  MkConLike Ident
  deriving (Show, Eq)

-- A ConLike that represents a user written type constructor
type TTyCon   = ConLike
-- A ConLike that represents a user written data constructor
type TDataCon = ConLike
-- A ConLike that represents a user written pattern synonym
type TPatSyn  = ConLike
-- A ConLike that represents a user writen  class
type TClass   = ConLike

pprConLike :: forall a. ConLike -> Doc a
pprConLike (MkConLike cl) = pretty (unIdent cl)
