-- | Basic types that are used everywhere

module BasicTypes
  (Unique -- See Note [Unique]
  ,pprUnique
  ,getKey
  ,Uniquable(..)
  ,unUnique
  ,mkUnique)
where

import Prettyprinter

-- TODO: Note [Unique]

newtype Unique =
  MkUnique Int
  deriving (Show, Eq, Ord, Num)

unUnique :: Unique -> Int
unUnique (MkUnique n) = n

mkUnique :: Int -> Unique
mkUnique = MkUnique

pprUnique :: forall a. Unique -> Doc a
pprUnique (MkUnique i) = pretty i

class Uniquable a where
  getUnique :: a -> Unique

getKey :: Uniquable a => a -> Int
getKey = unUnique . getUnique

instance Uniquable Unique where
  getUnique = id
