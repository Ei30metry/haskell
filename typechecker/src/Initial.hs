-- | Entities that are present in the environement from the beginning

module Initial (
  -- The Gamma we start with
  initialGamma
  ) where

import {-# SOURCE #-} Tc.Types
import {-# SOURCE #-} Tc.Monad
import Term
import Type
import Language.MicroHs.Ident

initialGamma :: Tc Gamma
initialGamma = do
  vs        <- initialVars
  ty_cons   <- initialTyCons
  pat_syns  <- initialPatSyns
  data_cons <- initialDataCons
  pure $ MkGamma { dataCons = data_cons
                 , patSyns = pat_syns
                 , tyCons = ty_cons
                 , delta = emptyDelta { variables = vs } }

initialVars :: Tc [(Var, Sigma)]
initialVars = (monomorphic ++) <$> polymorphic
  where
    monomorphic = [plusBD, notBD, orBD, andBD]
    polymorphic = sequenceA [idBD, constBD, concatBD
                            ,undefinedBD,takeBD, singletonBD
                            ,mapBD, foldrBD, consBD
                            ,minusBD, showBD, eqBD]

initialTyCons :: Tc [(TTyCon, (TyCon, Kappa))]
initialTyCons = do
  ka <- newTyVar
  let appArrowTS ts = tauToSigma . appArrowT ts
  pure [(ttEqCt, (tyConEqCt, typeToConstraintSigma))          -- Eq
       ,(ttNumCt, (tyConNumCt, typeToConstraintSigma))        -- Num
       ,(ttShowCt, (tyConShowCt, typeToConstraintSigma))      -- Show
       ,(ttFoldableCt, (tyConFoldableCt, appArrowTS [typeTau, typeTau] constraintTau)) -- Foldable
       ,(ttArrow, (tyConArrow, appArrowTS [typeTau, typeTau] typeTau)) -- (->)
       ,(ttList, (tyConList, typeToTypeSigma))                    -- List
       ,(ttChar, (tyConChar, typeSigma))                             -- Char
       ,(ttInt, (tyConInt, typeSigma))                               -- Int
       ,(ttBool, (tyConBool, typeSigma))                             -- Bool
       ,(ttString, (tyConString, typeSigma))                         -- String
       ,(ttTuple2, (tyConTuple2, appArrowTS [typeTau, typeTau]  typeTau)) -- Tuple2
       ,(ttTuple3, (tyConTuple3, appArrowTS [typeTau, typeTau, typeTau] typeTau)) -- Tuple3
       ,(ttTuple4, (tyConTuple4, appArrowTS [typeTau, typeTau, typeTau, typeTau] typeTau)) -- Tuple4
       ,(ttTilde, (tyConNomEq, QuantifiedSg (Specified (ka, typeSigma)) $ appArrowTS [AlphaT ka, AlphaT ka] typeTau)) -- (~)
       ,(ttType, (tyConType, typeSigma))                -- Type
       ,(ttConstraint, (tyConConstraint, typeSigma))]   -- Constraint

initialPatSyns :: Tc [(TPatSyn, (PatSynDir, PatSynType))]
initialPatSyns = pure [] -- TODO: Add initial pattern synonyms

initialDataCons :: Tc [(TDataCon, DataConType)]
initialDataCons = do
  rightDC <- rightDataCon
  leftDC <- leftDataCon
  nothingDC <- nothingDataCon
  justDC <- justDataCon
  nilDC <- nilDataCon
  consDC <- consDataCon
  mkT2DC <- mkTuple2DataCon
  mkT3DC <- mkTuple3DataCon
  mkT4DC <- mkTuple4DataCon
  pure [trueDataCon,falseDataCon -- Bool
       ,rightDC,leftDC           -- Either
       ,nothingDC,justDC         -- Maybe
       ,nilDC,consDC             -- List
       ,mkT2DC                   -- Tuple2
       ,mkT3DC                   -- Tuple3
       ,mkT4DC]                  -- Tuple4

-- TODO: It might be a good idea to write these with Template Haskell

--------------------------------
---- Term type constructors ----
--------------------------------

mkConLike :: String -> TTyCon
mkConLike = MkConLike . mkIdent

mkTTCon :: String -> TTyCon
mkTTCon = mkConLike

mkTDataCon :: String -> TTyCon
mkTDataCon = mkConLike

ttType :: TTyCon
ttType = mkTTCon "Type"

ttConstraint :: TTyCon
ttConstraint = mkTTCon "Constraint"

ttTilde :: TTyCon
ttTilde = mkTTCon "~"

ttArrow :: TTyCon
ttArrow = mkTTCon "(->)"

ttShowCt :: TTyCon
ttShowCt = mkTTCon "Show"

ttEqCt :: TTyCon
ttEqCt = mkTTCon "Eq"

ttFoldableCt :: TTyCon
ttFoldableCt = mkTTCon "Foldable"

ttNumCt :: TTyCon
ttNumCt = mkTTCon "Num"

ttInt :: TTyCon
ttInt = mkTTCon "Int"

ttString :: TTyCon
ttString = mkTTCon "String"

ttBool :: TTyCon
ttBool = mkTTCon "Bool"

ttChar :: TTyCon
ttChar = mkTTCon "Char"

ttList :: TTyCon
ttList = mkTTCon "List"

ttTuple2 :: TTyCon
ttTuple2 = mkTTCon "Tuple2"

ttTuple3 :: TTyCon
ttTuple3 = mkTTCon "Tuple3"

ttTuple4 :: TTyCon
ttTuple4 = mkTTCon "Tuple4"

---------------------------
---- Data Constructors ----
---------------------------

-- See Note [Data constructor and pattern synonym types]

-- Datatype: Maybe
justDataCon :: Tc (TDataCon, DataConType)
justDataCon = do
  al <- newTyVar
  let ps = [PsiSg $ Specified (al, typeSigma)
           ,PsiT (AlphaT al)]
  pure (mkTDataCon "Just", MkDataConType { dctArgs  = [AlphaT al]
                                         , dctTyCon = tyConMaybe
                                         , dctPsis  = ps })
-- Datatype: Maybe
nothingDataCon :: Tc (TDataCon, DataConType)
nothingDataCon = do
  al <- newTyVar
  pure (mkTDataCon "Nothing"
       ,MkDataConType { dctArgs  = [AlphaT al]
                      , dctTyCon = tyConMaybe
                      , dctPsis  = [PsiSg $ Specified (al, typeSigma)] })
-- Datatype: Either
leftDataCon :: Tc (TDataCon, DataConType)
leftDataCon = do
  al <- newTyVar
  be <- newTyVar
  let ps = [PsiSg $ Specified (al, typeSigma)
           ,PsiSg $ Specified (be, typeSigma)
           ,PsiT (AlphaT al)]
      args = [AlphaT al, AlphaT be]
  pure (mkTDataCon "Left", MkDataConType { dctArgs  = args
                                         , dctTyCon = tyConEither
                                         , dctPsis  = ps })
-- Datatype: Either
rightDataCon :: Tc (TDataCon, DataConType)
rightDataCon = do
  al <- newTyVar
  be <- newTyVar
  let ps = [PsiSg $ Specified (al, typeSigma)
           ,PsiSg $ Specified (be, typeSigma)
           ,PsiT (AlphaT be)]
      args = [AlphaT al, AlphaT be]
  pure (mkTDataCon "Right" ,MkDataConType { dctArgs  = args
                                          , dctTyCon = tyConEither
                                          , dctPsis  = ps })
-- Datatype: Bool
trueDataCon :: (TDataCon, DataConType)
trueDataCon = (mkTDataCon "True", MkDataConType { dctArgs  = []
                                                , dctTyCon = tyConBool
                                                , dctPsis  = []})
-- Datatype: Bool
falseDataCon :: (TDataCon, DataConType)
falseDataCon = (mkTDataCon "False", MkDataConType { dctArgs = []
                                                  , dctTyCon = tyConBool
                                                  , dctPsis  = []})

-- Datatype: List
consDataCon :: Tc (TDataCon, DataConType)
consDataCon = do
  al <- newTyVar
  pure (mkTDataCon "Cons"
       ,MkDataConType { dctArgs  = [AlphaT al]
                       , dctTyCon = tyConList
                       , dctPsis = [PsiSg $ Specified (al, typeSigma)
                                   ,PsiT (AlphaT al)
                                   ,PsiT (AppT listTau (AlphaT al))]})

-- Datatype: List
nilDataCon :: Tc (TDataCon, DataConType)
nilDataCon = do
  al <- newTyVar
  pure (mkTDataCon "Nil"
       ,MkDataConType { dctArgs   = [AlphaT al]
                      , dctTyCon = tyConList
                      , dctPsis  = [PsiSg $ Specified (al, typeSigma)]})

-- Datatype: Tuple2
mkTuple2DataCon :: Tc (TDataCon, DataConType)
mkTuple2DataCon = do
  al <- newTyVar
  be <- newTyVar
  pure (mkTDataCon "MkTuple2"
       ,MkDataConType { dctArgs  = [AlphaT al, AlphaT be]
                      , dctTyCon = tyConTuple2
                      , dctPsis  = [PsiSg $ Specified (al, typeSigma)
                                   ,PsiSg $ Specified (be, typeSigma)
                                   ,PsiT (AlphaT al)
                                   ,PsiT (AlphaT be)]})

-- Datatype: Tuple3
mkTuple3DataCon :: Tc (TDataCon, DataConType)
mkTuple3DataCon = do
  al <- newTyVar
  be <- newTyVar
  ga <- newTyVar
  pure (mkTDataCon "MkTuple3"
       ,MkDataConType { dctArgs  = map AlphaT [al, be, ga]
                      , dctTyCon = tyConTuple3
                      , dctPsis  = [PsiSg $ Specified (al, typeSigma)
                                   ,PsiSg $ Specified (be, typeSigma)
                                   ,PsiSg $ Specified (ga, typeSigma)
                                   ,PsiT (AlphaT al)
                                   ,PsiT (AlphaT be)
                                   ,PsiT (AlphaT ga)] })

-- Datatype: Tuple4
mkTuple4DataCon :: Tc (TDataCon, DataConType)
mkTuple4DataCon = do
  al <- newTyVar
  be <- newTyVar
  ga <- newTyVar
  i  <- newTyVar
  pure (mkTDataCon "MkTuple4"
       ,MkDataConType { dctArgs  = map AlphaT [al, be, ga, i]
                      , dctTyCon = tyConTuple4
                      , dctPsis  = [PsiSg $ Specified (al, typeSigma)
                                   ,PsiSg $ Specified (be, typeSigma)
                                   ,PsiSg $ Specified (ga, typeSigma)
                                   ,PsiSg $ Specified (i, typeSigma)
                                   ,PsiT (AlphaT al)
                                   ,PsiT (AlphaT be)
                                   ,PsiT (AlphaT ga)
                                   ,PsiT (AlphaT i)] })

------------------------
---- Value bindings ----
------------------------

idBD :: Tc (Var, Sigma)
idBD = do
  al <- newTyVar
  pure (mkVar "id" ,(QuantifiedSg (Specified (al, typeSigma)) .
                     tauToSigma $ appArrowT [AlphaT al] (AlphaT al)))

undefinedBD :: Tc (Var, Sigma)
undefinedBD = do
  al <- newTyVar
  pure (mkVar "undefined"
       ,QuantifiedSg (Specified (al, tauToSigma typeTau))
                    ((tauToSigma (AlphaT al))))

plusBD :: (Var, Sigma)
plusBD = (mkVar "plus", tauToSigma $ appArrowT [intTau, intTau] intTau)

constBD :: Tc (Var, Sigma)
constBD = do
  al <- newTyVar
  be <- newTyVar
  pure (mkVar "const", appPsiSigmas [Specified (al, typeSigma)
                                    ,Specified (be, typeSigma)]
                       . tauToSigma
                       $ appArrowT [AlphaT al, AlphaT be] (AlphaT al))

concatBD :: Tc (Var, Sigma)
concatBD = do
  al <- newTyVar
  let listOfAl = AppT listTau (AlphaT al)
  pure (mkVar "concat", QuantifiedSg (Specified (al, typeSigma))
                      . tauToSigma
                      $ appArrowT [listOfAl, listOfAl] listOfAl)

minusBD :: Tc (Var, Sigma)
minusBD = do
  al <- newTyVar
  pure (mkVar "minus",
                     appPsiSigmas [Specified (al, typeSigma), FatArrow (AppT numCtQ (AlphaT al))]
                                   . tauToSigma $ appArrowT [AlphaT al, AlphaT al] (AlphaT al))

takeBD :: Tc (Var, Sigma)
takeBD = do
  al <- newTyVar
  let listOfAl = AppT listTau (AlphaT al)
  pure (mkVar "take", QuantifiedSg (Specified (al, tauToSigma typeTau))
                    . tauToSigma
                    $ appArrowT [intTau, listOfAl] listOfAl)

singletonBD :: Tc (Var, Sigma)
singletonBD = do
  al <- newTyVar
  let listOfAl = AppT listTau (AlphaT al)
  pure (mkVar "singleton", QuantifiedSg (Specified (al, tauToSigma typeTau))
                         . tauToSigma
                         $ ArrowT (AlphaT al) listOfAl)

mapBD :: Tc (Var, Sigma)
mapBD = do
  al <- newTyVar
  be <- newTyVar
  let listOfAl = AppT listTau (AlphaT al)
      listOfBe = AppT listTau (AlphaT be)
  pure (mkVar "map", (QuantifiedSg (Specified (al, typeSigma))
                      . tauToSigma
                      $ appArrowT [(AlphaT al), (AlphaT be), listOfAl]
                        listOfBe))

foldrBD :: Tc (Var, Sigma)
foldrBD = do
  al <- newTyVar
  be <- newTyVar
  ga <- newTyVar
  let psiList = [Specified (ga, typeToTypeSigma)
                ,Specified (al, typeSigma)
                ,Specified (be, typeSigma)
                ,FatArrow $ AppT foldableCtQ (AlphaT ga)]
  pure (mkVar "foldr"
        , appPsiSigmas psiList
        . tauToSigma
        . appArrowT [(appArrowT [AlphaT al, AlphaT be] $ AlphaT be)
                    ,AlphaT be
                    ,(AppT (AlphaT ga) (AlphaT al))]
        $ (AlphaT be))

consBD :: Tc (Var, Sigma)
consBD = do
  al <- newTyVar
  let listOfAl = AppT listTau (AlphaT al)
  pure (mkVar "cons", QuantifiedSg (Specified (al, typeSigma))
                    . tauToSigma
                    . appArrowT [AlphaT al, listOfAl]
                    $ listOfAl)

showBD :: Tc (Var, Sigma)
showBD = do
  al <- newTyVar
  pure (mkVar "show", appPsiSigmas [Specified (al, typeSigma)
                                   ,FatArrow (AppT showCtQ (AlphaT al))]
                    . tauToSigma
                    $ ArrowT (AlphaT al) stringTau)

eqBD :: Tc (Var, Sigma)
eqBD = do
  al <- newTyVar
  pure (mkVar "eq", appPsiSigmas [Specified (al, typeSigma)
                                 ,FatArrow (AppT eqCtQ (AlphaT al))]
                   . tauToSigma
                   . appArrowT [(AlphaT al), (AlphaT al)]
                   $ boolTau)

notBD :: (Var, Sigma)
notBD = (mkVar "not", tauToSigma $ ArrowT boolTau boolTau)

andBD :: (Var, Sigma)
andBD = (mkVar "and", tauToSigma $ ArrowT boolTau boolTau)

orBD :: (Var, Sigma)
orBD = (mkVar "or", tauToSigma $ ArrowT boolTau boolTau)
