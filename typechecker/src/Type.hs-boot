{-# LANGUAGE TypeData #-}
{-# LANGUAGE TypeFamilies #-}

module Type where

import BasicTypes
import Data.Kind (Type)

data Sigma
data Rho
data Tau

data Psi
data PsiSg
data PsiR


instance Show Psi
instance Show PsiR
instance Show PsiSg

instance Show Sigma
instance Show Rho
instance Show Tau

data TyCon

instance Show TyCon
instance Eq TyCon

data TyVar

data SkolTyVar

data MetaTyVar

data MuTyVar

data TcTyVar

instance Show TcTyVar
instance Uniquable TcTyVar

instance Show TyVar
instance Eq TyVar
instance Uniquable TyVar

instance Show SkolTyVar
instance Eq SkolTyVar
instance Uniquable SkolTyVar

instance Show MetaTyVar
instance Uniquable MetaTyVar
instance Eq MetaTyVar

instance Show MuTyVar
instance Uniquable MuTyVar
instance Eq MuTyVar

data MetaInfo
data MetaDetails


data DataConType

instance Show DataConType

instance Show PatSynType

data PatSynType

type Binding = (TyVar, Kappa)

type Q     = Tau

type Phi   = Sigma

type Iota  = Tau

type Kappa = Sigma

type TcSigma = Sigma
type TcPhi = Phi
type TcKappa = Kappa
type TcTau = Tau
type TcRho = Rho
type TcQ = Q
