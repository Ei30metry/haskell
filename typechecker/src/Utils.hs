-- |

module Utils
  (concatMapM
  ,mapMaybeM
  ,anyM
  ,allM
  ,orM
  ,andM
  ,foldM_
  ,foldMapM
  ,whenM
  ,unlessM
  ,filterOutM
  ,partitionM
  ,ifM
  ,headMaybe
  ,headEither
  ,disjoint
  ,eqFst
  ,(|#|)
  ,dom
  ,rng
  ,subset
  ,snoc
  ,pprList)
where

import Data.Monoid
import Data.List
import Prettyprinter

import Control.Monad

concatMapM :: (Monad m, Traversable f) => (a -> m [b]) -> f a -> m [b]
concatMapM f xs = fmap concat (mapM f xs)

mapMaybeM :: Applicative m => (a -> m (Maybe b)) -> [a] -> m [b]
mapMaybeM f = foldr g (pure [])
  where g a = liftA2 (maybe id (:)) (f a)

anyM :: (Monad m, Foldable f) => (a -> m Bool) -> f a -> m Bool
anyM f = foldr (orM . f) (pure False)

allM :: (Monad m, Foldable f) => (a -> m Bool) -> f a -> m Bool
allM f = foldr (andM . f) (pure True)

orM :: Monad m => m Bool -> m Bool -> m Bool
orM m1 m2 = m1 >>= \x -> if x then return True else m2

andM :: Monad m => m Bool -> m Bool -> m Bool
andM m1 m2 = m1 >>= \x -> if x then m2 else return False

foldlM_ :: (Monad m, Foldable t) => (a -> b -> m a) -> a -> t b -> m ()
foldlM_ = foldM_

foldMapM :: (Applicative m, Foldable t, Monoid b) => (a -> m b) -> t a -> m b
foldMapM f = getAp <$> foldMap (Ap . f)

whenM :: Monad m => m Bool -> m () -> m ()
whenM mb thing = do { b <- mb
                    ; when b thing }

unlessM :: Monad m => m Bool -> m () -> m ()
unlessM condM acc = do { cond <- condM
                       ; unless cond acc }

filterOutM :: (Applicative m) => (a -> m Bool) -> [a] -> m [a]
filterOutM p =
  foldr (\ x -> liftA2 (\ flg -> if flg then id else (x:)) (p x)) (pure [])

partitionM :: Monad m => (a -> m Bool) -> [a] -> m ([a], [a])
partitionM _ [] = pure ([], [])
partitionM f (x:xs) = do
    res <- f x
    (as,bs) <- partitionM f xs
    pure ([x | res] ++ as, [x | not res] ++ bs)

ifM :: Monad m => m Bool -> m a -> m a -> m a
ifM b t f = do
  b' <- b
  if b' then t else f

headMaybe :: forall a. [a] -> Maybe a
headMaybe []     = Nothing
headMaybe (x:_)  = Just x

headEither :: forall a. [a] -> Either String a
headEither []    = Left "Empty list"
headEither (x:_) = Right x

disjoint :: forall a. Eq a => [a] -> [a] -> Bool
disjoint xs ys = null (xs `intersect` ys)

eqFst :: forall a b. Eq a => a -> (a, b) -> Bool
eqFst v (a,_) = a == v

(|#|) :: forall a. Eq a => [a] -> [a] -> Bool
xs |#| ys = disjoint xs ys

dom :: forall a b. [(a,b)] -> [a]
dom = map fst

rng :: forall a b. [(a,b)] -> [b]
rng = map snd

subset :: forall a. Eq a => [a] -> [a] -> Bool
subset = isSubsequenceOf

snoc :: forall a. a -> [a] -> [a]
snoc x = (++ [x])

pprList :: forall a b. ([b] -> Doc a) -> [b] -> Doc a
pprList _ []   = pretty "[]"
pprList f list = f list
