-- | Parser

module Parser (
  -- Parse, and then translate the Expr AST to Term
  parseTranslate,
  -- Parse Haskell file into Microparser's Expr AST
  parseHsFile,
  )
where

import Control.Monad

import Language.MicroHs.Parse
import qualified Language.MicroHs.Expr as E
import Language.MicroHs.Ident
import Data.Char
import Utils
import Data.Bifunctor
import Term

type ESign  = ([Ident], E.EType)
type EFunBn = (Ident, [E.Eqn])


parseTranslate :: FilePath -> IO (Either String ValDecls)
parseTranslate = pure . translateParsed <=< parseHsFile

parseHsFile :: FilePath -> IO ([ESign], [EFunBn])
parseHsFile fp = do
  file <- readFile fp
  case parse pTop "" file of
    Left x                     -> fail x
    Right (E.EModule _ _ defs) -> pure $ go ([],[]) defs
      where
        go (ss, fs) (E.Fcn idt eqs : ds)   = go (ss, (idt, eqs) : fs) ds
        go (ss, fs) (E.Sign idts ety : ds) = go ((idts, ety) : ss, fs) ds
        go (ss, fs) (_ : ds)               = go (ss, fs) ds
        go acc []                          = acc

translateParsed :: ([ESign], [EFunBn]) -> Either String ValDecls
translateParsed (sigs, decls)
  = MkValDecls
  . concat <$> traverse (uncurry toSigs) sigs
           <*> traverse (\(idt, eqs) -> FunBind <$> detVar (unIdent idt)
                                                <*> traverse toMatch eqs) decls

toMatches :: [E.Eqn] -> Either String [Match]
toMatches = traverse toMatch

toMatch :: E.Eqn -> Either String Match
toMatch (E.Eqn pats (E.EAlts alts _)) =
  let rhs = snd <$> headEither alts
  in MkMatch <$> traverse (fmap VisArg . toTerm) pats
             <*> (toTerm =<< rhs)

caseArmToMatch :: E.ECaseArm -> Either String Match
caseArmToMatch (pat, E.EAlts alts _) =
  let rhs = snd <$> headEither alts
  in MkMatch <$> fmap ((:[]) . VisArg) (toTerm pat)
             <*> (toTerm =<< rhs)

toLit :: E.Lit -> Either String Lit
toLit (E.LInt _)     = Right LitInt
toLit (E.LInteger _) = Right LitInt
toLit (E.LStr _)     = Right LitString
toLit (E.LChar _)    = Right LitChar
toLit _              = Left "Unsupported literal!"

toValDecls :: [E.EBind] -> Either String ValDecls
toValDecls = fmap (uncurry MkValDecls) . go (Right ([],[]))
    where
    go (Right (sigs, binds)) bs'
      = case bs' of
          E.BFcn idt eqs : bs
            -> toMatches eqs >>= (\b -> go (Right (sigs, b : binds)) bs) . FunBind (MkVar idt)
          E.BPat p e : bs
            -> PatBind <$> toTerm p <*> toTerm e >>= \b -> go (Right (sigs, b : binds)) bs
          E.BSign is typ : bs
            -> toSigs is typ >>= \ss -> go (Right (ss ++ sigs, binds)) bs
          E.BDfltSign _ _ : _
            -> Left "Default signatures are not supported!"
          []
            -> Right (sigs, binds)
    go (Left x) _ = Left x

toSigs :: [Ident] -> E.EType -> Either String [Sig]
toSigs idts ety
  = let ty = toTerm ety in traverse (\v -> MkSig (MkVar v) <$> ty) idts

toTyp :: [E.IdKind] -> E.EType -> Either String Typ
toTyp idks ety = do
  typ <- toTerm ety
  binders <- traverse (\(E.IdKind v t) -> Ann (MkVar v) <$> toTerm t) idks
  pure $ foldr (TForall QSpecified) typ binders

toHead :: E.Expr -> Either String Head
toHead (E.EVar v)         = Right (toVarOrWildCard v)
  where
    toVarOrWildCard :: Ident -> Head
    toVarOrWildCard idt = conOrVarOrWC (unIdent idt)
    detTerm i@('_':cs)
      | null cs = HWildCard
      | otherwise = HVar $ MkNamedWC (mkIdent i)
    detTerm i = HVar $ MkVar (mkIdent i)
    conOrVarOrWC str@(c:cs)
      | isUpper c = (HConLike . MkConLike . mkIdent) str
      | otherwise = detTerm str
toHead (E.ESign idt typ)  = HAnn  <$> toTerm idt <*> toTerm typ
toHead e                  = HTerm <$> toTerm e

toTerm :: E.Expr -> Either String Term
toTerm (E.EApp e1 e2)      = uncurry (liftA2 TApp) (bimap toHead toArgs (toApp e1 e2))
toTerm h@(E.EVar i)        = app0 <$> toHead h
toTerm h@(E.ESign _ _ )    = app0 <$> toHead h
toTerm (E.ELit _ l)        = TLit <$> toLit l
toTerm (E.EIf cond te ee)  = TIf <$> toTerm cond <*> toTerm te <*> toTerm ee
toTerm (E.EForall _ ks ty) = toTyp ks ty
toTerm (E.EParen e)        = toTerm e
toTerm (E.ECase sc alts)   = TCase <$> toTerm sc <*> traverse caseArmToMatch alts
toTerm (E.ELam eqs)        = TSuperLambda <$> toMatches eqs
toTerm (E.ELet bs e)       = TLet <$> toValDecls bs <*> toTerm e
toTerm (E.EAt idt p)       = TAs (MkVar idt) <$> toTerm p
toTerm (E.ETuple as)       = TApp (HConLike (MkConLike $ mkIdent ("MkTuple" ++ show (length as))))
                           <$> traverse (fmap VisArg . toTerm) as
toTerm (E.EViewPat e p)    = TArrow <$> toTerm e <*> toTerm p
toTerm (E.EOper ret args)  = Left "Operators are not supported!"
toTerm (E.ENegApp _)       = Left "The Negation operator is not supported!"
toTerm (E.ELazy _ _)       = Left "Lazy patterns are not supported!"
toTerm (E.EUVar _)         = Left "EUVar: Impossible!"
toTerm (E.EDo _ _)         = Left "Do blocks are not supported!"
toTerm (E.EMultiIf _)      = Left "Multiway If-expressions are not supported!"
toTerm (E.EOr _)           = Left "Or-patterns are not supported!"
toTerm (E.EListish _)      = Left "Listish is not supported!"
toTerm (E.ESectL _ _)      = Left "Sections are not supported!"
toTerm (E.ESectR _ _)      = Left "Sections are not supported!"
toTerm (E.EUpdate _ _)     = Left "Record updates are not supported!"
toTerm (E.ESelect _)       = Left "Select is not supported!"
toTerm (E.ECon _)          = Left "ECon can't happen"

toApp :: E.Expr -> E.Expr -> (E.Expr, [E.Expr])
toApp e1 e2 = second (reverse . (e2 :)) (go e1 [])
  where
    go (E.EApp e1 e2) es = second (e2 :) (go e1 es)
    go e1 es             = (e1, es)

toArgs :: [E.Expr] -> Either String [Arg]
toArgs = traverse (fmap VisArg . toTerm)

detVar :: String -> Either String Var
detVar idt@('_':cs)
  | null cs
  = Left "WildCard can't be used as a function name!"
  | otherwise
  = Right $ MkNamedWC (mkIdent idt)
detVar idt = Right $ MkVar (mkIdent idt)
