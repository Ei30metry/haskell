-- |

module Tc
  (module Tc.Monad
  ,module Tc.Gen.ValDecl
  ,module Tc.Gen.Expr
  ,module Tc.Gen.Pat
  ,module Tc.Gen.Kind
  ,module Tc.Gen.Type
  ,typeCheckFile)
where

import Term
import Type

import Tc.Monad
import Tc.Gen.ValDecl
import Tc.Gen.Expr
import Tc.Gen.Pat
import Tc.Gen.Kind
import Tc.Gen.Type

import Parser

import Data.ByteString.Lazy (ByteString)
import Data.ByteString.Builder (toLazyByteString, string7)

-- TODO: This is just terrible
typeCheckFile :: Maybe FilePath -> FilePath -> IO ByteString
typeCheckFile outputPath path =
  let
    toBS = toLazyByteString . string7 . show
  in do parsRes <- parseTranslate path
        case parsRes of
          Left x -> (pure . toLazyByteString . string7) x
          Right vds -> do
            tcRes <- runTc outputPath (flip tcValDecls vds)
            case tcRes of
              Left err -> pure (toBS err)
              Right x  -> pure (pprVarBindingsToBS x)
