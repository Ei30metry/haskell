-- | Typechecker constraints

module Tc.Constraint (
  -- Non-canonical constraints. See Note [Non-canonical constraints]
  Ct(..), CtFlavour(..),
  isGiven,
  isWanted,

  mapCt,
  mkCt,
  mkEqPred,
  mkNCEqCt,

  -- Canonical constraints. See Note [Canonical constraints]
  CanEqCt(..),
  eqCtFlavour,
  mkCanEqCt,

  -- Irreducible constraints. See Note [Irreducible constraints]
  CanIrredCt(..),
  IrredReason(..),

  -- Implication constraint
  ImplicCt(..),
  addImplics,
  buildImplicCt,

  -- Wanted constraints
  WantedCts(..),
  isInsolubleWC,
  isSolvedWC,
  isEmptyWC,

  addSimples,
  andCts,
  andWC,
  mkSimpleWC,
  emptyWC,
  mkImplicWC,
  unionsWC,

  -- Pretty printing functions
  pprCt,
  pprAsEq,
  pprCanEqCt,
  pprCtFlavour,
  pprCtsH,
  pprCtsV,
  pprImplicCt,
  pprImplicCtsV,
  pprImplicCtsH,
  pprWantedCts,
 )
where

import Tc.Types
import Type
import Prettyprinter
import Utils (pprList)

{- Note [Non-canonical constraints]
The initial, unprocessed form of a constraint, which cannot be used to simplified or solve another
constraint. During canonicalization (see Note [Canonicalization]), non-canonical constraints
are either turned into irreducible constraints, (see Note [Irreducible constraints]) or their
canonical form, which will then be put in the InertSet (see Note [InertSet]) and used during solving
-}

data Ct =
  MkCt
    { ctPred    :: Q
    , ctFlavour :: CtFlavour
    }
  deriving Show

{- Note [Canonical constraints]
  - eqLhs is always a TcTyVar
  - eqRhs could be anything
  - eqPred is just: eqLhs ~ eqRhs
  - eqLhs ~ eqRhs is a canonical constraint. See Note [Canonicalization]
-}
data CanEqCt =
  MkCanEqCt
    { eqPred :: Ct
    , eqLhs  :: TcTyVar
    , eqRhs  :: TcTau
    }
  deriving (Show)

data CtFlavour
  = CtGiven
  | CtWanted
  deriving Show

{- TODO: Note [Irreducible constraints]
Invariant: Is always canonical.
-}

-- Used to indicate extra information about why a CanIrredCt is irreducible
data IrredReason
  = IrredShapeReason
  | NonCanonicalReason
  | ShapeMismatchReason
  deriving Show

-- See Note [Irreducible constraints]
data CanIrredCt = MkIrredCt
  { irredCt     :: Ct
  , irredReason :: IrredReason
  }
  deriving Show

-- TODO: Fix the following note
{- Note [ImplicCt invariants]
  - We track the TcLevel of the innermost implication that has a Given equality, and its Given is not a let-bound skolem variable one. The value is stored in inertGivenEqLvl
  - We should make sure that the given we are inspecting is canonicalized
  - A meta-variable alpha[n] is untouchable iff n < inertGivenEqLvl (born outside)
-}

data ImplicCt
  = MkImplic { implTcLvl   :: TcLevel
             , implSkolems :: [(SkolTyVar, Kappa)]
             , implGivens  :: [Ct]        -- TODO: Consider using Seq
             , implWanteds :: WantedCts }
  deriving Show

data WantedCts =
  MkWC { wcImplics :: [ImplicCt] -- TODO: Consider using Seq
       , wcSimples :: [Ct] }     -- TODO: Consider using Seq
  deriving Show

mapCt :: (Q -> Q) -> Ct -> Ct
mapCt f ct@(MkCt { ctPred = q })
  = ct { ctPred = f q }

isEmptyWC :: WantedCts -> Bool
isEmptyWC (MkWC {wcImplics = i, wcSimples = s})
  = null i && null s

isSolvedWC :: WantedCts -> Bool
isSolvedWC (MkWC { wcImplics = impls
                 , wcSimples = cts })
  = null cts -- TODO: FIX ME: the implication is not being tested at all.


isGiven :: Ct -> Bool
isGiven (MkCt { ctFlavour = CtGiven}) = True
isGiven _                             = False

isWanted :: Ct -> Bool
isWanted = not . isGiven

isInsolubleWC :: WantedCts -> Bool
isInsolubleWC wcs = False -- TODO: FIX me

mkSimpleWC :: CtFlavour -> [Q] -> WantedCts
mkSimpleWC flv preds
  = emptyWC { wcSimples = map (mkCt flv) preds }

emptyWC :: WantedCts
emptyWC = MkWC { wcImplics = []
               , wcSimples = [] }

andWC :: WantedCts -> WantedCts -> WantedCts
andWC (MkWC { wcImplics = i1
            , wcSimples = s1 })
      (MkWC { wcImplics = i2
            , wcSimples = s2 })
  = MkWC { wcImplics = i1 ++ i2
         , wcSimples = s1 ++ s2}

andCts :: [Ct] -> [Ct] -> [Ct]
andCts = (++)

mkImplicWC :: [ImplicCt] -> WantedCts
mkImplicWC implics = emptyWC { wcImplics = implics }

unionsWC :: [WantedCts] -> WantedCts
unionsWC = foldr andWC emptyWC

addImplics :: WantedCts -> [ImplicCt] -> WantedCts
addImplics wcs implics
  = wcs { wcImplics = wcImplics wcs ++ implics }

addSimples :: WantedCts -> [Ct] -> WantedCts
addSimples wcs simples
  = wcs { wcSimples = wcSimples wcs ++ simples }

mkEqPred :: TcTau -> TcTau -> Q
mkEqPred t1 t2 = AppT (AppT (TyConT tyConNomEq) t1) t2

eqCtFlavour :: CanEqCt -> CtFlavour
eqCtFlavour = ctFlavour . eqPred

-- See Note [Non-canonical constraints]
mkNCEqCt :: CtFlavour -> TcTau -> TcTau -> Ct
mkNCEqCt flv t1 t2 = MkCt { ctPred    = mkEqPred t1 t2
                          , ctFlavour = flv }

mkCanEqCt :: CtFlavour -> TcTyVar -> TcTau -> CanEqCt
mkCanEqCt flv lhs rhs
  = MkCanEqCt { eqPred = mkNCEqCt flv (TcTyVarT lhs) rhs
              , eqLhs  = lhs
              , eqRhs  = rhs }

mkCt :: CtFlavour -> TcQ -> Ct
mkCt = flip MkCt

buildImplicCt :: TcLevel -> [(SkolTyVar, Kappa)] -> [Ct] -> WantedCts -> [ImplicCt]
buildImplicCt tclvl skols givens wanteds
  | isEmptyWC wanteds && null givens
  = []
  | otherwise
  = [MkImplic { implTcLvl   = tclvl
              , implSkolems = skols
              , implGivens  = givens
              , implWanteds = wanteds }]

pprCt :: forall a. Ct -> Doc a
pprCt (MkCt { ctPred    = q
            , ctFlavour = fl }) = pprCtFlavour fl <+> pprTau q

pprCanEqCt :: forall a. CanEqCt -> Doc a
pprCanEqCt (MkCanEqCt { eqPred = ct
                      , eqLhs  = lhs
                      , eqRhs  = rhs })
  = align $ vsep [pretty "eqPred =" <+> pprCt ct
                 ,pretty "eqLhs  =" <+> pprTcTyVar lhs
                 ,pretty "eqRhs  =" <+> pprTau rhs]

pprCtsV :: forall a. [Ct] -> Doc a
pprCtsV = pprList (vsep . map pprCt)

pprCtsH :: forall a. [Ct] -> Doc a
pprCtsH = pprList (hsep . map pprCt)

pprCtFlavour :: forall a. CtFlavour -> Doc a
pprCtFlavour CtGiven  = pretty "[G]"
pprCtFlavour CtWanted = pretty "[W]"

pprImplicCt :: forall a. ImplicCt -> Doc a
pprImplicCt (MkImplic { implTcLvl   = lvl
                      , implSkolems = skolems
                      , implGivens  = cts
                      , implWanteds = wcts })
  = vcat [pretty "implTcLvl   =" <+> pprTcLevel lvl
         ,pretty "implSkolems =" <+> pprSkolKinds skolems
         ,pretty "implGivens  =" <+> pprCtsV cts
         ,pretty "implWanteds =" <+> align (pprWantedCts wcts)]

pprImplicCtsV :: forall a. [ImplicCt] -> Doc a
pprImplicCtsV = pprList (vsep . map pprImplicCt)

pprImplicCtsH :: forall a. [ImplicCt] -> Doc a
pprImplicCtsH = pprList (hsep . map pprImplicCt)

pprWantedCts :: forall a. WantedCts -> Doc a
pprWantedCts (MkWC { wcSimples = simpls
                   , wcImplics = impls })
  = vcat [pretty "wcImplics =" <+> align (pprImplicCtsV impls)
         ,pretty "wcSimples =" <+> align (pprCtsV simpls)]

pprAsEq :: forall a. CtFlavour -> TcTau -> TcTau -> Doc a
pprAsEq flv t1 t2 = pprCtFlavour flv <+>
                      pprTau t1  <+>
                      pretty '~' <+>
                      pprTau t2
