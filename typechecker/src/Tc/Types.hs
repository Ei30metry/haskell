-- | Types that are commonly used throughout the typechecker

module Tc.Types (
  module BasicTypes,
  module Initial,
  -- Typechecker Level. See Note [TcLevel]
  TcLevel(..) ,
  pushTcLevel,
  topTcLevel,
  isTopTcLevel,
  maxTcLevel,
  minTcLevel,
  pprTcLevel,

  -- Gamma: \Gamma, Delta: \Delta, and Sigma: \Sigma
  Gamma(..), Delta(..), SigmaEnv,
  emptyDelta, appendDelta,
  pprGamma, pprDelta,
  pprTidiedVarBinding, pprTidiedVarBindings,
  pprVarBinding, pprVarBindings,
  pprScopedTyVar, pprScopedTyVars,

  -- The Typechecker Monad
  Tc, TcError(..), Env(..), updEnv,
  liftEither, throwError, getEnv,
  runTc, initEnv, notInScope, tyVarIsFresh,

  -- Language extensions and pattern synonyms
  PatSynDir(..),
  LangExt(..),
  pprLangExt,

  -- Infereence
  ExpType(..), -- See Note [ExpType]
  InferHole(..),
  InferResult(..), -- See Note [InferResult]

  -- Mutuble references in the typechecker
  TcRef,
  newTcRefIO,
  newTcRef,
  readTcRef,
  writeTcRef,
  modifyTcRef,

  TvBinding,
) where

import BasicTypes
import Data.IORef
import Type
import Term
import Utils (pprList)
import Control.Monad
import Control.Monad.IO.Class
import Control.Exception
import {-# SOURCE #-} Tc.Constraint
import {-# SOURCE #-} Initial
import Data.Text (Text)
import Prettyprinter

-- TODO: Note [TcLevel]

newtype TcLevel
  = Lvl Int
  deriving (Eq, Show, Ord)

pushTcLevel :: TcLevel -> TcLevel
pushTcLevel (Lvl l1) = Lvl (l1 + 1)

topTcLevel :: TcLevel
topTcLevel = Lvl 0

isTopTcLevel :: TcLevel -> Bool
isTopTcLevel = (== topTcLevel)

maxTcLevel :: TcLevel -> TcLevel -> TcLevel
maxTcLevel (Lvl u1) (Lvl u2)
  | u1 > u2    = Lvl u1
  | otherwise  = Lvl u2

minTcLevel :: TcLevel -> TcLevel -> TcLevel
minTcLevel (Lvl u1) (Lvl u2)
  | u1 < u2   = Lvl u1
  | otherwise = Lvl u2

pprTcLevel :: forall a. TcLevel -> Doc a
pprTcLevel (Lvl i) = pretty i

newtype Tc a = MkTc { unTc :: (Env -> IO (Either TcError a)) }

instance MonadIO Tc where
  liftIO io = MkTc $ \_ -> fmap Right io

instance MonadFail Tc where
  fail = undefined -- TODO: fix me

instance Monad Tc where
  (MkTc x) >>= f = MkTc $ \env -> do
    x' <- x env
    case x' of
      Right y  -> unTc (f y) env
      Left err -> pure (Left err)

instance Applicative Tc where
  pure x = MkTc $ \_ -> pure (Right x)
  (<*>)  = ap

instance Functor Tc where
  fmap f (MkTc x) = MkTc $ \env -> fmap (fmap f) (x env)

updEnv :: forall a. (Env -> Env) -> Tc a -> Tc a
updEnv f (MkTc tc) = MkTc (tc . f)

liftEither :: Either TcError a -> Tc a
liftEither x = MkTc $ \_ -> pure x

throwError :: forall a. TcError -> Tc a
throwError err = MkTc $ \_ -> (pure . Left) err

notInScope :: forall a. Doc Text -> Either TcError a
notInScope = Left . NotInScopeError

getEnv :: Tc Env
getEnv = MkTc (pure . Right)

initEnv :: Maybe FilePath -> IO Env
initEnv outputPath = do
  unique <- newTcRefIO 0
  ctvref <- newTcRefIO emptyWC
  pure $ MkEnv { ambientTcLevel  = topTcLevel
               , lastUnique      = unique
               , wantedCts       = ctvref
               , traceOutputPath = outputPath
               , langExts        = [XPatSigBind
                                  ,XExtendedForallScope
                                  ,XTypeAbstractions] }

runTc :: forall a. Maybe FilePath -> (Gamma -> Tc a) -> IO (Either TcError a)
runTc oPath tc = unTc (initialGamma >>= tc) =<< initEnv oPath

data TcError
  = NotInScopeError (Doc Text)
  | UntouchableMetaVar (Doc Text)
  | MuVarInCS (Doc Text)
  | ScopingError (Doc Text)
  | PolyTypeInConstraintError (Doc Text)
  | IllegalTermError (Doc Text)
  | ArgDoesntMatchQuantifer (Doc Text)
  | ImplementationBug (Doc Text)
  | IllegalArgumentPattern (Doc Text)
  | ExpectedLessArguments (Doc Text)
  | KindCheckingError (Doc Text)
  | KindingError (Doc Text)
  | WrongDataConType (Doc Text)
  | WrongPatSynType (Doc Text)
  | IllegalSigmaType (Doc Text)
  | ShapeMismatch (Doc Text)
  | OccursCheck (Doc Text)
  | UnsupportedCt (Doc Text)
  | DifferentTyCons (Doc Text)
  | UnaccompaniedSigs (Doc Text)
  | IllegalCaseLHS (Doc Text)
  | ProbablyNotZonked (Doc Text)
  | ProbablyNotFilledIn (Doc Text)
  | ArityMisMatch (Doc Text)
  | CantSolveUnmatchedPsis (Doc Text)
  | InsolubleWC (Doc Text)
  | CantConvertToRho (Doc Text)
  | CantConvertToTau (Doc Text)
  | PprPanic (Doc Text)
  deriving Show

instance Exception TcError

data Env =
  MkEnv
    { ambientTcLevel  :: TcLevel
    , lastUnique      :: TcRef Unique
    , wantedCts       :: TcRef WantedCts
    , langExts        :: [LangExt]
    , traceOutputPath :: Maybe FilePath
    }

-- \Gamma in the spec
data Gamma =
  MkGamma
    { tyCons   :: [(TTyCon, (TyCon, Kappa))]
    , dataCons :: [(TDataCon, DataConType)] -- See Note [Data constructor and pattern synonym types]
    , patSyns  :: [(TPatSyn, (PatSynDir, PatSynType))] -- See Note [Data constructor and pattern synonym types]
    , delta    :: Delta
    }
    deriving Show

-- \Delta in the spec
data Delta =
  MkDelta
    { scopedTyVars :: [(Var,  (Tau, Kappa))] -- TODO: Turn this into a record?
    , skolems      :: [(SkolTyVar, Kappa)]
    , variables    :: [(Var,  Sigma)] -- TODO: Note [How shadowing is handled]
    }
    deriving Show

instance Pretty Delta where
  pretty = pprDelta

pprDelta :: forall a. Delta -> Doc a
pprDelta (MkDelta { scopedTyVars = stvs
                  , skolems      = sks
                  , variables    = vars })
  = vcat [pretty "scopedTyVars =" <+> pprScopedTyVars stvs
         ,pretty "skolems      =" <+> pprSkolKinds sks
         ,pretty "variables    =" <+> pprVarBindings vars]

pprTidiedVarBinding :: forall a. (Var, Sigma) -> Doc a
pprTidiedVarBinding (v,sg) = pprVar v <+> pretty ':' <+> pprTidiedSigma sg

pprVarBinding :: forall a. (Var, Sigma) -> Doc a
pprVarBinding (v,sg) = pprVar v <+> pretty ':' <+> pprSigma sg

pprVarBindings :: forall a. [(Var,Sigma)] -> Doc a
pprVarBindings = pprList (hsep . map pprVarBinding)

pprTidiedVarBindings :: forall a. [(Var,Sigma)] -> Doc a
pprTidiedVarBindings = pprList (vsep . map pprTidiedVarBinding)

pprScopedTyVar :: forall a. (Var, (Tau, Kappa)) -> Doc a
pprScopedTyVar = undefined

pprScopedTyVars :: [(Var, (Tau, Kappa))] -> Doc a
pprScopedTyVars = hsep . map pprScopedTyVar

pprGamma :: forall a. Gamma -> Doc a
pprGamma = undefined

type SigmaEnv = [(Var, Sigma)]

emptyDelta :: Delta
emptyDelta = MkDelta [] [] []

tyVarIsFresh :: [(SkolTyVar,Kappa)] -> TyVar -> Bool
tyVarIsFresh sks tv = foldr (\(sk,_) b -> skolTyVar sk == tv && b) True sks

emptyGamma :: Gamma
emptyGamma = MkGamma [] [] [] emptyDelta

instance Semigroup Delta where
  (<>) = appendDelta

instance Monoid Delta where
  mappend = (<>)
  mempty = emptyDelta

instance Semigroup Gamma where
  (<>) = appendGamma

instance Monoid Gamma where
  mappend = (<>)
  mempty = emptyGamma

appendGamma :: Gamma -> Gamma -> Gamma
appendGamma (MkGamma { tyCons   = t1
                     , dataCons = c1
                     , patSyns  = p1
                     , delta    = d1 })
            (MkGamma { tyCons   = t2
                     , dataCons = c2
                     , patSyns  = p2
                     , delta    = d2 })
  = MkGamma { tyCons   = t1 <> t2
            , dataCons = c1 <> c2
            , patSyns  = p1 <> p2
            , delta    = d1 <> d2}

appendDelta :: Delta -> Delta -> Delta
appendDelta (MkDelta { scopedTyVars = stys1
                     , skolems      = sks1
                     , variables    = vs1 })
            (MkDelta { scopedTyVars = stys2
                     , skolems      = sks2
                     , variables    = vs2 })
  = MkDelta { scopedTyVars = stys1 <> stys2
            , skolems      = sks1 <> sks2
            , variables    = vs1 <> vs2 }

data PatSynDir
  = Unidir
  | Bidir
  deriving (Eq, Show)

data LangExt
  = XPatSigBind
  | XExtendedForallScope
  | XTypeAbstractions
  | XDeepSubsumption
  deriving (Eq, Show)

pprLangExt :: forall a. LangExt -> Doc a
pprLangExt XPatSigBind          = pretty "-XPatSigBind"
pprLangExt XExtendedForallScope = pretty "-XExtendedForallScope"
pprLangExt XTypeAbstractions    = pretty "-XTypeAbstractions"
pprLangExt XDeepSubsumption     = pretty "-XDeepSubsumption"


-- TODO: Note [ExpType]

-- \delta in the spec. See Note [ExpType]
data ExpType
  = Infer InferResult
  | Check TcSigma

data InferHole
  = Hole
  | Filled TcRho
  deriving Show

-- TODO: Note [InferResult]

data InferResult =
  IR
    { irLvl  :: TcLevel
    , irRef  :: TcRef InferHole
    }

newtype TcRef a = MkTcRef { unTcRef :: IORef a }

newTcRef :: forall a. a -> Tc (TcRef a)
newTcRef x = MkTc $ \_ -> (fmap (Right . MkTcRef) . newIORef) x

newTcRefIO :: forall a. a -> IO (TcRef a)
newTcRefIO x = MkTcRef <$> newIORef x

readTcRef :: forall a. TcRef a -> Tc a
readTcRef = liftIO . readIORef . unTcRef

writeTcRef :: forall a. TcRef a -> a -> Tc ()
writeTcRef x = liftIO . writeIORef (unTcRef x)

modifyTcRef :: forall a. TcRef a -> (a -> a) -> Tc ()
modifyTcRef x = liftIO . modifyIORef' (unTcRef x)

type TvBinding = (Var, TyVar, Kappa)
