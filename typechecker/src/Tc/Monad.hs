-- | Utilities for working with the Tc Monad

module Tc.Monad
  (module Tc.Types
  -- Functions over Gamma and its stuff
  ,modifyScopedTyVars
  ,modifySkolems
  ,modifyVariables
  ,modifyDelta
  ,extendDeltaVars
  ,extendSkolems
  ,extendDeltaScopedTyVars
  ,disjointDomDelta
  ,disjointScopedTyVars

  ,modifyAmbientTcLevel
  ,setAmbientTcLevel
  ,pushAmbientTcLevel

  -- Capturing constraints that were emited
  ,captureCts
  ,pushLevelAndCaptureCts

  -- Building implication constraints
  ,implicCtNeeded
  ,buildImplicIfNeeded

  -- Promotion. See Note [Promotion]
  ,promoteTcSigma
  ,promoteMetaVarTo
  ,promoteMetaVars

  -- Emiting constraints to the Tc Monad
  ,emitSimples
  ,emitSimple
  ,emitWantedCts
  ,emitImplics
  ,emitImplic
  ,emitResidualCts

  -- Bidirectional typechecking. See Note [ExpType]
  ,readExpType
  ,newInferType
  ,newInferResult
  ,expTypeToSigma
  ,inferResultToRho
  ,extractInferredResult
  ,inferResultToMetaVar
  ,tcInfer
  ,tcInfer_
  ,ensureTauType
  ,fillInferResult

  -- Fucntions over TcTyVars
  ,newTyVar
  ,newSkol
  ,newMuVar
  ,newMetaVar
  ,cloneMetaVar
  ,newTauMetaVar
  ,newMetaVarAt
  ,newMetaVarAndKind
  ,newMetaVarAtAmbLvl
  ,newMetaVarOfKindType
  ,newMetaVarOfKindConstraint

  -- Retrieving environment from the Tc Monad
  ,getTcLevel
  ,getLastUnique
  ,getWantedCtsVar
  ,getLangExts
  ,langFlag

  -- Looking up stuff in Gamma and Delta
  ,GammaElt(..)
  ,KPLookupRes(..)
  ,lookupVarType
  ,lookupVarTypeInSigs
  ,lookupScopedTyVar
  ,lookupSkolemKind
  ,lookupDataCon
  ,lookupPatSyn
  ,lookupTyCon
  ,lookupGammaElt

  -- Zonking. See Note [Zonking]
  ,zonkSigma
  ,zonkRho
  ,zonkTau
  ,zonkCt
  ,zonkCts
  ,zonkWantedCts
  ,zonkImplicCt
  ,zonkImplicCts
  ,zonkMetaVar

  -- Language extensions
  ,deepSubFlag
  ,tyAbsFlag
  ,patSigBindFlag
  ,extendedForallFlag

  -- Tracing and other utilities
  ,dumpEnv
  ,pprPanic
  ,traceTc
  ,pprVarBindingsToBS
  ,sigmaToTauTc
  ,sigmaToRhoTc
  ,rhoToTauTc)
where

import Data.Bifunctor
import Data.Foldable
import Data.List
import Data.Text (pack, Text)
import Data.Text.Lazy.Encoding

import qualified Data.Text.IO as T

import Data.ByteString.Lazy (ByteString)
import Data.ByteString.Builder (toLazyByteString, string7)

import Type

import Term
import Initial

import Utils
import UniqMap
import FV

import Tc.Types
import Tc.Constraint

import Control.Monad
import Control.Exception
import GHC.Stack
import Control.Monad.IO.Class

import System.Directory

import Prettyprinter
import Prettyprinter.Render.Text

-----------------
---- TcLevel ----
-----------------

modifyAmbientTcLevel :: (TcLevel -> TcLevel) -> Env -> Env
modifyAmbientTcLevel f g@(MkEnv { ambientTcLevel = l })
 = g { ambientTcLevel = f l}

setAmbientTcLevel :: TcLevel -> Env -> Env
setAmbientTcLevel lvl env = env { ambientTcLevel = lvl }

pushAmbientTcLevel :: Env -> Env
pushAmbientTcLevel = modifyAmbientTcLevel pushTcLevel

---------------------------------
---- Typechecker environment ----
---------------------------------

dumpEnv :: forall a. Tc (Doc a)
dumpEnv = do
  MkEnv { ambientTcLevel = lvl
        , lastUnique     = uniqRef
        , wantedCts      = wctsRef
        , langExts       = exts } <- getEnv
  wcts <- readTcRef wctsRef
  uniq <- readTcRef uniqRef
  pure . align $ vsep [pretty "Ambient Level:"      <+> pprTcLevel lvl
                      ,pretty "Last unique:"        <+> pprUnique uniq
                      ,pretty "Wanted Constraints:" <+> pprWantedCts wcts
                      ,pretty "Extensions:"         <+> pprList (hsep . map pprLangExt)
                                                                exts]
---------------------
---- Constraints ----
---------------------

captureCts :: Tc r -> Tc (r, WantedCts)
captureCts tc = do
  lvl <- getTcLevel
  traceTc "captureCts:" (pretty "Current TcLevel = " <+> pprTcLevel lvl)
  ctv <- newTcRef emptyWC
  res <- updEnv (\ e -> e { wantedCts = ctv }) tc
  wcs <- readTcRef ctv
  return (res, wcs)

implicCtNeeded :: [(SkolTyVar, Kappa)] -> [Ct] -> Bool
implicCtNeeded skols givens
  | null skols
  , null givens
  = False
  | otherwise
  = True

buildImplicIfNeeded :: [(SkolTyVar, Kappa)] -> [Ct] -> Tc a -> Tc a
buildImplicIfNeeded skols givens tc
  | implicCtNeeded skols givens
  = do (tclvl, wanteds, result) <- pushLevelAndCaptureCts tc
       let impl = buildImplicCt tclvl skols givens wanteds
       emitImplics impl
       traceTc "buildImplicIfNeeded" (vsep $ map pprImplicCt impl)
       pure result

  | otherwise
  = tc

pushLevelAndCaptureCts :: Tc a -> Tc (TcLevel, WantedCts, a)
pushLevelAndCaptureCts tc = do
  tclvl <- getTcLevel
  let tclvl' = pushTcLevel tclvl
  traceTc "pushLevelAndCaptureCts:" (pretty "New TcLevel =" <+> pprTcLevel tclvl')
  (res, wcs) <- updEnv (setAmbientTcLevel tclvl') $ captureCts tc
  pure (tclvl', wcs, res)

getWantedCtsVar :: Tc (TcRef WantedCts)
getWantedCtsVar = wantedCts <$> getEnv

emitSimples :: [Ct] -> Tc ()
emitSimples cts = do
  ctv <- getWantedCtsVar
  modifyTcRef ctv (`addSimples` cts)

emitSimple :: Ct -> Tc ()
emitSimple = emitSimples . (:[])

emitImplic :: ImplicCt -> Tc ()
emitImplic = emitImplics . (:[])

emitWantedCts :: WantedCts -> Tc ()
emitWantedCts wct = unless (isEmptyWC wct) $
  do ctv <- getWantedCtsVar
     modifyTcRef ctv (`andWC` wct)

emitImplics :: [ImplicCt] -> Tc ()
emitImplics cts = unless (null cts) $
  do ctv <- getWantedCtsVar
     modifyTcRef ctv (`addImplics` cts)

-- TODO: Fix me
emitResidualCts :: TcLevel -> [Tau] -> [(SkolTyVar,Kappa)] -> [Ct] -> WantedCts -> Tc ()
emitResidualCts rhs_tclvl taus qtvs givens wanteds
  | isEmptyWC wanteds = return ()
  | otherwise = do
      wanted_simple <- zonkCts (wcSimples wanteds)
      let (outer_simple, inner_simple) = partition is_mono wanted_simple
          is_mono ct = undefined
                 -- see Note [Emitting the residual implication in simplifyInfer]
      lvl <- getTcLevel
      let inner_wanted = wanteds { wcSimples = inner_simple }
      implics <- if isEmptyWC inner_wanted
                 then return []
                 else return $ buildImplicCt rhs_tclvl qtvs givens inner_wanted
      emitWantedCts (MkWC { wcSimples = outer_simple
                          , wcImplics = implics })
-------------------
---- Inference ----
-------------------

-- TODO: Note [Promotion]
-- See Note [Promotion]
promoteTcSigma :: TcLevel -> TcRho -> Tc TcRho
promoteTcSigma lvl rho = do
  curLvl <- getTcLevel
  if curLvl == lvl
    then return rho
    else do tv <- newMetaVarAt lvl typeSigma
            let mv = metaTyVarT tv
            t <- rhoToTauTc rho
            emitSimple $ mkNCEqCt CtWanted (metaTyVarT tv) t
            pure (tauToRho mv)

promoteMetaVarTo :: TcLevel -> MetaTyVar -> Tc Bool
promoteMetaVarTo tclvl tv
  | metaVarLvl tv > tclvl
  = do cloned_tv <- cloneMetaVar tv
       let rhs_tv = cloned_tv { metaVarLvl = tclvl}
       traceTc "promoteMetaVarTo:" (pretty "promoted:" <+> pprMetaVar tv <+>
                                    pretty "to:"      <+> pprMetaVar rhs_tv)
       let mv = metaTyVarT rhs_tv
       writeTcRef (metaVarDetails rhs_tv) (Indirect mv)
       return True
   | otherwise
   = return False

promoteMetaVars :: FVSet MetaTyVar -> Tc Bool
promoteMetaVars freemvs = do
  tclvl <- getTcLevel
  bools <- mapM (promoteMetaVarTo tclvl) freemvs
  return (or bools)

newInferType :: Tc ExpType
newInferType = Infer <$> newInferResult

newInferResult :: Tc InferResult
newInferResult = IR <$> getTcLevel <*> newTcRef Hole

readExpType :: ExpType -> Tc TcSigma
readExpType (Check t) = pure t
readExpType (Infer (IR { irRef = ref})) = do
  res <- readTcRef ref
  case res of
    Filled x -> pure $ rhoToSigma x
    Hole     -> pprPanic "InferResult hasn't been filled in yet!"
                         emptyDoc

expTypeToSigma :: ExpType -> Tc TcSigma
expTypeToSigma (Check sigma)    = pure sigma
expTypeToSigma (Infer inferRes) = rhoToSigma <$> inferResultToRho inferRes

inferResultToRho :: InferResult -> Tc TcRho
inferResultToRho (IR { irRef = ref
                     , irLvl = lvl })
  = do res <- readTcRef ref
       case res of
         Filled rho -> return rho -- TODO: we might need to check the ensureTauType check
         Hole       -> tauToRho . metaTyVarT <$> newMetaVarAt lvl typeSigma

extractInferredResult :: InferResult -> Tc TcRho
extractInferredResult (IR { irRef = ref
                          , irLvl = lvl })
  = do res <- readTcRef ref
       case res of
         Filled rho -> return rho
         Hole       -> pprPanic "extractInferredResult"
                                (pretty "Encountered a Hole!")

inferResultToMetaVar :: InferResult -> Tc Tau
inferResultToMetaVar (IR { irRef = ref
                         , irLvl = lvl })
  = do res <- readTcRef ref
       case res of
         Filled r | TauR t <- r
                  , isMetaTcTvT t -> pure t
                  | otherwise     -> pprPanic "inferResultMetaVar" (pprRho r)
         Hole                     -> metaTyVarT <$> newMetaVarAt lvl typeSigma

tcInfer :: (ExpType -> Tc a) -> Tc (a, TcSigma)
tcInfer tc = do
  rho   <- newInferType
  res   <- tc rho
  resTy <- readExpType rho
  pure (res, resTy)

tcInfer_ :: (ExpType -> Tc ()) -> Tc TcSigma
tcInfer_ tc = snd <$> tcInfer tc

-- TODO: This needs another look
ensureTauType :: TcRho -> Tc ()
ensureTauType (TauR _) = pure ()
ensureTauType rho
  = throwError (IllegalSigmaType
                $ pretty "ensureTauType: Expteded a rho-type but got: "
                <+> pprRho rho)

-- TODO: Note [fillInferResult]
fillInferResult :: TcRho -> InferResult -> Tc ()
fillInferResult tcRho
  (IR { irLvl = lvl
      , irRef = ref }) = do
  inside <- readTcRef ref
  case inside of
    Filled tcRho' ->
      do tcIota <- rhoToTauTc tcRho'
         tcTau <- rhoToTauTc tcRho
         curLvl <- getTcLevel
         unless (curLvl == lvl) $ ensureTauType tcRho
         traceTc "fillInferResult - Filled:"
                 (align . vsep $ [pretty "Current tau:" <+> pprTau tcIota
                                 ,pretty "New tau:"     <+> pprTau tcTau])
         let weqct = mkNCEqCt CtWanted tcIota tcTau
         emitSimple weqct
         traceTc "fillInferResult - emmited equality:" (pprCt weqct)
         writeTcRef ref (Filled tcRho)
    Hole ->
      do tcPhi <- promoteTcSigma lvl tcRho
         traceTc "fillInferResult - Hole:"
                 (align . vsep $ [pretty "Original rho -" <+> pprRho tcRho
                                 ,pretty "Promoted rho -" <+> pprRho tcPhi])
         writeTcRef ref (Filled tcPhi)

newMuVar :: Kappa -> Tc MuTyVar
newMuVar kappa = do
  uniq <- newUnique
  pure $ MkMuTv { muVarText   = pack "mu"
                , muVarUnique = uniq
                , muVarKind   = kappa }

newTyVarMetaVar :: Kappa -> Tc MetaTyVar
newTyVarMetaVar = newMetaVarAtAmbLvl TyVarTv

newTauMetaVar :: Kappa -> Tc MetaTyVar
newTauMetaVar = newMetaVarAtAmbLvl TauTv

newUnique :: Tc Unique
newUnique = do
  lastUnique <- getLastUnique
  modifyTcRef lastUnique (+1)
  readTcRef lastUnique

newTyVar :: Tc TyVar
newTyVar = do
  uniq <- newUnique
  pure $ MkTv { tyVarUnique = uniq
              , tyVarText   = pack "a" }

newSkol :: [(SkolTyVar,Kappa)] -> TyVar -> Tc SkolTyVar
newSkol sks tv = do
  tv' <- if tyVarIsFresh sks tv then pure tv else newTyVar
  lvl <- getTcLevel
  pure $ MkSkolTv { skolTyVar = tv'
                  , skolLvl   = lvl }

newMetaVar :: TcLevel -> MetaInfo -> Kappa -> Tc MetaTyVar
newMetaVar lvl mi kappa = do
  uniq <- newUnique
  ref <- newTcRef Flexi
  pure $ MkMetaTv { metaVarText    = textFromMetaInfo mi
                  , metaVarUnique  = uniq
                  , metaVarLvl     = lvl
                  , metaVarKind    = kappa
                  , metaVarInfo    = mi
                  , metaVarDetails = ref }

newMetaVarAt :: TcLevel -> Kappa -> Tc MetaTyVar
newMetaVarAt lvl = newMetaVar lvl TauTv

newMetaVarAndKind :: Tc (MetaTyVar, TcKappa)
newMetaVarAndKind = do
  kindVar <- newMetaVarOfKindType
  let kappa = (tauToSigma . metaTyVarT) kindVar
  tyVar <- newTauMetaVar kappa
  pure (tyVar, kappa)

newMetaVarAtAmbLvl :: MetaInfo -> Kappa -> Tc MetaTyVar
newMetaVarAtAmbLvl mi kappa = getTcLevel >>= \lvl -> newMetaVar lvl mi kappa

newMetaVarOfKindType :: Tc MetaTyVar
newMetaVarOfKindType = newMetaVarAtAmbLvl TauTv typeSigma

newMetaVarOfKindConstraint :: Tc MetaTyVar
newMetaVarOfKindConstraint = newMetaVarAtAmbLvl TauTv constraintSigma

cloneMetaVar :: MetaTyVar -> Tc MetaTyVar
cloneMetaVar mv = do
  ref <- newTcRef Flexi
  pure $ mv { metaVarDetails = ref }

textFromMetaInfo :: MetaInfo -> Text
textFromMetaInfo CycleBreakerTv = pack "mv-c"
textFromMetaInfo TauTv          = pack "mv-t"
textFromMetaInfo TyVarTv        = pack "mv-tv"

getTcLevel :: Tc TcLevel
getTcLevel = ambientTcLevel <$> getEnv

getLastUnique :: Tc (TcRef Unique)
getLastUnique = lastUnique <$> getEnv

getTraceOutputPath :: Tc (Maybe FilePath)
getTraceOutputPath = traceOutputPath <$> getEnv

----------------------------------------------------------------
---- Gamma, Delta, and Sigma (signatures) utility functions ----
----------------------------------------------------------------

disjointDomDelta :: Delta -> Delta -> Tc ()
disjointDomDelta d1@(MkDelta { scopedTyVars = stys1
                             , skolems      = sks1
                             , variables    = vs1 })
                 d2@(MkDelta { scopedTyVars = stys2
                             , skolems      = sks2
                             , variables    = vs2 } )
  = unless (dom stys1 |#| dom stys2 &&
            dom sks1  |#| dom sks2  &&
            dom vs1   |#| dom vs2) (throwError . ScopingError $ pretty d1 <+> pretty d2)

disjointScopedTyVars :: [(Var, (Tau, Kappa))] -> [(Var, (Tau, Kappa))] -> Tc ()
disjointScopedTyVars stv1 stv2
  = unless (dom stv1 |#| dom stv2) (throwError . ScopingError $ vsep [pprScopedTyVars stv1,pprScopedTyVars stv2])

extendDeltaVars :: Gamma -> [(Var, Sigma)] -> (Gamma -> Tc a) -> Tc a
extendDeltaVars gamma vs tc = tc (modifyDelta (modifyVariables (<> vs)) gamma)

extendSkolems :: Gamma -> [(SkolTyVar,Kappa)] -> (Gamma -> Tc a) -> Tc a
extendSkolems gamma vs tc = tc (modifyDelta (modifySkolems (<> vs)) gamma)

extendDeltaScopedTyVars :: Gamma -> [(Var, (Tau, Kappa))] -> (Gamma -> Tc a) -> Tc a
extendDeltaScopedTyVars gamma vs tc
  = tc (modifyDelta (modifyScopedTyVars (<> vs)) gamma)

lookupVarType :: Delta -> Var -> Either TcError Sigma
lookupVarType dlt var =
  case find (eqFst var) (variables dlt) of
    Nothing    -> notInScope (pprVar var)
    Just (_,x) -> Right x

lookupVarTypeInSigs :: SigmaEnv -> Var -> Either TcError Sigma
lookupVarTypeInSigs sigs var =
  case find (eqFst var) sigs of
    Nothing    -> notInScope (pprVar var)
    Just (_,x) -> Right x

lookupScopedTyVar :: Delta -> Var -> Either TcError (Tau, Kappa)
lookupScopedTyVar dlt var =
  case find (eqFst var) (scopedTyVars dlt) of
    Nothing    -> notInScope (pprVar var)
    Just (_,x) -> Right x

lookupSkolemKind :: Delta -> SkolTyVar -> Either TcError Kappa
lookupSkolemKind dlt skol =
  case find (eqFst skol) (skolems dlt) of
    Nothing    -> notInScope (pprSkol skol)
    Just (_,x) -> Right x

lookupDataCon :: Gamma -> TDataCon -> Either TcError DataConType
lookupDataCon gamma dataCon =
  case find (eqFst dataCon) (dataCons gamma) of
    Nothing    -> notInScope (pprConLike dataCon)
    Just (_,x) -> Right x

lookupPatSyn :: Gamma -> TPatSyn -> Either TcError (PatSynDir, PatSynType)
lookupPatSyn gamma patSyn =
  case find (eqFst patSyn) (patSyns gamma) of
    Nothing    -> notInScope (pprConLike patSyn)
    Just (_,x) -> Right x

lookupTyCon :: Gamma -> TTyCon -> Either TcError (TyCon, Kappa)
lookupTyCon gamma tyCon =
  case find (eqFst tyCon) (tyCons gamma) of
    Nothing    -> notInScope (pprConLike tyCon)
    Just (_,x) -> Right x

data GammaElt result where
  GSkol        :: SkolTyVar -> GammaElt Kappa
  GVar         :: Var -> GammaElt Sigma
  GScopedTyVar :: Var -> GammaElt (Tau, Kappa)
  GDataCon     :: TDataCon -> GammaElt DataConType
  GPatSyn      :: TPatSyn -> GammaElt (PatSynDir, PatSynType)
  GKP          :: ConLike -> GammaElt KPLookupRes
  GTyCon       :: TTyCon -> GammaElt (TyCon, Kappa)

deriving instance Show (GammaElt a)

data KPLookupRes
  = PatSynRes PatSynDir PatSynType
  | DataConRes DataConType
  deriving Show

-- TODO: It might be a good idea to make this the entry exported
-- lookupFunction for
lookupGammaElt :: forall r. Gamma -> GammaElt r -> Either TcError r
lookupGammaElt gamma (GSkol skol)       = lookupSkolemKind (delta gamma) skol
lookupGammaElt gamma (GVar var)         = lookupVarType (delta gamma) var
lookupGammaElt gamma (GScopedTyVar var) = lookupScopedTyVar (delta gamma) var
lookupGammaElt gamma (GDataCon dataCon) = lookupDataCon gamma dataCon
lookupGammaElt gamma (GTyCon tyCon)     = lookupTyCon gamma tyCon
lookupGammaElt gamma (GPatSyn patSyn)   = lookupPatSyn gamma patSyn
lookupGammaElt gamma (GKP conLike)      =
  case uncurry PatSynRes <$> lookupGammaElt gamma (GPatSyn conLike) of
    Left _ -> DataConRes <$> lookupGammaElt gamma (GDataCon conLike)
    x -> x

modifyScopedTyVars :: ([(Var, (Tau, Kappa))] -> [(Var, (Tau, Kappa))] ) -> Delta -> Delta
modifyScopedTyVars f d@(MkDelta { scopedTyVars = stvs}) = d { scopedTyVars = f stvs }

modifySkolems :: ([(SkolTyVar, Kappa)] -> [(SkolTyVar, Kappa)]) -> Delta -> Delta
modifySkolems f d@(MkDelta { skolems = sks}) = d { skolems = f sks }

modifyVariables :: ([(Var, Sigma)] -> [(Var, Sigma)]) -> Delta -> Delta
modifyVariables f d@(MkDelta { variables = vs}) = d { variables = f vs }

modifyDelta :: (Delta -> Delta) -> Gamma -> Gamma
modifyDelta f g@(MkGamma { delta = d}) = g { delta = f d }

-----------------
---- Zonkers ----
-----------------

-- TODO: Ask Simon: Should I turn this into a class?
-- TODO: Note [Zonking]

zonkMetaVar :: MetaTyVar -> Tc TcTau
zonkMetaVar mv@(MkMetaTv { metaVarDetails = ref }) = do
  dets <- readTcRef ref
  case dets of
    Flexi -> do zmv <- fmap metaTyVarT (zonkMetaVarKind mv)
                traceTc "Flexi after zonkMetaVarKind: " (pprTau zmv)
                pure zmv
    Indirect t -> do zt <- zonkTau t
                     writeTcRef ref (Indirect zt)
                     traceTc "zonkMetaVar"
                             (vsep [pretty "Indirect:"      <+> pprTau t
                                   ,pretty "After zonking:" <+> pprTau zt])
                     pure zt

zonkMetaVarKind :: MetaTyVar -> Tc MetaTyVar
zonkMetaVarKind mv@(MkMetaTv { metaVarKind = ka })
  = do zka <- zonkSigma ka
       pure $ mv { metaVarKind = zka}

zonkSigma :: TcSigma -> Tc TcSigma
zonkSigma (RhoSg rho)              = RhoSg        <$> zonkRho rho
zonkSigma (QuantifiedSg psi sigma) = QuantifiedSg <$> zonkPsi psi <*> zonkSigma sigma
  where
    zonkPsi (Specified (a, ka)) = Specified . (a,) <$> zonkSigma sigma
    zonkPsi (Inferred (a, ka))  = Inferred  . (a,) <$> zonkSigma sigma
    zonkPsi (FatArrow q)        = FatArrow <$> zonkTau q

zonkTau :: TcTau -> Tc TcTau
zonkTau (ArrowT t1 t2)    = ArrowT <$> zonkTau t1 <*> zonkTau t2
zonkTau (AppT t1 t2)      = AppT   <$> zonkTau t1 <*> zonkTau t2
zonkTau t@(TyConT _)      = pure t
zonkTau t@(AlphaT _)      = pure t
zonkTau t@(TcTyVarT tctv) = case tctv of
  MetaTcTv mv -> zonkMetaVar mv -- See Note [Meta variables]
  _               -> pure t

zonkRho :: TcRho -> Tc TcRho
zonkRho (TauR tau)              = TauR        <$> zonkTau tau
zonkRho (AppR sigma phi)        = AppR        <$> zonkSigma sigma <*> zonkSigma phi
zonkRho (QuantifiedR psi sigma) = QuantifiedR <$> zonkPsi psi <*> zonkSigma sigma
  where
    zonkPsi (ArrowR tau)       = ArrowR          <$> zonkSigma tau
    zonkPsi (Required (a, ka)) = Required . (a,) <$> zonkSigma ka

zonkCt :: Ct -> Tc Ct
zonkCt ct@(MkCt { ctPred = q }) = do
  -- zonked <- zonkTau q
  let zonked = q -- TODO: Fix me
  pure $ ct { ctPred = zonked }

zonkCts :: [Ct] -> Tc [Ct]
zonkCts = mapM zonkCt

zonkWantedCts :: WantedCts -> Tc WantedCts
zonkWantedCts (MkWC { wcImplics = impls
                    , wcSimples = cts }) = do
  zImpls <- zonkImplicCts impls
  zCts <- zonkCts cts
  pure (MkWC { wcImplics = zImpls
             , wcSimples = zCts })

zonkImplicCt :: ImplicCt -> Tc ImplicCt
zonkImplicCt (MkImplic { implTcLvl   = lvl
                       , implSkolems = skols
                       , implGivens  = givens
                       , implWanteds = wanteds }) = do
  zSkols   <- traverse (sequence . second zonkSigma) skols
  zGivens  <- zonkCts givens
  zWanteds <- zonkWantedCts wanteds
  pure (MkImplic { implTcLvl   = lvl
                 , implSkolems = zSkols
                 , implGivens  = zGivens
                 , implWanteds = zWanteds })

zonkImplicCts :: [ImplicCt] -> Tc [ImplicCt]
zonkImplicCts = mapM zonkImplicCt

-----------------------------
---- Language extensions ----
-----------------------------

getLangExts :: Tc [LangExt]
getLangExts = langExts <$> getEnv

langFlag :: LangExt -> Tc Bool
langFlag l = elem l <$> getLangExts

deepSubFlag :: Tc Bool
deepSubFlag = langFlag XDeepSubsumption

tyAbsFlag :: Tc Bool
tyAbsFlag = langFlag XTypeAbstractions

patSigBindFlag :: Tc Bool
patSigBindFlag = langFlag XPatSigBind

extendedForallFlag :: Tc Bool
extendedForallFlag = langFlag XExtendedForallScope

-------------------
---- Utilities ----
-------------------

sigmaToTauTc :: Sigma -> Tc Tau
sigmaToTauTc = liftEither . sigmaToTau

sigmaToRhoTc :: Sigma -> Tc Rho
sigmaToRhoTc = liftEither . sigmaToRho

rhoToTauTc :: Rho -> Tc Tau
rhoToTauTc = liftEither . rhoToTau

pprPanic :: HasCallStack => String -> Doc Text -> a
pprPanic s doc = throw (PprPanic $ vcat [pretty s ,doc, pretty $ prettyCallStack callStack])


-- traceTc :: forall a. String -> Doc a -> Tc ()
-- traceTc fname message = liftIO . putDoc . (<+> line)
--                       $ vcat [pretty fname <+> lbrace
--                              ,indent 1 message
--                              ,rbrace]

-- TODO: I kind of feel like this is going to be incredibly inefficient
-- Long story short: it is.
traceTc :: forall a. String -> Doc a -> Tc ()
traceTc fname message = do
  path <- getTraceOutputPath
  case path of
    Nothing -> pure ()
    Just x -> liftIO $
      do actPath <- makeAbsolute x
         exists <- doesPathExist actPath
         let prettyMessage
               = renderStrict
               . layoutPretty defaultLayoutOptions
               . (<+> line)
               $ vcat [pretty fname <+> lbrace
                      ,indent 1 message
                      ,rbrace]
         if exists
           then T.appendFile actPath prettyMessage
           else T.writeFile actPath prettyMessage

pprVarBindingsToBS :: [(Var, Sigma)] -> ByteString
pprVarBindingsToBS = encodeUtf8 . renderLazy
                   . layoutPretty defaultLayoutOptions
                   . pprTidiedVarBindings
