module Tc.Subst where

import UniqMap

import BasicTypes

import {-# SOURCE #-} Type

type LTheta = Subst MuTyVar Tau
type BTheta = Subst MuTyVar Sigma

type LOmega = Subst TyVar Tau
type BOmega = Subst TyVar Sigma

type LOmegaSk = Subst SkolTyVar Tau
type BOmegaSk = Subst SkolTyVar Sigma


class Substitutable a where
  appBOmega  :: BOmega   -> a -> Sigma
  appLOmega  :: LOmega   -> a -> a

  appBOmegaSk :: BOmegaSk -> a -> Sigma
  appLOmegaSk :: LOmegaSk -> a -> a

  appBTheta   :: BTheta   -> a -> Sigma
  appLTheta   :: LTheta   -> a -> a


type Subst      = UniqMap
