-- | Substitution

module Tc.Subst
  (LOmega     -- \omega
  ,BOmega     -- \Omega
  ,LTheta     -- \theta
  ,BTheta     -- \Theta
  ,BOmegaSk
  ,LOmegaSk
  ,Substitutable(..)
  ,Subst
  ,extendSubst
  ,inSubstDom
  ,inSubstRng
  ,substDom
  ,substRng
  ,composeSubsts
  ,fromListSubst
  ,mkSingSubst
  ,lookupSubst
  ,emptySubst)
where

import BasicTypes

import UniqMap

import {-# SOURCE #-} Type

type Subst = UniqMap

extendSubst :: Uniquable d => Subst d r -> d -> r -> Subst d r
extendSubst m from to = addToUM m from to

inSubstDom :: forall d r. d -> Subst d r -> Bool
inSubstDom = undefined

inSubstRng :: forall d r. r -> Subst d r -> Bool
inSubstRng = undefined

substDom :: forall d r. Subst d r -> [d]
substDom = undefined

substRng :: forall d r. Subst d r -> [r]
substRng = undefined

composeSubsts :: forall d r. Subst d r -> Subst d r -> Subst d r
composeSubsts s1 s2 = unionUM s1 s2

lookupSubst :: forall d r. Uniquable d => Subst d r -> d -> Maybe r
lookupSubst s k = lookupUM k s

fromListSubst :: forall d r. Uniquable d => [(d,r)] -> Subst d r
fromListSubst = fromListUM

mkSingSubst :: forall d r. Uniquable d => d -> r -> Subst d r
mkSingSubst from to = fromListSubst [(from,to)]

emptySubst :: Subst d r
emptySubst = emptyUM

type LTheta = Subst MuTyVar Tau
type BTheta = Subst MuTyVar Sigma

type LOmega = Subst TyVar Tau
type BOmega = Subst TyVar Sigma

type LOmegaSk = Subst SkolTyVar Tau
type BOmegaSk = Subst SkolTyVar Sigma

class Substitutable a where
  appBOmega  :: BOmega   -> a -> Sigma
  appLOmega  :: LOmega   -> a -> a

  appBOmegaSk :: BOmegaSk -> a -> Sigma
  appLOmegaSk :: LOmegaSk -> a -> a

  appBTheta   :: BTheta   -> a -> Sigma
  appLTheta   :: LTheta   -> a -> a
