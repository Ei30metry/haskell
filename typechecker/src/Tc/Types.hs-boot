module Tc.Types
  (module BasicTypes
  ,TcLevel
  ,pushTcLevel
  ,topTcLevel
  ,isTopTcLevel
  ,maxTcLevel
  ,minTcLevel
  ,pprTcLevel
  ,Gamma(..)
  ,SigmaEnv
  ,Tc
  ,Delta(..)
  ,emptyDelta
  ,appendDelta
  ,PatSynDir(..)
  ,throwError
  ,notInScope
  ,LangExt(..)
  ,ExpType(..)
  ,InferHole(..)
  ,InferResult(..)
  ,TcRef
  ,TvBinding
  ,TcError(..)) where

import BasicTypes
import Data.IORef
import {-# SOURCE #-} Type
import Term
import Data.Text (Text)
import Control.Monad
import Control.Monad.IO.Class
import Prettyprinter

newtype TcLevel = Lvl Int

instance Eq TcLevel
instance Show TcLevel

pushTcLevel :: TcLevel -> TcLevel

topTcLevel :: TcLevel

isTopTcLevel :: TcLevel -> Bool

maxTcLevel :: TcLevel -> TcLevel -> TcLevel

minTcLevel :: TcLevel -> TcLevel -> TcLevel

pprTcLevel :: forall a. TcLevel -> Doc a

newtype Tc a = MkTc { unTc :: (Env -> IO (Either TcError a)) }

instance MonadIO Tc

instance Monad Tc

instance Applicative Tc

instance Functor Tc

data TcError
  = NotInScopeError (Doc Text)
  | UntouchableMetaVar (Doc Text)
  | MuVarInCS (Doc Text)
  | ScopingError (Doc Text)
  | PolyTypeInConstraintError (Doc Text)
  | IllegalTermError (Doc Text)
  | ArgDoesntMatchQuantifer (Doc Text)
  | ImplementationBug (Doc Text)
  | IllegalArgumentPattern (Doc Text)
  | ExpectedLessArguments (Doc Text)
  | KindCheckingError (Doc Text)
  | KindingError (Doc Text)
  | WrongDataConType (Doc Text)
  | WrongPatSynType (Doc Text)
  | IllegalSigmaType (Doc Text)
  | ShapeMismatch (Doc Text)
  | OccursCheck (Doc Text)
  | UnsupportedCt (Doc Text)
  | DifferentTyCons (Doc Text)
  | UnaccompaniedSigs (Doc Text)
  | IllegalCaseLHS (Doc Text)
  | ProbablyNotZonked (Doc Text)
  | ProbablyNotFilledIn (Doc Text)
  | ArityMisMatch (Doc Text)
  | CantSolveUnmatchedPsis (Doc Text)
  | InsolubleWC (Doc Text)
  | CantConvertToRho (Doc Text)
  | CantConvertToTau (Doc Text)
  | PprPanic (Doc Text)

instance Show TcError

data Env

-- \Gamma in the spec
data Gamma

instance Show Gamma

data Delta

instance Show Delta

type SigmaEnv = [(Var, Sigma)]

emptyDelta :: Delta

emptyGamma :: Gamma

instance Semigroup Delta

instance Monoid Delta

instance Semigroup Gamma

instance Monoid Gamma

appendGamma :: Gamma -> Gamma -> Gamma

appendDelta :: Delta -> Delta -> Delta

data PatSynDir

instance Eq PatSynDir
instance Show PatSynDir

data LangExt

instance Eq LangExt
instance Show LangExt

data ExpType

data InferHole

instance Show InferHole

data InferResult

newtype TcRef a = MkTcRef { unTcRef :: IORef a }

type TvBinding = (Var, TyVar, Kappa)

throwError :: forall a. TcError -> Tc a

notInScope :: forall a. Doc Text -> Either TcError a
