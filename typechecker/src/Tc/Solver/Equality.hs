
-- | Equality constraints

module Tc.Solver.Equality (
  -- Canonicalize equality constraints. See Note [Canonicalization]
  canonEq
) where

import Tc.Constraint
import Tc.Monad
import Tc.Solver.Monad
import UniqMap
import FV
import Utils
import Type
import Prettyprinter

-- TODO: Might also have a given
-- TODO: Elaborate on the explanation. do the same thing for filled in MetaDetails (kinda like having a given constraint)
-- [F] a ~ wombat
-- [G] a ~ Int
-- canonEq [F] Int ~ wombat

-- TODO: Note [Canonicalization]

-- TODO: Explain what canonEq does clearly
canonEq :: CtFlavour -> TcTau -> TcTau -> Cs ([CanIrredCt], [CanEqCt])
canonEq flv (TyConT tc1) (TyConT tc2)
  | tc1 `eqTyCon` tc2
  = do traceCs "Solved by reflexivity:" (pprAsEq flv (TyConT tc1) (TyConT tc2))
       pure ([],[])
  | otherwise
  = throwErrorCs (DifferentTyCons $ pretty "Can't unify different tycons:"
                                  <+> pprTyCon tc1 <+> pretty "and" <+> pprTyCon tc2)
-- This equation is completely redundant and deals with the very common (->) (->) case more efficiently
canonEq flv t1@(ArrowT tau1 io1) t2@(ArrowT tau2 io2) = do
  traceCs "canonEq - Arrow, Arrow:"
          (vsep [pretty "Original:" <+> pprAsEq flv t1 t2
                ,pretty "Decomposed into:" <+> (align $ vsep [pprAsEq flv tau1 tau2
                                                             ,pprAsEq flv io1 io2])])
  (argIrreds,argCans) <- canonEq flv tau1 tau2
  (resIrreds,resCans) <- canonEq flv io1 io2
  pure (argIrreds ++ resIrreds, argCans ++ resCans)
-- This equation is completely redundant and deals with the very common (App) (App) case more efficiently
canonEq flv t1@(AppT tau1 io1) t2@(AppT tau2 io2) = do
  traceCs "canonEq - App, App:"
          (vsep [pretty "Original:" <+> pprAsEq flv t1 t2
                ,pretty "Decomposed into:" <+> (align $
                                                vsep [pprAsEq flv tau1 tau2
                                                     ,pprAsEq flv io1 io2])])
  (argIrreds,argCans) <- canonEq flv tau1 tau2
  (resIrreds,resCans) <- canonEq flv io1 io2
  pure (argIrreds ++ resIrreds, argCans ++ resCans)
-- TODO [Decomposing ]
canonEq flv t1 t2
  | Just (tau1, io1) <- splitAppArrowT t1
  , Just (tau2, io2) <- splitAppArrowT t2
  = do
  traceCs "canonEq - AppT or ArrowT, AppT or ArrowT:"
          (vsep [pretty "Original:" <+> pprAsEq flv t1 t2
                ,pretty "Decomposed into:" <+> (align $
                                                vsep [pprAsEq flv tau1 tau2
                                                     ,pprAsEq flv io1 io2])])
  (argIrreds,argCans) <- canonEq flv tau1 tau2
  (resIrreds,resCans) <- canonEq flv io1 io2
  pure (argIrreds ++ resIrreds, argCans ++ resCans)
-- See Note [Unification preconditions]
canonEq CtWanted (TcTyVarT (MetaTcTv mv)) t = unifyIfTouchable mv t
canonEq CtGiven (TcTyVarT (MetaTcTv mv)) t  = undefined -- TODO: Note [Never unify givens]
canonEq flv t m@(TcTyVarT (MetaTcTv _))     = do -- See Note [Swaping equalities]
  traceCs "Swaping equality:"
          $ vsep [pretty "from:" <+> pprTau t <+> pretty '~' <+> pprTau m
                 ,pretty "to:" <+> pprTau m <+> pretty '~' <+> pprTau t]
  canonEq flv m t -- TODO: swapIfNeeded flv t m

canonEq _ (TcTyVarT (MuTcTv vu)) _
  = throwErrorCs (MuVarInCS
                 $ pretty "An instantiation variable in the constraint solver:"
                 <+> pprMuVar vu)
canonEq _ _ (TcTyVarT (MuTcTv vu))
  = throwErrorCs (MuVarInCS
                 $ pretty "An instantiation variable in the constraint solver:"
                 <+> pprMuVar vu)
canonEq flv t1 t2 = pure ([(MkIrredCt { irredCt     = mkNCEqCt flv t1 t2
                                      , irredReason = ShapeMismatchReason })]
                         ,[])

isTouchableMetaVar :: MetaTyVar -> TcTau -> Cs Bool
isTouchableMetaVar mv@(MkMetaTv { metaVarLvl = lvl }) t = do
      innerMost <- getInertGivenEqLvl
      let skolEscape = anyUM (\mv -> lvl < metaVarLvl mv) (fmv t)
                    || anyUM (\sk -> lvl < skolLvl sk) (fsv t)
      if ((lvl < innerMost) && skolEscape)
        then do traceCs "isTouchableMetaVar" (pretty "Untouchable:" <+>
                                              pprMetaVar mv)
                pure False
        else do traceCs "isTouchableMetaVar" (pretty "Touchable:" <+>
                                              pprMetaVar mv)
                pure True

unifyIfTouchable :: MetaTyVar -> TcTau -> Cs ([CanIrredCt], [CanEqCt])
unifyIfTouchable mv t
  = ifM (isTouchableMetaVar mv t) (unifyMetaVar mv t >> pure ([],[])) throw
  where
    eqCt  = mkNCEqCt CtWanted (metaTyVarT mv) t
    throw = throwErrorCs $ UntouchableMetaVar (pprMetaVar mv <+>
                                               pretty "Arising from:" <+>
                                               pprCt eqCt)

-- TODO Note [Swaping equalities]
swapIfNeeded :: CtFlavour -> TcTau -> MetaTyVar -> Cs ([CanIrredCt], [CanEqCt])
swapIfNeeded flv lhs mv = undefined -- unifyIfTouchable mv

{- Note [Unification preconditions]
TODO
-}

-- NOTE: Used for families: a ~ F a should become F a ~ a
-- orient :: CtFlavour -> TcSigma -> TcSigma -> Ct
-- orient flv sg mv@(MetaTyVar (MkMetaTv { metaVarLvl = lvl1 }))
--   | Alpha {}  <- sg
--   , CtWanted <- flv
--   = mkNCEqCt CtWanted mv sg
--   | MetaTyVar v <- sg
--   , MkMetaTv { metaVarLvl = lvl2 } <- v
--   , lvl2 > lvl1
--   = mkNCEqCt flv sg mv
--   | otherwise
--   = mkNCEqCt flv mv sg
-- orient flv sg1 sg2 = mkNCEqCt flv sg1 sg2
