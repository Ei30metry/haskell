-- | The Constraint Solver monad

module Tc.Solver.Monad (
  -- The constraint solver monad
  Cs, runCs, throwErrorCs, traceCs, liftTc,
  CsEnv(..), getCsEnv, getUnifLvl, getWorkList,
  getUnsolvedInerts, getInertSet, resetUnifFlag,
  setUnifFlag, setInertSet,

  -- The inert set. See Note [InertSet]
  InertSet(..), emptyIS, isEmptyIS, getInertGivenEqLvl,
  extendInertSet, updInertEqs, updInertSet, updGivenEqs,

  -- Constraints we plan to solve. See Note [WorkList]
  WorkList(..), emptyWL, isEmptyWL, appendWL, selectWorkItem,
  updWorkList, extendWLCt, extendWLCts,

  -- Mutable references in the constraint solver
  CsRef, writeCsRef, readCsRef, newCsRef, modifyCsRef,

  -- Emiting constraints to the Cs Monad
  emitImplicsToWL, emitSimplesToWL,

  -- Unification
  unifyMetaVar,
)
where

import Control.Monad
import Tc.Constraint
import Tc.Monad
import Control.Monad.IO.Class
import Type
import FV
import Prettyprinter

{- Note [extending inertEqs]
-}

newtype Cs a = MkCs { unCs :: CsEnv -> Tc a }

instance Functor Cs where
  fmap f (MkCs cont) = MkCs $ \csenv -> fmap f (cont csenv)

instance Applicative Cs where
  pure x = MkCs $ const (return x)
  (<*>)  = ap

instance Monad Cs where
  m >>= k = MkCs $ \csenv -> do
    unCs m csenv >>= \r -> unCs (k r) csenv

instance MonadIO Cs where
  liftIO io = MkCs $ const (liftIO io)

type CsRef = TcRef

liftTc :: forall a. Tc a -> Cs a
liftTc tc = MkCs $ const tc

writeCsRef :: forall a. CsRef a -> a -> Cs ()
writeCsRef ref = liftTc . writeTcRef ref

readCsRef :: forall a. CsRef a -> Cs a
readCsRef = liftTc . readTcRef

newCsRef :: forall a. a -> Cs (CsRef a)
newCsRef = liftTc . newTcRef

modifyCsRef :: forall a. CsRef a -> (a -> a) -> Cs ()
modifyCsRef ref = liftTc . modifyTcRef ref

throwErrorCs :: forall a. TcError -> Cs a
throwErrorCs = liftTc . throwError

{- Note [Unification level flag]
-}

data CsEnv =
  MkCsEnv
    { cseInerts   :: CsRef InertSet -- See Note [InertSet]
    , cseWorkList :: CsRef WorkList -- See Note [WorkList]
    , cseUnifLvl  :: CsRef (Maybe TcLevel)  -- See Note [The Unification Level Flag]
    }

{- Note [InertSet]
The InertSet contains:
  - inertEqs: The canonicalized constraints that we can leverage during solving
  - inertIrreds: The constarints that we couldn't canonicalize
  - inertGivenEqLvl: The TcLevel of the innermost implication that has a Given
                     equality of the sort that make a unification variable
-}

data InertSet =
  MkIS
    { inertEqs        :: [CanEqCt]      -- Canonnicals. see Note [inertEqs invariants]
    , inertIrreds     :: [CanIrredCt]
    , inertGivenEqLvl :: TcLevel
    }
    deriving Show

{- Note [WorkList]
The WorkList contains the constraints that we are planning to solve.
-}
data WorkList =
  MkWL
    { wlEqs        :: [Ct]
    , wlRest       :: [Ct]
    , wlResImplics :: [ImplicCt]
    }
  deriving Show

instance Semigroup WorkList where
  (<>) = appendWL

instance Monoid WorkList where
  mempty = emptyWL
  mappend = (<>)

emptyIS :: InertSet
emptyIS
  = MkIS { inertEqs        = []
         , inertIrreds     = []
         , inertGivenEqLvl = topTcLevel }

isEmptyIS :: InertSet -> Bool
isEmptyIS (MkIS { inertEqs    = eqs
                , inertIrreds = irreds })
  = null eqs && null irreds

emptyWL :: WorkList
emptyWL = MkWL { wlEqs        = []
               , wlRest       = []
               , wlResImplics = [] }

isEmptyWL :: WorkList -> Bool
isEmptyWL (MkWL { wlEqs        = eqs
                , wlRest       = rest
                , wlResImplics = impls })
  = null eqs && null rest && null impls

appendWL :: WorkList -> WorkList -> WorkList
appendWL (MkWL { wlEqs        = eqs1
               , wlRest       = rest1
               , wlResImplics = implics1 })
         (MkWL { wlEqs        = eqs2
               , wlRest       = rest2
               , wlResImplics = implics2 })
  = MkWL { wlEqs        = eqs1 ++ eqs2
         , wlRest       = rest1 ++ rest2
         , wlResImplics = implics1 ++ implics2 }

runCs :: Cs a -> Tc a
runCs cs = do
  inerts  <- newTcRef emptyIS
  wl      <- newTcRef emptyWL
  unifLvl <- newTcRef Nothing
  unCs cs (MkCsEnv { cseInerts   = inerts
                   , cseWorkList = wl
                   , cseUnifLvl = unifLvl })

getCsEnv :: Cs CsEnv
getCsEnv = MkCs pure

getUnifLvl :: Cs (CsRef (Maybe TcLevel))
getUnifLvl = cseUnifLvl <$> getCsEnv

getWorkList :: Cs (CsRef WorkList)
getWorkList = cseWorkList <$> getCsEnv

getUnsolvedInerts :: Cs ([ImplicCt] , [Ct] )
getUnsolvedInerts = pure ([],[]) -- TODO: Fix me

-- Equality constarints have the highest priority
-- in the solver pipeline
selectWorkItem :: WorkList -> Maybe (Ct, WorkList)
selectWorkItem wl@(MkWL { wlEqs  = eqs
                        , wlRest = rest })
  | ct : cts <- eqs
  = Just (ct, wl { wlEqs = cts })
  | ct : cts <- rest
  = Just (ct, wl { wlRest = cts })
  | otherwise
  = Nothing

getInertSet :: Cs (CsRef InertSet)
getInertSet = cseInerts <$> getCsEnv

getInertGivenEqLvl :: Cs TcLevel
getInertGivenEqLvl = do
  ref <- getInertSet
  is <- readCsRef ref
  pure (inertGivenEqLvl is)

{- Note [Tracking given equailities]
TODO: Explain
-}

{-
Note [Let-bound skolems]
~~~~~~~~~~~~~~~~~~~~~~~~
If   * the inert set contains a canonical Given CEqCan (a ~ ty)
and  * 'a' is a skolem bound in this very implication,

then:
   a) The Given is pretty much a let-binding, like
         f :: (a ~ b->c) => a -> a
      Here the equality constraint is like saying
         let a = b->c in ...
      It is not adding any new, local equality  information,
      and hence can be ignored by has_given_eqs

   b) 'a' will have been completely substituted out in the inert set,
      so we can safely discard it.
-}

-- See Note [Tracking given equalities]
extendInertSet :: TcLevel -> CanEqCt -> InertSet -> InertSet
extendInertSet lvl ect@(MkCanEqCt { eqPred = ct }) is@(MkIS { inertEqs = eqs })
  | not (isGiven ct)
  || isLetBoundSkolemCt lvl ct
  = is { inertEqs = ect : eqs }
  | otherwise
  = is { inertEqs        = ect : eqs
       , inertGivenEqLvl = lvl }

updInertEqs :: CanEqCt -> Cs ()
updInertEqs ct = do
  lvl <- liftTc getTcLevel
  traceCs "updInertEqs:" (pprCanEqCt ct)
  updInertSet (extendInertSet lvl ct)

resetUnifFlag :: Cs Bool
resetUnifFlag = do
  ref <- getUnifLvl
  curLevel <- liftTc getTcLevel
  val <- readCsRef ref
  case val of
    Nothing  -> return False
    Just lvl -> if curLevel > lvl
                then return False
                else do writeCsRef ref Nothing
                        return True

setUnifFlag :: TcLevel -> Cs ()
setUnifFlag lvl
  = do ref <- getUnifLvl
       val <- readCsRef ref
       case val of
         Just unif_lvl
           | lvl >= unif_lvl -> return ()
         _                   -> writeCsRef ref (Just lvl)

updInertSet :: (InertSet -> InertSet) -> Cs ()
updInertSet f = do
  ref <- getInertSet
  is <- readCsRef ref
  writeCsRef ref (f is)

updWorkList :: (WorkList -> WorkList) -> Cs ()
updWorkList f = do
  ref <- getWorkList
  wl <- readCsRef ref
  writeCsRef ref (f wl)

emitImplicsToWL :: [ImplicCt] -> Cs ()
emitImplicsToWL = undefined

setInertSet :: InertSet -> Cs ()
setInertSet is = do
  ref <- getInertSet
  writeCsRef ref is

extendWLCt :: Ct -> WorkList -> WorkList
extendWLCt ct@(MkCt { ctPred = q } ) wl@(MkWL { wlEqs  = eqs , wlRest = rest })
  | isEqPredQ q
  = wl { wlEqs = ct : eqs }
  | otherwise
  = wl { wlRest = ct : rest }

extendWLCts :: [Ct] -> WorkList -> WorkList
extendWLCts cts wl = foldr extendWLCt wl cts

emitSimplesToWL :: [Ct] -> Cs ()
emitSimplesToWL cts
  | null cts
  = return ()
  | otherwise
  = updWorkList (extendWLCts cts)

-- TODO: Precondition: metavar is touchable.
unifyMetaVar :: MetaTyVar -> TcTau -> Cs ()
unifyMetaVar mv@(MkMetaTv { metaVarDetails = ref
                          , metaVarLvl     = lvl }) tau
  | TcTyVarT (MetaTcTv mv') <- tau
  , mv `eqMetaTyVar` mv'
  = do traceCs "unifyMetaVar - solved by reflexivity:"
               (pprAsEq CtWanted (metaTyVarT mv)
                                 (metaTyVarT mv'))
       pure ()
  | otherwise
  = do mvd <- readCsRef ref
       writeCsRef ref (Indirect tau)
       traceCs "unifyMetaVar"
         . align
         . vsep
         $ [pretty "Previous MetaVarDetails:" <+> pprMetaDetails mvd
           ,pretty "Writing:" <+> pprMetaVar mv <+> pretty ":=" <+> pprTau tau]
       curLvl <- liftTc $ getTcLevel
       unless (lvl == curLvl) $ mapM_ (liftTc . promoteMetaVarTo lvl) (fmv tau)

updGivenEqs :: TcLevel -> Ct -> InertSet -> InertSet
updGivenEqs lvl ct inerts
  | not (isGiven ct)
  || isLetBoundSkolemCt lvl ct
  = inerts
  | otherwise
  = inerts { inertGivenEqLvl = lvl }

isLetBoundSkolemCt :: TcLevel -> Ct -> Bool
isLetBoundSkolemCt curLvl (MkCt { ctPred = q, ctFlavour = CtGiven })
  | Just (TcTyVarT sk, _) <- splitEqPredT q
  , SkolTcTv (MkSkolTv { skolLvl = lvl }) <- sk
  = lvl > curLvl
isLetBoundSkolemCt _ _ = False

-- Should set do the 'setUnificationFlag lvl' stuff
kickOutAfterUnification mv = undefined

kickOutRewritable = undefined

data KickOutReason
  = KOUnif MetaTyVar
  | KOCanon TcSigma

traceCs :: forall a. String -> Doc a -> Cs ()
traceCs str doc = liftTc (traceTc str doc)
