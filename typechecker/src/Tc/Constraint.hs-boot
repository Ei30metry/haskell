module Tc.Constraint where

import Tc.Subst
import {-# SOURCE #-} Type

data Ct

instance Show Ct

data CtFlavour

data WantedCts

instance Show WantedCts

data ImplicCt

emptyWC :: WantedCts

mkSimpleWC :: CtFlavour -> [Q] -> WantedCts

isWanted :: Ct -> Bool

isGiven :: Ct -> Bool

mkNCEqCt :: CtFlavour -> TcTau -> TcTau -> Ct

mkCt :: CtFlavour -> TcQ -> Ct
