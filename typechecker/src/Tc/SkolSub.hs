-- | Skolemisation and subtyping

module Tc.SkolSub (
  -- Skolemisation
  tcSkol,tcSkolAPats,
  -- Subsumption
  tcSub,

  tcBindTvs
  ) where

import Term
import Type

import Tc.Constraint

import Tc.Types
import Tc.Monad
import Tc.Gen.Type

import Utils

import Tc.Subst
import Data.Bifunctor

import FV

import Prettyprinter

{- *********************************************************************
*                                                                      *
*                  Gamma |-skol sigma ~> Delta ; rho                   *
*                                                                      *
********************************************************************* -}

tcSkol :: Gamma -> Bool -> Sigma -> Tc ([(SkolTyVar, Kappa)], [Q] ,Rho)
tcSkol gamma flg (QuantifiedS psi sigma)

-- | Skol-InvisForall
  | isInvisForallP psi
  , Just (alpha, kappa) <- forallQuantifieP psi
  = do sk <- newSkol (skolems . delta $ gamma) alpha
       let tctv = (skolTyVarT sk)
       (vs, qs, rho) <- tcSkol gamma flg (appLOmega (mkSingSubst alpha tctv) sigma)
       pure (snoc (sk, kappa) vs, qs, rho)

  -- | Skol-Constrint
  | FatArrowP _ <- psi
  = tcSkol gamma flg sigma

  -- | Skol-DeepFun
  | ArrowP phi <- psi
  , flg
  = do (bs1,qs1,rho) <- tcSkol gamma flg sigma
       let returnType = QuantifiedR (ArrowR phi) (rhoToSigma rho)
       pure (bs1,qs1, returnType)

-- | Skol-Base
tcSkol _ _ (RhoSg rho) = pure ([], [], rho)

tcSkol _ _ sg = pprPanic "tcSkol"
                         (pretty "Expected a rho-type but got:" <+>
                          pprSigma sg)

{- *********************************************************************
*                                                                      *
*                     Gamma |-sub sigma <= phi                         *
*                                                                      *
********************************************************************* -}

tcSub :: Gamma -> Bool -> Sigma -> Phi -> Tc ()

-- | Sub-Sub
tcSub gamma flg sg ph = do
  traceTc "Sub-Sub:" (vcat [pretty "Sigma:" <+> pprSigma sg
                           ,pretty "Phi:"   <+> pprSigma ph])
  (tvKinds, qs, rho) <- tcSkol gamma flg ph
  emitSimples $ map (mkCt CtGiven) qs
  rho' <- tcSubInst (modifyDelta (modifySkolems (tvKinds <>)) gamma) flg sg
  tcSub_rr (modifyDelta (modifySkolems (tvKinds <>)) gamma) flg rho' rho

{- *********************************************************************
*                                                                      *
*                     Gamma |-sub_rr rho <= rho'                       *
*                                                                      *
********************************************************************* -}

tcSub_rr :: Gamma -> Bool -> Rho -> Rho -> Tc ()

-- | Sub-DeepFun
tcSub_rr gamma True (QuantifiedR (ArrowR sg1) (RhoSg rho1)) (QuantifiedR (ArrowR sg2) (RhoSg rho2))
  = do tcSub gamma True sg2 sg1
       tcSub_rr gamma True rho1 rho2

-- | Sub-Base
tcSub_rr _ _ (TauR t1) (TauR t2) = emitSimple $ mkNCEqCt CtWanted t1 t2

tcSub_rr _ _ rho1 rho2
  = pprPanic "tcSub_rr" (pretty "Expected taus but got:" <+>
                         pprRho rho1 <+> pretty "<=" <+> pprRho rho2)

{- *********************************************************************
*                                                                      *
*                     Gamma |-sub_inst sigma ~> rho                    *
*                                                                      *
********************************************************************* -}

tcSubInst :: Gamma -> Bool -> Sigma -> Tc TcRho
tcSubInst gamma flg (QuantifiedS psi sg)
  -- | SInst-Fun
  | flg
  , ArrowP phi <- psi
  = do rho <- tcSubInst gamma True sg
       pure (QuantifiedR (ArrowR phi) (rhoToSigma rho))

  -- | SInst-InvisForall
  | isInvisForallP psi
  , Just (alpha, kappa) <- forallQuantifieP psi
  = do tau <- newTauMetaVar kappa
       tcSubInst gamma flg (appLOmega (mkSingSubst alpha (metaTyVarT tau)) sg)

  -- | SInst-Constraint
  | FatArrowP q <- psi
  = do emitSimple (mkCt CtGiven q)
       tcSubInst gamma flg sg

-- | SInst-Base
tcSubInst _ _ (RhoSg rho) = pure rho
tcSubInst _ _ sg = pprPanic "tcSkol"
                         (pretty "Expected a rho-type but got:" <+>
                          pprSigma sg)

{- *********************************************************************
*                                                                      *
*    Gamma |-skolapatss psis ; aps ~> < <| pat ; sigma |> > ; Delta    *
*                                                                      *
********************************************************************* -}

tcSkolAPats :: Gamma -> [Psi] -> [APat] -> Tc ([(Pat, Sigma)], Delta, [Q], LOmega)
tcSkolAPats gamma psis apats = do
  traceTc "tcSkolAPats" (vsep [pretty "Arguments:"   <+> pprArgs apats
                              ,pretty "Quantifiers:" <+> pprPsis psis])
  go emptySubst gamma psis apats
  where
    emptyOrVis (VisArg _ :_) = True
    emptyOrVis []            = True
    emptyOrVis _             = False

    go subst gamma (p : ps) apats
      -- | SkolAPats-InvisibleSkip
      | isInvisForallP p
      , emptyOrVis apats
      , Just (alpha, kappa) <- forallQuantifieP p
      = do traceTc "tcSkolAPats - InvisibleSkip" emptyDoc
           sk <- newSkol (skolems $ delta gamma) alpha
           let subst1 = mkSingSubst alpha (skolTyVarT sk)
               instPs = map (appLOmegaPsi subst1) ps
           go (subst <> subst1) (modifyDelta (modifySkolems ((sk, kappa) :)) gamma) instPs apats

      -- | SkolAPats-Specified
      | SpecifiedP (alpha, kappa) <- p
      , InvisArg typat : aps <- apats
      = do traceTc "tcSkolAPats - Specified"
                   (vsep [pretty "Bound:" <+> pprBinding (alpha, kappa)
                         ,pretty "Type Pattern:"   <+> pprTerm typat ])
           sk <- newSkol (skolems . delta $ gamma) alpha
           tvbs' <- tcBindTvs (fv typat)
           phi <- tcType (modifyDelta (modifyScopedTyVars (tvbs' <>)) gamma) typat (Check kappa)
           tau <- sigmaToTauTc phi
           disjointScopedTyVars ((scopedTyVars . delta) gamma) tvbs'
           let subst1 = mkSingSubst alpha (skolTyVarT sk)
               instPs = map (appLOmegaPsi subst1) ps
           emitSimple (mkNCEqCt CtWanted (skolTyVarT sk) tau)
           (pairs, dlt, qs, subst2) <- go (subst <> subst1)
                                          (modifyDelta (modifySkolems ((sk, kappa) :)) gamma)
                                          instPs aps
           let extend = modifyScopedTyVars (tvbs' <>) . modifySkolems ((sk, kappa) :)
           pure (pairs, extend dlt, qs,subst2)

      -- | SkolAPats-Required
      | RequiredP (alpha, kappa) <- p
      , VisArg typat : aps <- apats
      = do traceTc "tcSkolAPats - Required"
                   (vsep [pretty "Bound:" <+> pprBinding (alpha, kappa)
                         ,pretty "Type Pattern:"   <+> pprTerm typat ])
           sk <- newSkol (skolems . delta $ gamma) alpha
           tvbs' <- tcBindTvs (fv typat)
           phi <- tcType (modifyDelta (modifyScopedTyVars (tvbs' <>)) gamma) typat (Check kappa)
           tau <- sigmaToTauTc phi
           disjointScopedTyVars ((scopedTyVars . delta) gamma) tvbs'
           let subst1 = mkSingSubst alpha (skolTyVarT sk)
               instPs = map (appLOmegaPsi subst1) ps
           emitSimple (mkNCEqCt CtWanted (skolTyVarT sk) tau)
           (pairs, dlt', qs, subst2) <- go (subst <> subst1)
                                           (modifyDelta (modifySkolems ((sk, kappa) :)) gamma)
                                           instPs aps
           let extend = modifyScopedTyVars (tvbs' <>)
           pure (pairs, extend dlt', qs, subst2)

      -- | SkolAPats-Constraint
      | FatArrowP _ <- p
      = traceTc "tcSkolAPats - Constraint" emptyDoc >> go subst gamma ps apats

      -- | SkolAPats-Fun
      | ArrowP sigma <- p
      , VisArg pat : aps <- apats
      = do traceTc "tcSkolAPats - Arrow"
                   (vsep [pretty "Sigma:" <+> pprSigma sigma
                         ,pretty "Pat:"   <+> pprTerm pat ])
           (pairs, dlt, qs, subst1) <- go subst gamma ps aps
           pure ((pat, sigma) : pairs, dlt, qs, subst1)

    go subst _ [] aps
      -- | SkolAPats-Empty
      | null aps
      = pure ([], emptyDelta, [],subst)
      | otherwise
      = throwError (ExpectedLessArguments $ pprArgs aps)

    go subst _ ps aps
      = let doc = vsep [pretty "Arguments:" <+> pprArgs aps
                       ,pretty "Quantifiers:" <+> pprPsis ps]
        in do traceTc "tcSkolAPats - Last:" doc
              throwError (ArgDoesntMatchQuantifer doc)

{- *********************************************************************
*                                                                      *
*                     Gamma |-bindtv as ~> Delta                       *
*                                                                      *
********************************************************************* -}

tcBindTvs :: FVList Var -> Tc [(Var, (TcTau, TcKappa))]

-- | BindTyVars-Fresh
tcBindTvs = traverse (\v -> (v,) . first metaTyVarT <$> newMetaVarAndKind)
