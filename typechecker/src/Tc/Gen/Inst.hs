-- | Instantiation

module Tc.Gen.Inst (
  -- mguql
  mguql,
  -- Quick Look instantiation: |-i
  tcI,
  -- Typechecking application heads (QL): |-head_QL
  tcHeadQL,
  -- Typechecking arguments (QL): |-arg_QL
  tcArgQL
  )
where

import UniqMap

import Tc.Subst
import Tc.Constraint
import FV

import Tc.Monad
import Tc.Gen.Kind
import Tc.Gen.Type

import Term
import Type
import Prettyprinter

{- *********************************************************************
*                                                                      *
*                 Gamma |-mgu_ql sigma ; phi ~> Theta                  *
*                                                                      *
********************************************************************* -}

mguql :: Gamma -> Sigma -> Phi -> Tc BTheta

-- | MGU-IVarL
mguql gamma (TcTyVarS (MuTcTv vu)) sg = substIfSameKind gamma vu sg

-- | MGU-IVarR
mguql gamma sg (TcTyVarS (MuTcTv vu)) = substIfSameKind gamma vu sg

-- | MGU-App
mguql gamma (AppS sigma1 phi1) (AppS sigma2 phi2) = do
  btheta1 <- mguql gamma sigma1 sigma2
  btheta2 <- mguql gamma (appBTheta btheta1 phi1) (appBTheta btheta1 phi2)
  pure (btheta2 <> btheta1)

-- | MGU-Quantified
mguql gamma (QuantifiedS psi1 sigma) (QuantifiedS psi2 phi) = do
  btheta1 <- mguPsi gamma psi1 psi2
  btheta2 <- mguql gamma (appBTheta btheta1 sigma) (appBTheta btheta1 phi)
  pure (btheta2 <> btheta1)

-- | MGU-UnUnifiable
mguql _ _ _ = pure emptySubst

{- *********************************************************************
*                                                                      *
*               Gamma |-mgu_psi psi1 ; psi2 ~> Theta                   *
*                                                                      *
********************************************************************* -}

mguPsi :: Gamma -> Psi -> Psi -> Tc BTheta

-- | MGUPSI-InvisibleForall
mguPsi gamma (InferredP (_, kappa1)) (InferredP (_, kappa2))
  = mguql gamma kappa1 kappa2

-- | MGUPSI-InvisibleForall
mguPsi gamma (SpecifiedP (_, kappa1)) (SpecifiedP (_, kappa2))
  = mguql gamma kappa1 kappa2

-- | MGUPSI-VisibleForall
mguPsi gamma (RequiredP (_, kappa1)) (RequiredP (_, kappa2))
  = mguql gamma kappa1 kappa2

-- | MGUPSI-Arrow
mguPsi gamma (ArrowP sigma) (ArrowP phi)
  = mguql gamma sigma phi

-- | MGUPSI-Constraint
mguPsi _ (FatArrowP _) (FatArrowP _) = pure emptySubst

-- | MGU-REST
mguPsi _ _ _ = pure emptySubst

-- TODO: Consider renaming this
substIfSameKind :: Gamma -> MuTyVar -> Phi -> Tc BTheta
substIfSameKind gamma mu@(MkMuTv { muVarKind = ka }) ph = do
  -- TODO: Ask Simon if this is correct
  kappa2 <- sigmaToTauTc =<< tcKind gamma ph
  kappa1 <- sigmaToTauTc ka
  traceTc "substIfSameKind:" (pprAsEq CtWanted kappa1 kappa2)
  emitSimple $ mkNCEqCt CtWanted kappa1 kappa2
  pure $ mkSingSubst mu ph

{- *********************************************************************
*                                                                      *
*         Gamma |-i sigma ; args ~> Theta ; Qs ; phis ; exprs          *
*                                                                      *
********************************************************************* -}

tcI :: Gamma -> Sigma -> [Arg] -> Tc (BTheta, [Q], [(Expr, Phi)], Rho)

tcI gamma (QuantifiedS psi sigma) args
  -- | I-SkipSpecified
  | InvisArg TWildCard : as <- args
  , isSpecifiedP psi
  , Just (quantifiee, quantifieeKind) <- forallQuantifieP psi
  = do traceTc "You have got to be kidding me." emptyDoc
       mu <- newMuVar quantifieeKind
       tcI gamma (appLOmega (mkSingSubst quantifiee (muTyVarT mu)) sigma) as

  | InvisArg a : _ <- args
  , isArrowP psi || isRequiredP psi
  = throwError (ArgDoesntMatchQuantifer
                $ vsep [pretty "Quantifier:" <+> pprPsi psi
                       ,pretty "Arg:" <+> pprArg (InvisArg a)])

  -- | I-Invisible
  | isInvisForallP psi
  , Just (quantifee, quantifeeKind) <- forallQuantifieP psi
  = do mu <- newMuVar quantifeeKind
       let tctv = muTyVarT mu
       traceTc "I-Invisible"
         $ vsep [pretty "Instantiated:" <+> pprSigma (QuantifiedS psi sigma)
                ,pretty "To:" <+>
                 pprSigma (appLOmega (mkSingSubst quantifee tctv) sigma)]
       tcI gamma (appLOmega (mkSingSubst quantifee tctv) sigma) args

  -- | I-Required and I-TyApp
  | a : as <- args
  , (isVisArg a && isRequiredP psi) || (isInvisArg a && isSpecifiedP psi)
  , Just (quantifee, quantifeeKind) <- forallQuantifieP psi
  = do sigma' <- tcType gamma (actualArg a) (Check quantifeeKind)
       tcI gamma (appBOmega (mkSingSubst quantifee sigma') sigma) as

  -- | I-Arg
  | ArrowP phi <- psi
  , VisArg expr : as <- args
  = do traceTc "tcI - I-Arg:" $ vsep [pretty "Arg:"      <+> pprTerm expr
                                     ,pretty "Arg type:" <+> pprSigma phi]
       btheta1 <- tcArgQL gamma expr phi
       (btheta2, qs, pairs, rhor) <- tcI gamma (appBTheta btheta1 sigma) as
       let btheta = btheta2 <> btheta1
       pure (btheta, qs, (expr, appBTheta btheta phi) : pairs, rhor)

  -- | I-Constraint
  | FatArrowP q <- psi
  = do (btheta, qs, pairs, rhor) <- tcI gamma sigma args
       q' <- sigmaToTauTc (appBTheta btheta q)
       pure (btheta, q' : qs, pairs, rhor)

-- | I-VarM See Note [I-VarD and I-VarM]
-- TODO: Check this with Simon
tcI gamma (TcTyVarS mv@(MetaTcTv _)) args@(a : _)
  | isVisArg a
  = do alpha <- newMetaVarOfKindType
       beta <- newMetaVarOfKindType
       let fun_ty = ArrowT (metaTyVarT alpha)
                           (metaTyVarT beta)
       emitSimple $ mkNCEqCt CtWanted (TcTyVarT mv) fun_ty
       traceTc "tcI - MetaTyVar:" $
         vsep [pretty "Emmited an equality between:" <+>
               pprTcTyVar mv
              ,pretty "And:" <+> pprTau fun_ty]
       tcI gamma (tauToSigma fun_ty) args

-- | I-VarD See Note [I-VarD and I-VarM]
tcI gamma (TcTyVarS (MuTcTv mu)) args@(a : _)
  | isVisArg a
  = do vu1 <- newMuVar typeSigma
       vu2 <- newMuVar typeSigma
       let fun_ty = tauToSigma $ ArrowT (muTyVarT vu1)
                                        (muTyVarT vu2)
           btheta1 = mkSingSubst mu fun_ty
       (btheta2, qs, pairs, rhor) <- tcI gamma fun_ty args
       pure (btheta2 <> btheta1, qs, pairs, rhor)

tcI _ (RhoSg rhor) leftovers
  -- | I-Result
  | null leftovers
  = pure (emptySubst, [], [], rhor)

  | otherwise
  = do traceTc "tcI - leftovers:"
         $ vsep [pretty (show leftovers)
                ,pretty "rhor:" <+> pprRho rhor]
       pprPanic "tcI - shouldn't be here" emptyDoc

-- TODO: Rename this note
{- Note [I-VarD and IVar-M]
Considering that this is an actual algorithm, we have three different notations of a variable:
- Type Variables
- Meta/Unification variable
- Instantiation variable

We don't have a rule for an actual meta (unification) variable
in our specification, it is covered by I-Result and I-Arg
But we do need a separate rule for unification variable.
Following the Quick Look paper, we'll split our I-Var rule into rules,
I-VarD, and I-VarM.
-}

{- *********************************************************************
*                                                                      *
*                Gamma |-arg_QL expr : sigma ~> Theta                  *
*                                                                      *
********************************************************************* -}

tcArgQL :: Gamma -> Expr -> Sigma -> Tc BTheta

-- | ArgQL-App
tcArgQL gamma (TApp head args) (RhoSg rho) = do
  sigma <- tcHeadQL gamma head
  (_,qs,_,rhor) <- tcI gamma sigma args
  emitSimples $ map (mkCt CtGiven) qs
  if isGuarded rho || isEmptyUM (fiv rhor)
    then mguql gamma (rhoToSigma rho) (rhoToSigma rhor)
    else pure emptySubst

-- | ArgQL-Rest
tcArgQL _ _ _ = pure emptySubst

{- *********************************************************************
*                                                                      *
*                   Gamma |-head_QL head : sigma                       *
*                                                                      *
********************************************************************* -}

tcHeadQL :: Gamma -> Head -> Tc Sigma

tcHeadQL gamma (HAnn _ typ)
-- | HeadQL-Ann
  = tcType gamma typ =<< newInferType

-- | HeadQL-Var
tcHeadQL gamma (HVar var)
  = liftEither $ lookupVarType (delta gamma) var

-- | HeadQL-ConLike
tcHeadQL gamma (HConLike kp) = do
  res <- liftEither $ lookupGammaElt gamma (GKP kp)
  pure (case res of
          DataConRes  dct -> fromDataConType dct
          PatSynRes _ pst -> fromPatSynType pst)

tcHeadQL _ head
  = pprPanic "tcHeadQL"
  $ hsep [pretty "Compiler bug: Passed "
         ,pprHead head
         ,pretty "to tcHeadQL"]
