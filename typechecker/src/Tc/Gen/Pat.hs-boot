module Tc.Gen.Pat
  (tcPat
  ,tcPatsInfer
  ,tcPatsCheck
  ,tcPatStar)
where

import Term
import Type

import Tc.Monad
import Term

tcPat :: Gamma -> SigmaEnv -> Pat -> ExpType -> Tc (Delta, [Q])

tcPatsInfer :: Gamma -> [(Pat, ExpType)] -> Tc (Delta, [Q])

tcPatsCheck :: Gamma -> [(Pat, Sigma)] -> Tc (Delta, [Q])

tcPatStar :: Gamma -> Pat -> ExpType -> Tc (Delta, [Q])