module Tc.Gen.Expr
  (tcInst
  ,tcExpr
  ,tcHeadExpr
  ,tcArg
  ,tcLit) where

import Tc.Monad

import Term
import Type

tcExpr :: Gamma -> Expr -> ExpType -> Tc ()

tcHeadExpr :: Gamma -> Head -> Tc Sigma

tcArg :: Gamma -> Expr -> Sigma -> Tc ()

tcInst :: Gamma -> Sigma -> [Arg] -> ExpType -> Tc ([Expr], [Phi])

litType :: Lit -> Sigma

tcLit :: Lit -> ExpType -> Tc ()
