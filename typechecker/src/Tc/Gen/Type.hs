 -- | Kindchecking user written types

module Tc.Gen.Type (
  -- Kindchecking user written types (Typs): |-type
  tcType
  ) where

import Tc.Constraint

import Term
import Type

import Tc.Monad

import Tc.Subst
import Data.Bifunctor

import Prettyprinter

{- *********************************************************************
*                                                                      *
*              Gamma |-type typ ~> sigma :_delta kappa                 *
*                                                                      *
********************************************************************* -}

tcType :: Gamma -> Typ -> ExpType -> Tc Sigma

-- | Type-Fun
tcType gamma (TArrow typ1 typ2) expType = do
  sg1 <- tcType gamma typ1 expType
  sg2 <- tcType gamma typ2 expType
  pure (ArrowS sg1 sg2)

-- | Type-Arg
tcType gamma (TApp head args) expType = do
  (sigma,kappa) <- tcHeadType gamma head
  (phis,kappa') <- tcTyInst gamma kappa args
  -- TODO: it seems like we are not using kappa''
  tau <- sigmaToTauTc kappa
  tau' <- sigmaToTauTc kappa'
  kappa'' <- readExpType expType
  emitSimple $ mkNCEqCt CtWanted tau tau'
  pure $ foldl AppS sigma phis

tcType gamma (TForall qflag binder typ) expType
  -- | Type-Specified
  | QSpecified <- qflag
  = do (var, alpha, tau) <- tcBinder gamma binder
       let gamma' = modifyDelta (modifyScopedTyVars ((var, (AlphaT alpha, tauToSigma tau)):)) gamma
       sigma <- tcType gamma' typ expType
       -- TODO: it seems like we are not using kappa
       kappa <- readExpType expType
       pure $ SpecifiedS (alpha, tauToSigma tau) sigma

  -- | Type-Required
  | QRequired <- qflag
  = do (var, alpha, tau) <- tcBinder gamma binder
       let gamma' = modifyDelta (modifyScopedTyVars ((var, (AlphaT alpha, tauToSigma tau)):)) gamma
       sigma <- tcType gamma' typ expType
       -- TODO: it seems like we are not using kappa
       kappa <- readExpType expType
       pure $ RequiredS (alpha, tauToSigma tau) sigma

-- | Type-Constraint
tcType gamma (TFatArrow head args typ) expType = do
  qsg <- tcType gamma (TApp head args) (Check constraintSigma)
  q <- sigmaToTauTc qsg
  sigma <- tcType gamma typ expType
  pure $ FatArrowS q sigma

tcType _ ty _ = throwError $ IllegalTermError (pretty "Invalid Type" <+> pprTerm ty)

{- *********************************************************************
*                                                                      *
*              Gamma |-head_type head ~> sigma : kappa                 *
*                                                                      *
********************************************************************* -}

tcHeadType :: Gamma -> Head -> Tc (Sigma, Kappa)

-- | TyHead-Var
tcHeadType gamma (HVar var)
  = first tauToSigma <$> liftEither (lookupScopedTyVar (delta gamma) var)

-- | TyHead-TyCon
tcHeadType gamma (HConLike tyCon)
  = (\(x,y) -> (TyConS x, y)) <$> liftEither (lookupTyCon gamma tyCon)

-- | TyHead-Ann
tcHeadType gamma (HAnn typ typ') = do
  kappa <- tcType gamma typ' (Check typeSigma)
  sigma <- tcType gamma typ (Check kappa)
  pure (sigma, kappa)
tcHeadType _ other = throwError (IllegalTermError $ pprHead other)

{- *********************************************************************
*                                                                      *
*           Gamma |-tyinst kappa ; args ~> sigmas ; kappa'             *
*                                                                      *
********************************************************************* -}

tcTyInst :: Gamma -> Kappa -> [Arg] -> Tc ([Sigma], Kappa)
tcTyInst gamma ka args@(a : as)
  -- | TyInst-Arg
  | ArrowS ka1 ka2 <- ka
  , VisArg typ <- a
  = do sg <- tcType gamma typ (Check ka1)
       (sgs, ka') <- tcTyInst gamma ka2 as
       pure (sg : sgs, ka')

  -- | TyInst-InvisArg
  | SpecifiedS (alpha, ka1) ka2 <- ka
  , InvisArg typ <- a
  = do phi <- tcType gamma typ (Check ka1)
       (sgs, ka') <- tcTyInst gamma (appBOmega (mkSingSubst alpha phi) ka2) as
       pure (phi : sgs, ka')

  -- | TyInst-InvisSkip
  | SpecifiedS (alpha, ka1) ka2 <- ka
  = do tau <- newTauMetaVar ka1
       tcTyInst gamma (appLOmega (mkSingSubst alpha (metaTyVarT tau)) ka2) args

  -- | TyInst-VisArg
  | RequiredS (alpha, ka1) ka2 <- ka
  , VisArg typ <- a
  = do phi <- tcType gamma typ (Check ka1)
       tcTyInst gamma (appBOmega (mkSingSubst alpha phi) ka2) as

  | otherwise
  = throwError (KindCheckingError $ pprArgs args) -- TODO: better error handling.

-- | TyInst-Empty
tcTyInst _ ka [] = pure ([], ka)

{- *********************************************************************
*                                                                      *
*                  Gamma |-binder binder ~> Delta                      *
*                                                                      *
********************************************************************* -}

tcBinder :: Gamma -> Binder -> Tc (Var, TyVar, Tau)

-- | Binder-Var
tcBinder _ (UnAnn var) = do
  mv <- metaTyVarT <$> newTauMetaVar typeSigma
  tv <- newTyVar
  pure (var, tv,mv)

-- | Binder-VarAnn
tcBinder gamma (Ann var typ) = do
  tau <- liftEither . sigmaToTau =<< tcType gamma typ (Check typeSigma)
  tv <- newTyVar
  pure (var,tv,tau)
