module Tc.Gen.Inst
  (mguql
  ,tcI
  ,tcHeadQL
  ,tcArgQL)
where

import Tc.Monad

import Tc.Constraint

import Tc.Subst

import Term
import Type

mguql :: Gamma -> Sigma -> Phi -> Tc BTheta

tcI :: Gamma -> Sigma -> [Arg] -> Tc (BTheta, [Q], [(Expr, Phi)], Rho)

tcArgQL :: Gamma -> Expr -> Sigma -> Tc BTheta

tcHeadQL :: Gamma -> Head -> Tc Sigma