module Tc.Gen.Match(tcMatches, caseLHS) where

import Term
import Type

import Tc.Monad

tcMatches :: Gamma -> [Match] -> ExpType -> Tc ()

caseLHS :: [APat] -> Tc Pat
