-- | Typechecking of value declarations

module Tc.Gen.ValDecl (
  -- Typechecking of value declarations: |-vds
  tcValDecls
  )
where

import Term
import Type
import FV
import Utils
import Tc.Monad
import Tc.Solver (simplifyInfer)
import Tc.Constraint
import Tc.Gen.Type
import Tc.Gen.Match
import Tc.Gen.Pat
import {-# SOURCE #-} Tc.Gen.Expr
import Data.List
import Data.Bifunctor
import UniqMap
import Data.List.NonEmpty (NonEmpty)
import qualified Data.List.NonEmpty as NE
import Data.Graph
import Control.Monad
import Control.Monad.IO.Class
import Prettyprinter

data ValGroup
  = Rec (NonEmpty ValBind)
  | NonRec ValBind
  deriving Show

isClosedLocal :: [ValGroup] -> Bool
isClosedLocal = undefined

isTopLevel :: Tc Bool
isTopLevel = (== topTcLevel) <$> getTcLevel

sccWithSigs :: [Var] -> [ValBind] -> [ValGroup]
sccWithSigs wSigs vbs = fmap valGroup
                      . stronglyConnComp
                      . map node
                      $ if not (null wSigs)
                         then filter noSig vbs
                         else vbs
  where
    node d@(FunBind v mchs) = (d, v, concatMap matchFvs mchs)
    -- TODO: we don't support pattern bindings yet
    node (PatBind _ _) = pprPanic "Pattern binding"
                                  (pretty "Pattern bindings are not supported yet!")

    matchFvs (MkMatch aps e) = fv e \\ concatMap (fv . actualArg) aps

    valGroup (AcyclicSCC vb)   = NonRec vb
    valGroup (NECyclicSCC vbs) = Rec vbs

    noSig (FunBind v _)
      | v `elem` wSigs
      = True
      | otherwise
      = False
      -- TODO: We don't support pattern bindings yet.
    noSig (PatBind pat e) = undefined

{- *********************************************************************
*                                                                      *
*               Gamma ; Sigma |-valbind valbind ~> Delta               *
*                                                                      *
********************************************************************* -}

tcValBind :: Gamma -> SigmaEnv -> ValBind -> Tc [(Var, Sigma)]
tcValBind gamma sigs (FunBind var mchs)
  = case lookupVarTypeInSigs sigs var of
      -- | ValBind-FunBind
      Left _ -> do
        rho <- tcInfer_ (tcMatches gamma mchs)
        pure [(var, rho)]

      -- | ValBind-FunBindCheck
      -- TODO: Fix me
      Right sigma -> pure undefined -- do mapM_ (\mch -> tcMatch gamma mch (Check sigma)) mchs
                                    --   pure []

tcValBind gamma sigs (PatBind pat expr)
  = if bv pat |#| dom (variables (delta gamma))
    -- | ValBind-PatBindNoTopSig
    then do
      (_,rho) <- tcInfer (tcExpr gamma expr)
      (dlta,_) <- tcPat gamma sigs pat (Check rho)
      pure (variables dlta)

    -- | ValBind-PatBind
    else do
      ((dlta,_), tau) <- tcInfer (tcPat gamma sigs pat) -- TODO: handle the qs
      tcExpr gamma expr (Check tau)
      pure (variables dlta)

{- *********************************************************************
*                                                                      *
*          Gamma ; Sigma |-valbinds valbinds ~> Delta                  *
*                                                                      *
********************************************************************* -}

tcValBinds :: Gamma -> SigmaEnv -> [ValBind] -> Tc [(Var, Sigma)]
-- | ValBinds-Concat
-- | ValBinds-Empty
tcValBinds gamma sigs = concatMapM (tcValBind gamma sigs)

{- *********************************************************************
*                                                                      *
*                 Gamma |-valdecls valdecls ~> Delta                   *
*                                                                      *
********************************************************************* -}

-- TODO: Shouldn't bind the same binder more than once
tcValDecls :: Gamma -> ValDecls -> Tc [(Var, Sigma)]

-- | ValDecls-Sigs
tcValDecls gamma (MkValDecls sigs vbs) = do
  let (vs, typs) = unzip (map unSig sigs)
  sgs <- mapM (\typ -> tcType gamma typ (Check typeSigma)) typs
  let sigenv = zip vs sgs
  unless (dom sigenv `subset` bvBinds vbs) $ throwError (UnaccompaniedSigs $ hsep (map pprVar vs))
  bs <- tcValGroups (modifyDelta (modifyVariables (<> sigenv)) gamma) sigenv (sccWithSigs (dom sigenv) vbs)
  pure (bs <> sigenv)

-- | ValDecls-NoSigs
tcValDecls gamma (MkValDecls [] vbs) = tcValGroups gamma [] (sccWithSigs [] vbs)

{- *********************************************************************
*                                                                      *
*            Gamma ; Sigma |-valgroups valgroups ~> Delta              *
*                                                                      *
********************************************************************* -}

-- TODO: Add the monomorphism restriction
-- TODO: Turn this into a folding function
tcValGroups :: Gamma -> SigmaEnv -> [ValGroup] -> Tc [(Var, Sigma)]

-- | ValGroups-Concat
tcValGroups gamma sigs (vgp : vgps) = do
  bs <- tcValGroup gamma sigs vgp
  bs' <- tcValGroups (modifyDelta (modifyVariables (<> bs)) gamma) sigs vgps
  pure (bs ++ bs')

-- | ValGroups-Empty
tcValGroups _ _ [] = pure []

{- *********************************************************************
*                                                                      *
*            Gamma ; Sigma |-valgroup valgroup ~> Delta                *
*                                                                      *
********************************************************************* -}

tcValGroup :: Gamma -> SigmaEnv -> ValGroup -> Tc [(Var, Sigma)]

-- | ValGroup-Recursive
tcValGroup gamma sigs (Rec vbs)   = do
  let vbsL = NE.toList vbs
      xs   = bvBinds vbsL \\ map fst (variables (delta gamma))
  bs  <- zip xs <$> replicateM (length xs) (fmap (tauToSigma . metaTyVarT) newMetaVarOfKindType)
  (bindingsAndTypes, wcs) <- captureCts $ tcValBinds (modifyDelta (modifyVariables (<> bs)) gamma) sigs vbsL
  traceTc "tcValGroup - Recursive WantedCts:" $ indent 2 (pprWantedCts wcs)
  let isTopLevel = True
      (bindings,phis) = unzip bindingsAndTypes
  tclvl <- getTcLevel
  ((qtvs,qs), residual) <- captureCts $ simplifyInfer isTopLevel tclvl phis wcs
  zphis <- mapM zonkSigma phis
  traceTc "tcValGroup - Recursive: Zonked phis:" (pprSigmas zphis)
  let sigmas = map (quantifyOverSigma qtvs qs) zphis
  traceTc "tcValGroup - Recursive: Generalized phis:" (pprSigmas sigmas)
  pure (zip bindings sigmas)

-- | ValGroup-NonRecursive
tcValGroup gamma sigs (NonRec vb) = do
  (bindingsAndTypes, wcs) <- captureCts $ tcValBind gamma sigs vb
  traceTc "tcValGroup - Nonrecursive WantedCts:" $ indent 2 (pprWantedCts wcs)
  let isTopLevel = True -- TODO: IMPORTANT! Fix me
      (bindings,phis) = unzip bindingsAndTypes
  traceTc "Monomorphic types:" $ pprList pprSigmas phis
  tclvl <- getTcLevel
  ((qtvs,qs), residual) <- captureCts $ simplifyInfer isTopLevel tclvl phis wcs
  zphis <- mapM zonkSigma phis
  traceTc "tcValGroup - NonRecursive Zonked phis:" (pprSigmas zphis)
  let sigmas = map (quantifyOverSigma qtvs qs) zphis
  traceTc "Final generalized types: " (indent 2 $ pprTidiedSigmas sigmas)
  pure (zip bindings sigmas)

quantifyOverSigma :: [(SkolTyVar,Kappa)] -> [Q] -> Sigma -> Sigma
quantifyOverSigma bs qs sigma
  = quantifyOverSigma' bs' qs' (unwrapSkol (map fst bs') sigma)
  where
    freeSkolemVars = fsv sigma
    bs' = map (first skolTyVar) $ filter (flip memberUM freeSkolemVars . fst) bs
    qs' = qs -- TODO: Fix me

    -- TODO: IMPORTANT
    unwrapSkolTau qtvs t = undefined
    unwrapSkolPsi qtvs (RequiredP (a,ka))  = RequiredP (a, unwrapSkol qtvs ka)
    unwrapSkolPsi qtvs (SpecifiedP (a,ka)) = SpecifiedP (a, unwrapSkol qtvs ka)
    unwrapSkolPsi qtvs (InferredP (a,ka))  = InferredP (a, unwrapSkol qtvs ka)
    unwrapSkolPsi qtvs (ArrowP sg)         = ArrowP (unwrapSkol qtvs sg)
    unwrapSkolPsi qtvs (FatArrowP q)       = FatArrowP (unwrapSkolTau qtvs q)

    unwrapSkol :: [TyVar] -> Sigma -> Sigma
    unwrapSkol qtvs (QuantifiedS psi sg) = QuantifiedS (unwrapSkolPsi qtvs psi) (unwrapSkol qtvs sg)
    unwrapSkol qtvs (AppS ph sg)         = AppS (unwrapSkol qtvs ph) (unwrapSkol qtvs sg)
    unwrapSkol _ tau@(TyConS _)          = tau
    unwrapSkol _ tau@(AlphaS _)          = tau
    unwrapSkol qtvs tau@(TcTyVarS tctv)
      = case tctv of
          SkolTcTv sk -> if skolTyVar sk `elem` qtvs then AlphaS (skolTyVar sk) else tau
          _            -> tau

quantifyOverSigma' :: [Binding] -> [Q] -> Sigma -> Sigma
quantifyOverSigma' bs qs = mkFatArrowS qs . mkInferredS bs
