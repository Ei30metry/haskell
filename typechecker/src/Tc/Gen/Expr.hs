-- | Typechecking of expressions

module Tc.Gen.Expr
  (tcInst
  ,tcExpr
  ,tcHeadExpr
  ,tcArg
  ,tcLit)
where

import FV

import Tc.Monad
import {-# SOURCE #-} Tc.Gen.Inst
import Tc.Gen.Match
import Tc.Gen.Pat
import Tc.Gen.ValDecl
import Tc.Gen.Type
import Tc.Gen.Subst
import Tc.Constraint

import Tc.SkolSub
import Tc.Subst

import Term
import Type

import Prettyprinter

import Control.Monad

{- *********************************************************************
*                                                                      *
*           Gamma |-inst sigma ; args :_delta rho ; es ; phis          *
*                                                                      *
********************************************************************* -}

tcInst :: Gamma -> Sigma -> [Arg] -> ExpType -> Tc ([Expr], [Phi])

-- | Inst-Infer
tcInst gamma sigma args (Infer expType) = do
  traceTc "tcInst - sigma:" (pprSigma sigma)
  (_,qs, pairs, rhor) <- tcI gamma sigma args
  traceTc "tcInst - rhor" (pprRho rhor)
  let (es,phis) = unzip pairs
  theta <- tcSubstFill (fivs (rhoToSigma rhor : phis))
  let wcts = mkSimpleWC CtWanted $ map (appLTheta theta) qs
  traceTc "tcInst - infer case:" (pprWantedCts wcts)
  emitWantedCts wcts
  fillInferResult (appLTheta theta rhor) expType
  pure (es, map (appLTheta theta) phis)

-- | Inst-Check
tcInst gamma sigma args (Check rho) = do
  (_, qs, pairs, rhor) <- tcI gamma sigma args
  let (es, phis) = unzip pairs
  btheta <- mguql gamma (rhoToSigma rhor) rho
  theta <- tcSubstFill $ fivs (map (appBTheta btheta) phis)
  flg <- deepSubFlag
  tcSub gamma flg rho (appLTheta theta . appBTheta btheta $ rhor)
  wcts <- mkSimpleWC CtWanted <$> mapM (sigmaToTauTc    .
                                        appLTheta theta .
                                        appBTheta btheta) qs
  traceTc "tcInst - check case:" (pprWantedCts wcts)
  emitWantedCts wcts
  pure (es, map (appLTheta theta . appBTheta btheta) phis)

{- *********************************************************************
*                                                                      *
*                     Gamma |-expr expr :_delta rho                    *
*                                                                      *
********************************************************************* -}

tcExpr :: Gamma -> Expr -> ExpType -> Tc ()

-- | Expr-App
tcExpr gamma (TApp head args) expType = do
  sigma <- tcHeadExpr gamma head
  traceTc "tcExpr - head:" $ pretty (show head) <+> pretty ':' <+> (pprSigma sigma)
  traceTc "tcExpr - args:" $ pretty (show args)
  (exprs, phis) <- tcInst gamma sigma args expType
  mapM_ (uncurry $ tcArg gamma) $ zip exprs phis

-- | Expr-SuperLambda
tcExpr gamma (TSuperLambda matches) expType
  = tcMatches gamma matches expType

-- | Expr-Let
tcExpr gamma (TLet valDecls expr) expType = do
  vars <- tcValDecls gamma valDecls
  traceTc "tcExpr - Let" (pprVarBindings vars)
  tcExpr (modifyDelta (modifyVariables (<> vars)) gamma ) expr expType

-- | Expr-Case
tcExpr gamma (TCase expr0 matches) expType = do
  tcInfer_ $ \rho0 -> do
    tcExpr gamma expr0 rho0
    forM_ matches $ \(MkMatch apats expr) -> do
      pat <- caseLHS apats
      (dlt,qs) <- tcPatStar gamma pat rho0
      let givens = map (mkCt CtGiven) qs
          skols  = skolems dlt
      buildImplicIfNeeded skols givens $
        tcExpr (modifyDelta (appendDelta dlt) gamma) expr expType
  pure ()

-- | Expr-If
tcExpr gamma (TIf cond thene elsee) expType = do
  condType <- sigmaToTauTc =<< tcInfer_ (tcExpr gamma cond)
  emitSimple (mkNCEqCt CtWanted condType boolTau)
  tcExpr gamma thene expType
  tcExpr gamma elsee expType

tcExpr _ (TLit lit) expType = tcLit lit expType

-- Illegal term!
tcExpr _ iterm _ = throwError (IllegalTermError $ pprTerm iterm)

{- *********************************************************************
*                                                                      *
*                    Gamma |-head_expr head : sigma                    *
*                                                                      *
********************************************************************* -}

tcHeadExpr :: Gamma -> Head -> Tc Sigma
tcHeadExpr gamma (HVar var)
  = case lookupVarType (delta gamma) var of
      -- | Head-Var
      Right x -> pure x

      -- | Head-NamedHole
      Left err | isNamedWC var -> tauToSigma . metaTyVarT <$>
                                  newMetaVarOfKindType
               | otherwise     -> throwError err
-- | Head-Ann
tcHeadExpr gamma (HAnn expr ttype) = do
  (_,sigma) <- tcInfer (tcType gamma ttype)
  tcArg gamma expr sigma
  pure sigma

-- | Head-Con and Head-PatSyn
tcHeadExpr gamma (HConLike kp) = do
  res <- liftEither $ lookupGammaElt gamma (GKP kp)
  pure (case res of
          DataConRes  dct -> fromDataConType dct
          PatSynRes _ pst -> fromPatSynType pst)

-- | Head-Infer
tcHeadExpr gamma (HTerm expr) = tcInfer_ (tcExpr gamma expr)

tcHeadExpr _ HWildCard
  = pprPanic "tcHeadExpr" (pretty "Holes are not supported yet!")

{- *********************************************************************
*                                                                      *
*                      Gamma |-arg expr : sigma                        *
*                                                                      *
********************************************************************* -}

tcArg :: Gamma -> Expr -> Sigma -> Tc ()

-- | Push-TySuperLambda
tcArg gamma (TSuperLambda matches) sigma
  = tcMatches gamma matches (Check sigma)

-- | Push-Skol
tcArg gamma expr sigma = do
  flg <- deepSubFlag
  (bs, qs, rho) <- tcSkol gamma flg sigma
  emitSimples $ map (mkCt CtGiven) qs
  tcExpr (modifyDelta (modifySkolems (bs <>)) gamma) expr (Check $ rhoToSigma rho)

{- *********************************************************************
*                                                                      *
*                      Typechecking of literals                        *
*                                                                      *
********************************************************************* -}

litType :: Lit -> Tau
litType LitChar   = chatTau
litType LitInt    = intTau
litType LitString = stringTau

tcLit :: Lit -> ExpType -> Tc ()
tcLit lit (Infer ires)  = fillInferResult (tauToRho $ litType lit) ires
tcLit lit (Check sigma) = do
  tau <- sigmaToTauTc sigma
  emitSimple (mkNCEqCt CtWanted tau (litType lit))
