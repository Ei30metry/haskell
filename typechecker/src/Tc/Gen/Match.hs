-- | Typechecking of match-expressions

module Tc.Gen.Match
  (tcMatches -- Typechecking of match expressions: does |-mch
  ,caseLHS   -- Retrieving the LHS of a case-expression branch
  ) where

import Term
import Type

import Tc.Constraint

import Tc.Monad
import Tc.Gen.Pat
import {-# SOURCE #-} Tc.Gen.Expr
import Control.Monad
import Prettyprinter
import Utils (pprList)

{- *********************************************************************
*                                                                      *
*                   Gamma |-match mch :_check sigma                    *
*                   Gamma |-match mch :_infer rho                      *
*                                                                      *
********************************************************************* -}

tcMatch :: Gamma -> [(APat,ExpType)] -> Expr -> ExpType -> Tc ()

-- | Match-Infer
tcMatch gamma expPAps e (Infer inferResult)
  = do ps <- getPats expPAps
       (dlt, qs) <- tcPatsInfer gamma ps
       traceTc "tcMatch - Delta returened by tcPatsInfer:" (pprDelta dlt)
       let givens = map (mkCt CtGiven) qs
           skols  = skolems dlt
       buildImplicIfNeeded skols givens
           $ tcExpr (modifyDelta (dlt <>) gamma) e (Infer inferResult)

-- | Match-Check
tcMatch gamma expPAps e (Check sigma) = undefined
  -- = case splitSigmaUsingArity sigma aps of
  --     Just (psis, rho) -> do
  --       (patSigmaPairs, lcl_ctxt, qs,subst) <- tcSkolAPats gamma psis aps
  --       (lcl_ctxt', qs') <- tcPatsCheck (modifyDelta (lcl_ctxt <>) gamma) patSigmaPairs
  --       let givens = map (mkCt CtGiven) (qs ++ qs')
  --           skols  = skolems lcl_ctxt
  --       buildImplicIfNeeded skols givens $
  --         tcExpr (modifyDelta ((lcl_ctxt' <> lcl_ctxt) <>)  gamma) e (Check (appOmega subst rho))
  --     Nothing -> throwError (ArityMisMatch $ vsep [pretty "Type:", pprSigma sigma
  --                                                 ,pretty "Arguments: ", pprArgs aps])

{- Note [Expeted type of patterns in tcMatch]
The tcMatch function should do fillInferResult (See Note [fillInferResult])
on the return type to make sure that all of the right hand sides have the same types.
But what about the left hand sides, i.e. patterns?
Imagine the following function definition:

f True 3 = 9      -- (True : mv-1) (3 : mv-2) (9 : mv-3)
f True 4 = 5      -- (True : mv-4) (4 : mv-5) (5 : mv-6)
f y    x = x      -- (y    : mv-7) (x : mv-8) (x : mv-8)

Let's assume OverloadedLiterals isn't a thing, then f should get the type

   f : mv-10 -> mv-11 -> mv-11 (more precisely: Bool -> Integer -> Integer)

We should make sure that /every/ equation of f gets this type. i.e. we should
make sure the following equalities hold:
  - mv-1 ~ mv-4 ~ mv-7
  - mv-2 ~ mv-5 ~ mv-8
  - mv-3 ~ mv-6 ~ mv-8
And the way we do this is by pushing the /same/ InferResults into the branches, and
considering that after we have typechecked the first equation of the definition,
the InferResults will have been filled-in, typechecking the consecutive equations
will cause fillInferResult to emit the necessary equalities.
-}

tcMatches :: Gamma
          -> [Match]
          -> ExpType   -- Expteced type of the whole match expression
          -> Tc ()

tcMatches gamma mchs (Infer inferRes) = do
  let unMatched          = map unMatch mchs
      (leftHandSides, _) = unzip unMatched
  -- The arity of match
  arity <- getArity leftHandSides
  -- Enough Expected types to push to the LHS of /all/ match-expressions.
  expTypes <- replicateM arity newInferType
  let pairs = map (\(aps,e) -> (zip aps expTypes, e)) unMatched
  -- Type of the RHS of /all/ match-expressions
  rho <- sigmaToRhoTc =<< tcInfer_ (\expType -> mapM_ (\(aps,e) -> tcMatch gamma aps e expType) pairs)
  -- Type of the LHS of all matches
  sigmas <- mapM readExpType expTypes
  let final = appArrowR sigmas rho
  traceTc "tcMatches" (pprRho final)
  -- Final type of the matches
  fillInferResult final inferRes

tcMatches gamma mchs (Check sigma) = undefined
  -- = case splitSigmaUsingArity sigma aps of
  --     Just (psis, rho) -> do
  --       (patSigmaPairs, lcl_ctxt, qs,subst) <- tcSkolAPats gamma psis aps
  --       (lcl_ctxt', qs') <- tcPatsCheck (modifyDelta (lcl_ctxt <>) gamma) patSigmaPairs
  --       let givens = map (mkCt CtGiven) (qs ++ qs')
  --           skols  = skolems lcl_ctxt
  --       buildImplicIfNeeded skols givens $
  --         tcExpr (modifyDelta ((lcl_ctxt' <> lcl_ctxt) <>)  gamma) e (Check (appOmega subst rho))
  --     Nothing -> throwError (ArityMisMatch $ vsep [pretty "Type:", pprSigma sigma
  --                                                 ,pretty "Arguments: ", pprArgs aps])

{- *********************************************************************
*                                                                      *
*                |-split sigma ; apats ~> psis ; rho                   *
*                                                                      *
********************************************************************* -}


splitSigmaUsingArity :: Sigma -> [APat] -> Maybe ([Psi],Rho)
splitSigmaUsingArity sigma apats = go sigma apats []
  where
    go (QuantifiedS p ph) aps@(a : as) psis

      -- | Split-Constraint
      | isFatArrowP p
      = go ph aps (p : psis)

      -- | Split-InvisibleSkip
      | (isSpecifiedP p && isVisArg a)
      || isInferredP p
      = go ph aps (p : psis)

      -- | Split-Arrow
      -- | Split-Required
      -- | Split-Specified
      | isArrowP p
      || isRequiredP p
      || isSpecifiedP p
      = go ph as (p : psis)

    -- | Split-Base
    go (RhoSg rho) aps psis
      | null aps
      = Just (psis, rho)

      | otherwise
      = Nothing
    go sg _ _ = pprPanic "spliteSigmaUsingArity" (pretty "Got:" <+>
                                                  pprSigma sg)

---------------------
----- Utilities -----
---------------------

getArity :: [[APat]] -> Tc Int
getArity aps
  = if allEq arities
        then pure (head arities)
        else throwError (ArityMisMatch $ pretty "Arity mismatch:"
                                      <+> pprList (vsep . map pprArgs) aps)
  where
    arities = map (foldr step 0) aps
    step (VisArg _) x = x + 1
    step _          x = x
    allEq (x : x' : xs) = x == x' && allEq xs
    allEq _             = True

caseLHS :: [APat] -> Tc Pat
caseLHS [VisArg pat] = pure pat
caseLHS x            = throwError $ IllegalCaseLHS
                                  $ pretty "Invalid LHS for case-expression: "
                                  <+> pprArgs x


getPats :: [(APat,ExpType)] -> Tc [(Pat,ExpType)]
getPats apats = mapM extract apats
  where
    extract (VisArg p, expType) = pure (p, expType)
    extract (InvisArg p, _)     = throwError (IllegalArgumentPattern $ pprTerm p)
