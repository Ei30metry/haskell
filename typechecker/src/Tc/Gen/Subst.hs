-- | Type substitution

module Tc.Gen.Subst (tcSubstFill, tcTyVarsToMus) where

import Data.Foldable

import Type

import Tc.Subst
import UniqMap

import FV
import Tc.Monad

-------------------------------------
---- G |-subst_fill mus ~> theta ----
-------------------------------------

tcSubstFill :: FVSet MuTyVar -> Tc LTheta
tcSubstFill mus = do
  let musList   = toListUM mus
      musKinds  = map muVarKind musList
  fromListSubst . zip musList <$>
    mapM (fmap metaTyVarT . newTauMetaVar)
    musKinds

tcTyVarsToMus :: [Binding] -> Tc LOmega
tcTyVarsToMus = foldrM step emptyUM
  where
    step (x,y) xs = pure
                  . addToUM xs x
                  . muTyVarT
                  =<< newMuVar y
