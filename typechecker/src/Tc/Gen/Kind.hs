-- | Kinding of types

module Tc.Gen.Kind (
  -- Kinding of types (sigmas): |-kind
  tcKind
  ) where

import Type

import Tc.Monad
import Data.Foldable
import Prettyprinter
import Control.Monad
import Tc.Subst
import Utils

{- *********************************************************************
*                                                                      *
*                      Gamma |-kind sigma : kappa                      *
*                                                                      *
********************************************************************* -}

tcKind :: Gamma -> Sigma -> Tc Kappa
tcKind (MkGamma { tyCons = tcs'
                , delta = dlt }) = go []
  where
    tcs = map snd tcs'
    skkinds = skolems dlt

    go :: [(TyVar, Kappa)] -> Sigma -> Tc Kappa
    go bs (QuantifiedS psi sigma)
      = case psi of
          FatArrowP q -> pprPanic "Constraint in a kind:" (pprTau q)
          ArrowP _  -> pure typeSigma

          -- | Kind-Required
          RequiredP (a, ka) -> do
            ka' <- go bs ka
            unless (ka' `eqSigma` typeSigma)
              $ throwError (KindingError $ pprSigma ka'
                            <+> pretty "Should be Type!")
            go ((a, ka) : bs) sigma

          -- | Kind-Invisible
          SpecifiedP (a, ka) -> do
            ka' <- go bs ka
            unless (ka' `eqSigma` typeSigma)
              $ throwError (KindingError $ pprSigma ka' <+>
                            pretty "Should be Type!")
            go ((a, ka) : bs) sigma

          -- | Kind-Invisible
          InferredP (a, ka) -> do
            ka' <- go bs ka
            unless (ka' `eqSigma` typeSigma)
              $ throwError (KindingError $ pprSigma ka' <+>
                            pretty "should be Type!")
            go ((a, ka) : bs) sigma

    go bs (AlphaS tv)
      = case find (eqFst tv) bs of
          Just (_,ka) -> pure ka
          Nothing     -> throwError (KindingError
                                     $ pprTyVar tv <+>
                                     pretty "is out of scope!")
    go bs (AppS sigma phi) = do
      ka1 <- go bs sigma
      case ka1 of
        ArrowS ka2 ka3 -> do
          ka4 <- go bs phi
          unless (ka2 `eqSigma` ka4)
            $ throwError (KindingError . nest 2 $
                          vsep [pretty "Kind mismatch:"
                               ,pretty "Expected:" <+> pprSigma ka2
                               ,pretty "Got:"      <+> pprSigma ka4])
          pure ka3

        -- | Kind-Inst
        InferredS (a, ka2) ka3 -> do
          ka4 <- go bs phi
          unless (ka2 `eqSigma` ka4)
            $ throwError (KindingError . nest 2 $
                            vsep [pretty "Kind mismatch:"
                                 ,pretty "Expected:" <+> pprSigma ka2
                                 ,pretty "Got:"      <+> pprSigma ka4])
          pure $ appBOmega (mkSingSubst a phi) ka3

        -- | Kind-Inst
        RequiredS (a, ka2) ka3 -> do
          ka4 <- go bs phi
          unless (ka2 `eqSigma` ka4)
            $ throwError (KindingError . nest 2 $
                          vsep [pretty "Kind mismatch:"
                               ,pretty "Expected:" <+> pprSigma ka2
                               ,pretty "Got:"      <+> pprSigma ka4])
          pure $ appBOmega (mkSingSubst a phi) ka3

        -- | Kind-Inst
        SpecifiedS (a, ka2) ka3 -> do
          ka4 <- go bs phi
          unless (ka2 `eqSigma` ka4)
            $ throwError (KindingError . nest 2 $
                          vsep [pretty "Kind mismatch:"
                               ,pretty "Expected:" <+> pprSigma ka2
                               ,pretty "Got:"      <+> pprSigma ka4])
          pure $ appBOmega (mkSingSubst a phi) ka3
        x -> throwError (KindingError . vsep
                         $ [pretty "App lhs:" <+> pprSigma sigma
                           ,pretty "Has invalid kind:" <+> pprSigma x])
    go _ (TcTyVarS tctv)
      = case tctv of
          --  TODO: Fix me. We are branching on the type ....
          SkolTcTv sk | Just (_,skka) <- find (eqFst sk) skkinds -> pure skka
                       | otherwise -> pprPanic "tcKind" (pretty "Skolem not in skkinds")
          MetaTcTv mv -> pure (metaVarKind mv)

          -- | Kind-InstVar
          MuTcTv mu   -> pure (muVarKind mu)
    go bs (TyConS tyCon)
      -- | Kind-TypeInType
      | tyCon == tyConType
      = pure typeSigma

      -- | Kind-ConstraintInType
      | tyCon == tyConConstraint
      = pure typeSigma

      -- | Kind-Arrow
      | tyCon == tyConArrow
      = pure . tauToSigma $ ArrowT typeTau ((ArrowT typeTau typeTau))

      -- | Kind-Equality
      | tyCon == tyConNomEq
      = do al <- newTyVar
           ka <- newTyVar
           pure $ SpecifiedS (al, AlphaS ka)
                $ tauToSigma (ArrowT (AlphaT al)
                                      (ArrowT (AlphaT al)
                                               constraintTau))

      -- | Kind-TyCon
      | otherwise
      = case find (\(tc,ka) -> tyCon == tc) tcs of
          Just (_,ka) -> pure ka
          Nothing     -> liftEither
                       $ notInScope (pretty "Type constructor not in scope:" <+>
                                     pprTyCon tyCon)
