-- | Typechecking of patterns

module Tc.Gen.Pat (
  -- Tyepchecking patterns: |-pat
  tcPat,
  -- Typechecking patterns, with no pattern binging signature in mind: |-pat_star
  tcPatStar,
  -- Inferring the type of a sequence of patterns: |-pats_infer
  tcPatsInfer,
  -- Checking the type of a sequence of patterns: |-pats_check
  tcPatsCheck,
  -- Typecchecking patterns)
  )
where

import Tc.Constraint
import Tc.Monad
import Tc.Gen.Type
import {-# SOURCE #-} Tc.Gen.Expr

import Utils
import FV
import Tc.SkolSub

import Tc.Subst

import Prettyprinter

import Term
import Type

import Control.Monad

import Data.Bifunctor

{- *********************************************************************
*                                                                      *
*                Gamma ; Sigma |-pat pat :_delta sigma                 *
*                                                                      *
********************************************************************* -}

tcPat :: Gamma -> SigmaEnv -> Pat -> ExpType -> Tc (Delta, [Q])

-- | Pat-Constraint
tcPat gamma sigs pat (Check sigma)
  | FatArrowS q phi <- sigma
  = do emitSimple (mkCt CtGiven q)
       (lcl', qs) <- tcPat gamma sigs pat (Check phi)
       pure (lcl', q : qs)
-- | Pat-WildCardCheck
tcPat _ _ TWildCard (Check _) = pure (emptyDelta, [])
-- | Pat-WildCardInfer
tcPat _ _ TWildCard (Infer inferResult) = do
    tau <- newTauMetaVar typeSigma
    fillInferResult (tauToRho . metaTyVarT $ tau) inferResult
    pure (emptyDelta, [])
-- | Pat-AsPat
tcPat gamma sigs (TAs x pat) expType = do
  (lclCtxt@(MkDelta { variables = vars}), qs) <- tcPat gamma sigs pat expType
  when (x `elem` dom vars) $ throwError (ScopingError $ pprVar x
                                                      <+> pretty "is being brough to scope more than once.")
  sigma <- expTypeToSigma expType
  pure (lclCtxt { variables = (x, sigma) : vars }, qs)

tcPat gamma sigs (TVar var) (Check sigma)
  = case lookup var sigs of
       -- | Pat-VarCheckSig
      Just phi -> do
        flg <- deepSubFlag
        tcSub gamma flg sigma phi

        pure (emptyDelta { variables = [(var, sigma)]}, [])
      -- | Pat-VarCheck
      Nothing -> pure (emptyDelta { variables = [(var, sigma)]}, [])

tcPat gamma sigs (TVar var) (Infer inferResult)
  = case lookup var sigs of
      -- | Pat-VarInferSig
     Just phi -> do
       rho <- sigmaToRhoTc phi
       fillInferResult rho inferResult
       traceTc "tcPat TVar Infer Just - bringing into scope:" (pprVarBinding (var,phi))

       pure (emptyDelta { variables = [(var, phi)] }, [])
     -- | Pat-VarInfer
     Nothing -> do
       mv <- newTauMetaVar typeSigma
       let tau = metaTyVarT mv
       fillInferResult (tauToRho tau) inferResult
       traceTc "tcPat TVar Infer Nothing - bringing into scope:" (pprVarBinding (var,tauToSigma tau))
       pure (emptyDelta { variables = [(var, tauToSigma tau)] }, [])

-- | Pat-SigCheck
tcPat gamma sigs (TAnn p ty) (Check sigma) = do
  psb <- patSigBindFlag
  tvbs <- tcBindTvs (if psb then fv ty else [])
  let extended = modifyDelta (modifyScopedTyVars (tvbs <>)) gamma
  sigma' <- tcType extended ty (Check typeSigma)
  flg <- deepSubFlag
  tcSub extended flg sigma sigma'
  (lcl',qs) <- tcPat extended sigs p (Check sigma')
  pure (modifyScopedTyVars (tvbs <>) lcl', qs)

-- | Pat-SigInfer
tcPat gamma sigs (TAnn p ty) (Infer inferResult) = do
  tvbs <- tcBindTvs (fv ty)
  let extended = modifyDelta (modifyScopedTyVars (tvbs <>)) gamma
  sigma <- tcType extended ty (Check typeSigma)
  rho <- sigmaToRhoTc sigma
  (lcl', qs) <- tcPat extended sigs p (Check sigma)
  fillInferResult rho inferResult
  pure (modifyScopedTyVars (tvbs <>) lcl', qs)

tcPat gamma sigs pat@(TApp head apats) expType
  -- | Pat-Inst
  | HConLike _ <- head
  , Check (SpecifiedS (alpha, kappa) sigma) <- expType
  = do tau <- newTauMetaVar kappa
       tcPat gamma sigs pat (Check $ appLOmega (mkSingSubst alpha (metaTyVarT tau)) sigma)

  | HConLike cl <- head
  = do lookupRes <- liftEither $ lookupGammaElt gamma (GKP cl)
       case lookupRes of
         -- | Pat-ConCheck
         -- | Pat-KPInfer
         DataConRes (MkDataConType { dctPsis  = psis
                                   , dctTyCon = tyCon
                                   , dctArgs  = taus }) ->
           do traceTc "tcPat - Pat-{ConCheck,KPInfer}"
                      $ vsep [pretty "DataCon:" <+> pprConLike cl
                             ,pretty "Found TyCon:" <+> pprTyCon tyCon
                             ,pretty "APats:" <+> pprArgs apats]
              mvs <- matchExpectedConTy expType tyCon taus -- See Note [matchExpectedConTy]
              (psps, dlt, qs1, toSkolSubst) <- tcSkolAPats gamma psis apats
              -- TODO: Refactor me
              (omegaSk, gadtWanteds) <- tcSolve $ zip (map (tauToSigma . appLOmega toSkolSubst) taus)
                                                      (map tauToSigma mvs)
              traceTc "Delta returned by tcSkolAPats:" (pprDelta dlt)
              qs1ts <- mapM (fmap (mkCt CtGiven) . sigmaToTauTc . appBOmegaSk omegaSk) qs1
              emitSimples $ gadtWanteds ++ qs1ts
              (dlt', qs3) <- tcPatsCheck (modifyDelta (dlt <>) gamma) (map (second (appBOmegaSk omegaSk)) psps)
              traceTc "Delta returned by tcPatsCheck:" (pprDelta dlt')
              pure (dlt <> dlt', qs1 ++ qs3)

           -- | Pat-SynCheck
         PatSynRes _ (MkPatSynType { pstUnivs = univs
                                   , pstReqs  = reqs
                                   , pstPsis  = psis
                                   , pstRes   = res }) ->
           pprPanic "tcPat - Pat-SynCheck" (pretty "Pattern synonyms are not support yet")
            -- TODO: Fix this. The skolemisation stuff might be out of order
            -- do emitSimple $ mkNCEqCt CtWanted sg res
            --    omega <- tcTyVarsToMus univs
            --    let phi' = appOmega omega res
            --    theta1 <- mguql gamma phi' sg
            --    theta2 <- tcSubstFill $ fiv (appTheta theta1 phi')
            --    (psps, dlt, qs1, subst1) <- tcSkolAPats gamma psis apats
            --    let theta = theta2 <> theta1
            --        subst = appTheta theta . appOmega (subst1 <> omega)
            --    emitSimples $ map ( mkCt CtWanted . subst) reqs
            --    emitSimples $ map (mkCt CtGiven) qs1
            --    (dlt', qs2) <- tcPatsCheck (modifyDelta (dlt <>) gamma)
            --                                    (map (second subst) psps)
            --    emitSimples $ map (mkCt CtGiven) qs2
            --    pure (dlt <> dlt', qs1 ++ qs2)

  | otherwise
  = throwError (IllegalTermError $ pretty "tcPat:" <+> pprTerm (TApp head apats))

-- | Pat-ViewPat
tcPat gamma sigs (TArrow expr pat) expType = do
  (_,phi) <- tcInfer (tcExpr gamma expr)
  case splitArrowS phi of
     Just (sigma1, sigma2) -> do
       sigma <- expTypeToSigma expType
       traceTc "tcPat - Pat-ViewPat:" (pprSigma sigma)
       flg <- deepSubFlag
       tcSub gamma flg sigma sigma1
       case expType of
         Infer ir -> do rho <- sigmaToRhoTc sigma
                        fillInferResult rho ir
                        tcPat gamma sigs pat (Check sigma2)
         Check _ -> tcPat gamma sigs pat (Check sigma2)
     Nothing -> throwError (ImplementationBug $ pretty "Inferred the wrong type!"
                                              <+> pprSigma phi)

tcPat _ _ (TLit lit) expType = tcLit lit expType >> pure (emptyDelta,[])

tcPat _ _  pat _  = throwError (IllegalTermError $ pprTerm pat)

-- See Note [matchExpectedConTy]
matchExpectedConTy :: ExpType -> TyCon -> [Tau] -> Tc [Tau]
matchExpectedConTy (Check sg) tyCon taus = do
  mvs <- replicateM (length taus) (metaTyVarT <$> newMetaVarOfKindType)
  traceTc "matchExpectedConTy - Check:" (pprTau (foldl AppT (TyConT tyCon) mvs))
  t <- sigmaToTauTc sg
  emitSimple $ mkNCEqCt CtWanted t (foldl AppT (TyConT tyCon) mvs)
  pure mvs
matchExpectedConTy (Infer ires) tyCon taus = do
  mvs <- replicateM (length taus) (metaTyVarT <$> newMetaVarOfKindType)
  traceTc "matchExpectedConTy - New meta variables:" (pprTaus mvs)
  traceTc "matchExpectedConTy - Infer:" (pprTau (appHeadToArgsT (TyConT tyCon) mvs))
  fillInferResult (tauToRho (appHeadToArgsT (TyConT tyCon) mvs)) ires
  pure mvs

{- *********************************************************************
*                                                                      *
*          Gamma |-pats_check < <| pat ; sigma |> > ~> Delta           *
*                                                                      *
********************************************************************* -}

tcPatsCheck :: Gamma -> [(Pat, Sigma)] -> Tc (Delta, [Q])

-- | PatsCheck-Base
tcPatsCheck _ [] = pure (emptyDelta, [])

-- | PatsCheck-Induction
tcPatsCheck gamma ((p,sg) : psps) = do
  (dlt1, qs1) <- tcPatStar gamma p (Check sg)
  (dlt2, qs2) <- tcPatsCheck gamma psps
  dlt1 `disjointDomDelta` dlt2
  pure (dlt1 <> dlt2, qs1 ++ qs2)


{- *********************************************************************
*                                                                      *
*              Gamma |-pats_infer pat ~> sigmas ; Delta                *
*                                                                      *
********************************************************************* -}

tcPatsInfer :: Gamma -> [(Pat,ExpType)] -> Tc (Delta, [Q])

-- | PatsInfer-Base
tcPatsInfer _ [] = pure (emptyDelta, [])

-- | PatsInfer-Induction
tcPatsInfer gamma ((p,expType) : ps) = do
  (dlt1, qs1)  <- tcPatStar gamma p expType
  (dlt2, qs2) <- tcPatsInfer gamma ps
  dlt1 `disjointDomDelta` dlt2
  pure (dlt1 <> dlt2, qs1 ++ qs2)

{- *********************************************************************
*                                                                      *
*              Gamma |-pat_star pat :_delta sigma ~> Delta             *
*                                                                      *
********************************************************************* -}

tcPatStar :: Gamma -> Pat -> ExpType -> Tc (Delta, [Q])

-- | PatStar-EmptySigs
tcPatStar gamma = tcPat gamma []

{- *********************************************************************
*                                                                      *
*                |-solve <| tau ; sigma |> ~> Omega ; Q                *
*                                                                      *
********************************************************************* -}

tcSolve :: [(Sigma, Sigma)] -> Tc (BOmegaSk, [Ct])

-- | Solve-Subst
tcSolve ((TcTyVarS (SkolTcTv sk), sigma) : pairs) = do
  let s = mkSingSubst sk sigma
  (bomega, gadtWcs) <- tcSolve $ map (bimap (appBOmegaSk s) (appBOmegaSk s)) pairs
  pure (s <> bomega, gadtWcs)

-- | Solve-Add
tcSolve ((phi, sigma) : pairs) = do
  tau <- sigmaToTauTc phi
  (bomega, gadtQs) <- tcSolve pairs
  pure (bomega, tcAdd (tau, sigma) gadtQs)

-- | Solve-Empty
tcSolve [] = pure (emptySubst, [])

{- *********************************************************************
*                                                                      *
*                     add(tau ; sigma ; Qs) = Q's                      *
*                                                                      *
********************************************************************* -}

tcAdd :: (Tau, Sigma) -> [Ct] -> [Ct]
tcAdd (t1, (RhoSg (TauR t2))) pairs
  = mkNCEqCt CtGiven t1 t2 : pairs -- TODO: Ask Simon IMPORTANT
tcAdd _ pairs = pairs
