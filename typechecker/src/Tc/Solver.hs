-- | Main solvers

module Tc.Solver (
  -- Simplify wanted constraints and return skolems and constraints
  -- to generalize over.
  simplifyInfer
  ) where

import Data.Maybe
import Data.Void

import Tc.Monad
import Tc.Constraint
import Utils
import UniqMap

import Term
import Type

import Tc.Solver.Equality
import Tc.Solver.Class
import Tc.Solver.Family
import Tc.Solver.Monad
import FV

import Control.Monad
import Control.Monad.IO.Class

import Prettyprinter

{- Note [Definition of Canonical]
if sg1 ~ sg2 then sg1 /= sg2. These should have been removed during canonicalization
if sg1 ~ sg2 and ph1 ~ ph2, then ph1 /= sg1
lhs is either a skolem or a metavar -- for now
-}

-- TODO: Why does this have a nested in its name?
solveNestedImplicCts :: [ImplicCt] -> Cs [ImplicCt]
solveNestedImplicCts impls
  | null impls
  = pure []
  | otherwise
  = catMaybes <$> traverse solveImplicCt impls

solveImplicCt :: ImplicCt -> Cs (Maybe ImplicCt)
solveImplicCt impl@(MkImplic { implTcLvl   = lvl
                             , implSkolems = skols
                             , implGivens  = givens
                             , implWanteds = wanteds }) = pure Nothing -- TODO Fix me

-- solveWantedCts :: WantedCts -> Cs WantedCts
-- solveWantedCts = solve True
--   where
--     solve redoImplics wcts@(MkWC { wcSimples = cts
--                                  , wcImplics = impls })
--       | isSolvedWC wcts
--       = pure wcts
--       | otherwise
--       = do wcts1 <- solveSimpleWanteds cts
--            wcts2 <- (if not redoImplics && null (wcImplics wcts1)
--                     then pure $ wcts { wcSimples = wcSimples wcts1 }
--                     else do impls2 <- solveNestedImplicCts $ impls <> (wcImplics wcts1)
--                             pure (wcts { wcSimples = wcSimples wcts1
--                                        , wcImplics = impls2 }))
--            unifHappened <- resetUnifFlag
--            if unifHappened then solve False wcts2 else pure wcts2

-- NOTE: This is just for debugging and a way to prevent the solver from
-- entering an infinite loop
solveWantedCts :: WantedCts -> Cs WantedCts
solveWantedCts = solve False
  where
    solve redoImplics wcts@(MkWC { wcSimples = cts
                                 , wcImplics = impls })
      | isSolvedWC wcts
      = pure wcts
      | otherwise
      = do traceCs "solveWantedCts cts:" (indent 2 $ pprCtsV cts)
           wcts1 <- solveSimpleWanteds cts
           traceCs "solveWantedCts wcts1 simples:" (indent 2 . pprCtsV $ wcSimples wcts1)
           wcts2 <- (if not redoImplics && null (wcImplics wcts1)
                    then pure $ wcts { wcSimples = wcSimples wcts1 }
                    -- else do impls2 <- solveNestedImplicCts $ impls <> (wcImplics wcts1)
                    else pure (wcts { wcSimples = wcSimples wcts1 }))
           traceCs "solveWantedCts wcts2:" (indent 2 $ pprWantedCts wcts2)
           unifHappend <- resetUnifFlag
           traceCs "Unification happend:" (pretty unifHappend)
           traceCs "wcts2:" (pprWantedCts wcts2)
           if unifHappend then solve False wcts2 else pure wcts2

solveSimples :: [Ct] -> Cs ()
solveSimples cts = emitSimplesToWL cts >> solve_loop
  where
    solve_loop = do
      sel <- selectNextWorkItem
      case sel of
        Nothing -> do traceCs "No more work items!" emptyDoc
                      return ()
        Just ct -> do traceCs "Current work item:" (pprCt ct)
                      solveCt ct
                      traceCs "Solved previous work item:" (pprCt ct)
                      solve_loop

selectNextWorkItem :: Cs (Maybe Ct)
selectNextWorkItem = do
  ref <- getWorkList
  wl  <- readCsRef ref
  case selectWorkItem wl of
    Nothing -> pure Nothing
    Just (ct, newWorkList) -> do
      writeCsRef ref newWorkList
      pure (Just ct)

-- TODO: It might be worth to do what GHC does and keep track of the number of solves we have done
-- TODO: Note [Canonicalization]
solveCt :: Ct -> Cs ()
solveCt ct@(MkCt { ctPred = q
              , ctFlavour = fl})
  | Just (lhs, rhs) <- splitEqPredT q
  = do solveEquality fl lhs rhs
  | otherwise
  = throwErrorCs (UnsupportedCt $ pprTau q)

solveSimpleWanteds :: [Ct] -> Cs WantedCts
solveSimpleWanteds cts = do
  solveSimples cts
  (implics, unsolved) <- getUnsolvedInerts
  let wcts = (MkWC { wcSimples = unsolved
                   , wcImplics = implics })
  return wcts

solveSimpleGivens :: [Ct] -> Cs ()
solveSimpleGivens  = undefined

approximateWantedCts :: WantedCts -> [Ct]
approximateWantedCts = undefined

-- The skolems that we can readily quantify over
-- Meta variables that we should zonk to skolems
candidateQTyVarsOfTypes :: TcLevel -> [Sigma] -> ([SkolTyVar], [MetaTyVar])
candidateQTyVarsOfTypes ambLvl phis = (fsvAmb phis, fmvAmb phis)
  where
    fsvAmb = toListUM . filterUM (\sk -> skolLvl sk == ambLvl) . fsvs
    fmvAmb = toListUM . filterUM (\mv -> metaVarLvl mv == ambLvl) . fmvs

quantifyTyVars :: [MetaTyVar] -> Tc [(SkolTyVar,Kappa)]
quantifyTyVars freevars
  | null freevars
  = return []
  | otherwise
  = mapM skolemiseMetaVar freevars

-- See Note [Zonking to Skolem]
zonkMetaVarToSkol :: MetaTyVar -> Tc SkolTyVar
zonkMetaVarToSkol mv = do
  zmv <- zonkMetaVar mv
  case zmv of
    TcTyVarT (SkolTcTv sk) -> pure sk
    x                      -> pprPanic "zonkMetaVarToSkol"
                                       (pretty "The result of zonking is not a skolem"
                                       <+> pprTau x)
-- TODO: Fix me
skolemiseMetaVar :: MetaTyVar -> Tc (SkolTyVar, Kappa)
skolemiseMetaVar (MkMetaTv { metaVarDetails = ref
		           , metaVarKind    = ka })
  = do tcLvl <- getTcLevel
       kaZonked <- zonkSigma ka
       tv <- newTyVar
       let skol = MkSkolTv { skolLvl = pushTcLevel tcLvl, skolTyVar = tv}
       writeTcRef ref (Indirect . skolTyVarT $ skol)
       return (skol, kaZonked)

{- Note [quantification]
For now, I'm not bothering with quantifying over class constraints. Because we don't them yet .
But considering that soon I will be adding support for them, most of the quantification-related
functions are quite similar to their GHC counterparts.
-}
decideQuantification :: Bool
                     -> TcLevel
                     -> [Sigma]   -- Variables to be generalised
                     -> WantedCts             -- Candidate wanted constraint; already zonked
                     -> Tc ([(SkolTyVar, Kappa)]  -- Quantify over these (skolems)
                            ,[Q])      -- and this context (fully zonked)
decideQuantification  topLevel rhslvl phis wcs
  = do qs <- decideAndPromoteMetaVars topLevel rhslvl phis wcs
       qtvs <- decideQuantifiedMetaVars phis qs
       qsZonked <- mapM zonkTau qs
       return (qtvs, qsZonked)

decideAndPromoteMetaVars :: Bool -> TcLevel -> [Sigma] -> WantedCts -> Tc [Q]
decideAndPromoteMetaVars top_lvl rhs_tclvl phis wanted
  = do promoteMetaVars (fmvs phis)
       pure []

decideQuantifiedMetaVars :: [Sigma] -> [Q] -> Tc [(SkolTyVar,Kappa)]
decideQuantifiedMetaVars zonkedPhis preds
  = do lvl <- getTcLevel
       let (sks, mvs) = candidateQTyVarsOfTypes lvl $ (map tauToSigma preds) ++ zonkedPhis
       qtvs <- quantifyTyVars mvs
       -- TODO: Double check
       pure (map (,typeSigma) sks ++ qtvs)

simplifyInfer :: Bool -> TcLevel -> [Sigma] -> WantedCts -> Tc ([(SkolTyVar, Kappa)], [Q])
simplifyInfer topLevel rhslvl phis wanteds
  | isEmptyWC wanteds
  = do lvl <- getTcLevel
       let (skols, mvs) = candidateQTyVarsOfTypes lvl phis
       qtkvs <- quantifyTyVars mvs
       return (qtkvs, [])

  | otherwise
  = do traceTc "simplifyInfer, wanteds:" (pprWantedCts wanteds)
       solved <- updEnv (setAmbientTcLevel rhslvl) $ runCs (solveWantedCts wanteds)
       wcsZonked <- zonkWantedCts solved
       traceTc "simplifyInfer, solved and zonked wanteds:" (pprWantedCts wcsZonked)
       zphis <- mapM zonkSigma phis
       traceTc "simplifyInfer, zonked phis:" (pprSigmas zphis)
       if isInsolubleWC wcsZonked
         then throwError (InsolubleWC $ pprWantedCts wcsZonked)
         else do (qtvs, qs) <- decideQuantification topLevel rhslvl zphis wcsZonked
                 -- TODO: Remove me
                 -- TODO: no implications yet. emitResidualCts rhslvl phis qtvs qs wcsZonked
                 pure (qtvs, qs)


tryInertEqs :: CanEqCt -> Cs ()
tryInertEqs _ = pure () -- TODO: Fix me

solveIrred :: CanIrredCt -> Cs ()
solveIrred _ = pure () -- TODO: Fix me

solveEquality :: CtFlavour -> TcTau -> TcTau -> Cs ()
solveEquality flv lhs rhs = do
  traceCs "Attempting to zonk lhs:" (pprTau lhs)
  zlhs <- liftTc (zonkTau lhs)
  traceCs "solveEquality - zonked lhs:" (pprTau zlhs)
  zrhs <- liftTc (zonkTau rhs)
  traceCs "solveEquality - zonked rhs:" (pprTau zrhs)
  (irreds, canons) <- canonEq flv zlhs zrhs
  traceCs "solveEquality - canons:" (hsep $ map pprCanEqCt canons)
  mapM_ solveIrred irreds
  mapM_ tryInertEqs canons
  mapM_ updInertEqs canons
