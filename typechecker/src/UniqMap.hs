-- |

module UniqMap
  (UniqMap
  ,UniqSet
  ,emptyUM
  ,addToUM
  ,addToUM'
  ,delUM
  ,updateUM
  ,differenceUM
  ,intersectionUM
  ,sizeUM
  ,unionUM
  ,unionsUM
  ,allUM
  ,filterUM
  ,anyUM
  ,lookupUM
  ,isEmptyUM
  ,toListUM
  ,disjointUM
  ,memberUM
  ,memberUMUnique
  ,fromListUM)
where

import Data.IntMap (IntMap)
import qualified Data.IntMap as M
import BasicTypes
import Data.Bifunctor (first)

newtype UniqMap k el =
  UM { unUniqMap :: IntMap el }
  deriving (Eq, Show, Functor, Foldable, Traversable)

type UniqSet el = UniqMap el el

emptyUM :: UniqMap k el
emptyUM = UM M.empty

addToUM :: Uniquable k => UniqMap k el -> k -> el -> UniqMap k el
addToUM (UM m) k el = UM $ M.insert (getKey k) el m

addToUM' :: Uniquable el => UniqMap el el -> el -> UniqMap el el
addToUM' um el = addToUM um el el

delUM :: Uniquable k => UniqMap k el -> k -> UniqMap k el
delUM (UM m) k = UM $ M.delete (getKey k) m

updateUM :: Uniquable k => UniqMap k el -> (el -> Maybe el) -> k -> UniqMap k el
updateUM (UM m) f k = UM $ M.update f (getKey k) m

differenceUM :: UniqMap k el -> UniqMap k el -> UniqMap k el
differenceUM (UM m1) (UM m2) = UM $ M.difference m1 m2

intersectionUM :: UniqMap k el -> UniqMap k el -> UniqMap k el
intersectionUM (UM m1) (UM m2) = UM $ M.intersection m1 m2

sizeUM :: UniqMap k el -> Int
sizeUM (UM m) = M.size m

unionUM :: UniqMap k el -> UniqMap k el -> UniqMap k el
unionUM (UM m1) (UM m2) = UM $ M.union m1 m2

unionsUM :: [UniqMap k el] -> UniqMap k el
unionsUM = UM . foldr (M.union . unUniqMap) M.empty

allUM :: (el -> Bool) -> UniqMap k el -> Bool
allUM p (UM m) = foldr ((&&) . p) True m

filterUM :: (el -> Bool) -> UniqMap k el -> UniqMap k el
filterUM p (UM m) = UM (M.filter p m)

anyUM :: (el -> Bool) -> UniqMap k el -> Bool
anyUM p (UM m) = foldr ((||) . p) True m

lookupUM :: Uniquable k => k -> UniqMap k el -> Maybe el
lookupUM k (UM m) = M.lookup (getKey k) m

isEmptyUM :: UniqMap k el -> Bool
isEmptyUM (UM m) = M.null m

toListUM :: UniqMap k el -> [el]
toListUM (UM m) = map snd $ M.toList m

disjointUM :: UniqMap k el -> UniqMap k el -> Bool
disjointUM (UM m1) (UM m2) = M.disjoint m1 m2

memberUM :: Uniquable k => k -> UniqMap k el -> Bool
memberUM k (UM m) = M.member (getKey k) m

memberUMUnique :: Uniquable k => Unique -> UniqMap k el -> Bool
memberUMUnique u (UM m) = M.member (unUnique u) m

fromListUM :: forall k el. Uniquable k => [(k,el)] -> UniqMap k el
fromListUM = UM . M.fromList . map (first getKey)

instance Semigroup (UniqMap k el) where
  (<>) = unionUM

instance Monoid (UniqMap k el) where
  mempty = emptyUM
