-- | Free variables

module FV (
  HasMuVar(..),  -- Free instantiation variables
  HasSkolVar(..), -- Free skolem variables
  HasMetaVar(..), -- Free meta (unification) variables
  HasTcTyVar(..), -- Free TcTyVars
  HasTyVar(..),  -- Free type variables

  FVSet,      -- Set of free variables
  FVList,    -- List of free variables
  emptyFVSet,
  insertFVSet,
  unionsFVSet,
  unionFVSet,
  memberFVSet,
  toListFVSet,
  bv,         -- Bound variables in patterns
  bvBind,     -- Bound variabls in a binding
  bvBinds,
  fv,         -- Free term varibles
  fvs,
  )
where

import BasicTypes
import UniqMap
import Data.List (nub)

import Term
import Type

type FVSet a = UniqMap a a

type FVList a = [a]

emptyFVSet :: forall a. FVSet a
emptyFVSet = emptyUM

insertFVSet :: forall a. Uniquable a => a -> FVSet a -> FVSet a
insertFVSet x vars = addToUM vars x x

unionsFVSet :: forall a. [FVSet a] -> FVSet a
unionsFVSet = unionsUM

unionFVSet :: forall a. FVSet a -> FVSet a -> FVSet a
unionFVSet = unionUM

memberFVSet :: forall a. Uniquable a => a -> FVSet a -> Bool
memberFVSet = memberUM

toListFVSet :: forall a. FVSet a -> [a]
toListFVSet = toListUM

-- Bound variables in patterns
bv :: Pat -> FVList Var
bv = go []
  where
    go bs (TApp h aps) = case h of
      HVar v     -> go_aps (v : bs) aps
      HConLike _ -> go_aps bs aps
      HTerm _    -> go_aps bs aps
      HAnn pat _ -> go bs pat
    go bs (TAs v pat) = go (v : bs) pat
    go bs _           = bs

    go_aps bs (VisArg a : as) = go bs a <> go_aps bs as
    go_aps bs (_:as)          = go_aps bs as
    go_aps bs     []          = bs

bvBind :: ValBind -> FVList Var
bvBind (FunBind v _) = [v]
bvBind (PatBind p _) = bv p

bvBinds :: [ValBind] -> FVList Var
bvBinds = foldMap bvBind

-- Free instantiation variables
-- See Note [Shallow vs Deep free type variables]
class HasMuVar a where
  fiv  :: a   -> FVSet MuTyVar

  fivs :: [a] -> FVSet MuTyVar
  fivs = foldMap fiv

-- Free type variables
-- See Note [Shallow vs Deep free type variables]
class HasTyVar a where
  ftv :: Bool -> a -> FVSet TyVar

  shallowFtv :: a -> FVSet TyVar
  shallowFtv = ftv False

  deepFtv :: a -> FVSet TyVar
  deepFtv = ftv True

  shallowFtvs :: [a] -> FVSet TyVar
  shallowFtvs = foldMap shallowFtv

  deepFtvs :: [a] -> FVSet TyVar
  deepFtvs = foldMap deepFtv

-- Free skolem variables
-- Always gathers deeply. See Note [Shallow vs Deep free type variables]
class HasSkolVar a where
  fsv  :: a    -> FVSet SkolTyVar

  fsvs :: [a] -> FVSet SkolTyVar
  fsvs = foldMap fsv

-- Free meta variables
-- Always gathers deeply. See Note [Shallow vs Deep free type variables]
class HasMetaVar a where
  fmv  :: a    -> FVSet MetaTyVar

  fmvs :: [a] -> FVSet MetaTyVar
  fmvs = foldMap fmv

-- Free TcTyVar
-- Always gathers deeply. See Note [Shallow vs Deep free type variables]
class HasTcTyVar a where
  ftctv  :: a   -> FVSet TcTyVar

  ftctvs :: [a] -> FVSet TcTyVar
  ftctvs = foldMap ftctv

-- Free instantiation variables
instance HasMuVar Sigma where
  fiv (RhoSg rho)           = fiv rho
  fiv (QuantifiedSg psi sg) = case psi of
    Specified (a, ka) -> fiv ka <> fiv sg
    Inferred  (a, ka) -> fiv ka <> fiv sg
    FatArrow q        -> fiv q <> fiv sg

instance HasMuVar Rho where
  fiv (AppR rho sg)        = fiv rho <> fiv sg
  fiv (TauR t)             = fiv t
  fiv (QuantifiedR psi sg) = case psi of
    Required (a, ka) -> fiv ka <> fiv sg
    ArrowR ph        -> fiv ph <> fiv sg

instance HasMuVar Tau where
  fiv = go emptyFVSet
    where
      go vars (ArrowT t1 t2)  = vars <> go emptyFVSet t1 <> go emptyFVSet t2
      go vars (AppT t1 t2)    = vars <> go emptyFVSet t1 <> go emptyFVSet t2
      go vars (TyConT _)      = vars
      go vars (AlphaT _)      = vars
      go vars (TcTyVarT tctv) = case tctv of
        MuTcTv vu -> insertFVSet vu vars
        x -> vars

instance HasSkolVar Tau where
  fsv = go emptyFVSet
    where
      go vars (ArrowT t1 t2)  = vars <> go emptyFVSet t1 <> go emptyFVSet  t2
      go vars (AppT t1 t2)    = vars <> go emptyFVSet t1 <> go emptyFVSet t2
      go vars (TyConT _)      = vars
      go vars (AlphaT _)      = vars
      go vars (TcTyVarT tctv) = case tctv of
        SkolTcTv sk -> insertFVSet sk vars
        x            -> vars

instance HasSkolVar Rho where
  fsv (QuantifiedR psi sg) = case psi of
    ArrowR ph        -> fsv ph <> fsv sg
    Required (a, ka) -> fsv ka <> fsv sg
  fsv (AppR rho sg) = fsv rho <> fsv sg
  fsv (TauR t) = fsv t

instance HasSkolVar Sigma where
  fsv (RhoSg rho)           = fsv rho
  fsv (QuantifiedSg psi sg) = case psi of
    Specified (a, ka) -> fsv ka <> fsv sg
    Inferred  (a, ka) -> fsv ka <> fsv sg
    FatArrow q        -> fsv q  <> fsv sg

instance HasTcTyVar Sigma where
  ftctv (RhoSg rho)           = ftctv rho
  ftctv (QuantifiedSg psi sg) = case psi of
    Specified (a, ka) -> ftctv ka <> ftctv sg
    Inferred  (a, ka) -> ftctv ka <> ftctv sg
    FatArrow q        -> ftctv q  <> ftctv sg

instance HasTcTyVar Rho where
  ftctv (AppR rho sg) = ftctv rho <> ftctv sg
  ftctv (QuantifiedR psi sg) = case psi of
    Required (a, ka) -> ftctv ka <> ftctv sg
    ArrowR   ph      -> ftctv ph <> ftctv sg

instance HasTcTyVar Tau where
  ftctv = go emptyFVSet
    where
      go vars (AppT t1 t2) = vars <> go emptyFVSet t1 <> go emptyFVSet t2
      go vars (TyConT _) = vars
      go vars (AlphaT _) = vars
      go vars (ArrowT t1 t2) = vars <> go emptyFVSet t1 <> go emptyFVSet t2
      go vars (TcTyVarT t) = insertFVSet t vars

instance HasTyVar Tau where
  ftv _ = go emptyFVSet
    where
      go vars (AppT t1 t2)   = vars <> go emptyFVSet t1 <> go emptyFVSet t2
      go vars (AlphaT tv)    = insertFVSet tv vars
      go vars (ArrowT t1 t2) = vars <> go emptyFVSet t1 <> go emptyFVSet t2
      go vars (TyConT _)     = vars
      go vars (TcTyVarT _)   = vars

instance HasTyVar Rho where
  ftv deepFlag rho = go_rho emptyFVSet emptyFVSet rho
    where
      go_rho bs vars (TauR t) = go_tau bs vars t
      go_rho bs vars (AppR sigma phi) = go_sigma bs vars sigma <> go_sigma bs vars phi
      go_rho bs vars (QuantifiedR psi sigma) = case psi of
        ArrowR sg  -> go_sigma bs vars sg <> go_sigma bs vars sigma
        Required b -> withFlag deepFlag b bs vars sigma

      go_tau bs vars (AlphaT alpha)
        | not (alpha `memberFVSet` bs)
        = alpha `insertFVSet` vars
        | otherwise
        = vars
      go_tau bs vars (ArrowT t1 t2) = go_tau bs vars t1 <> go_tau bs vars t2
      go_tau bs vars (AppT t1 t2)   = go_tau bs vars t1 <> go_tau bs vars t2
      go_tau bs vars (TyConT _)     = vars
      go_tau bs vars (TcTyVarT _)   = vars

      go_sigma bs vars (QuantifiedSg psi sigma) = case psi of
        Specified b -> withFlag deepFlag b bs vars sigma
        Inferred b  -> withFlag deepFlag b bs vars sigma
        FatArrow q  -> vars <> go_tau bs emptyFVSet q <> go_sigma bs emptyFVSet sigma
      go_sigma bs vars (RhoSg rho) = go_rho bs vars rho

      withFlag True  (alpha, _) bs  vars sigma = go_sigma (alpha `insertFVSet` bs) vars sigma
      withFlag False (alpha, ka) bs vars sigma = go_sigma (alpha `insertFVSet` go_sigma bs vars ka) vars sigma


instance HasTyVar Sigma where
  ftv deepFlag rho = go_sigma emptyFVSet emptyFVSet rho
    where
      go_rho bs vars (TauR t) = go_tau bs vars t
      go_rho bs vars (AppR sigma phi) = go_sigma bs vars sigma <> go_sigma bs vars phi
      go_rho bs vars (QuantifiedR psi sigma) = case psi of
        ArrowR sg  -> go_sigma bs vars sg <> go_sigma bs vars sigma
        Required b -> withFlag deepFlag b bs vars sigma

      go_tau bs vars (AlphaT alpha)
        | not (alpha `memberFVSet` bs)
        = alpha `insertFVSet` vars
        | otherwise
        = vars
      go_tau bs vars (ArrowT t1 t2) = go_tau bs vars t1 <> go_tau bs vars t2
      go_tau bs vars (AppT t1 t2)   = go_tau bs vars t1 <> go_tau bs vars t2
      go_tau bs vars (TyConT _)     = vars
      go_tau bs vars (TcTyVarT _)   = vars

      go_sigma bs vars (QuantifiedSg psi sigma) = case psi of
        Specified b -> withFlag deepFlag b bs vars sigma
        Inferred b  -> withFlag deepFlag b bs vars sigma
        FatArrow q  -> vars <> go_tau bs emptyFVSet q <> go_sigma bs emptyFVSet sigma
      go_sigma bs vars (RhoSg rho) = go_rho bs vars rho

      withFlag True  (alpha, _) bs  vars sigma = go_sigma (alpha `insertFVSet` bs) vars sigma
      withFlag False (alpha, ka) bs vars sigma = go_sigma (alpha `insertFVSet` go_sigma bs vars ka) vars sigma

instance HasMetaVar Sigma where
  fmv = go_sigma emptyFVSet
    where
      go_rho vars (TauR t) = go_tau vars t
      go_rho vars (AppR sigma phi) = go_sigma vars sigma <> go_sigma vars phi
      go_rho vars (QuantifiedR psi sigma) = case psi of
        ArrowR sg       -> go_sigma vars sg <> go_sigma vars sigma
        Required (_,ka) -> vars <> go_sigma emptyFVSet ka <> go_sigma emptyFVSet sigma

      go_tau vars (ArrowT t1 t2)  = go_tau vars t1 <> go_tau vars t2
      go_tau vars (AppT t1 t2)    = go_tau vars t1 <> go_tau vars t2
      go_tau vars (TyConT _)      = vars
      go_tau vars (AlphaT _)      = vars
      go_tau vars (TcTyVarT tctv) = case tctv of
        MetaTcTv mv -> insertFVSet mv vars
        _               -> vars

      go_sigma vars (QuantifiedSg psi sigma) = case psi of
        Specified (_,ka) -> vars <> go_sigma emptyFVSet ka <> go_sigma emptyFVSet sigma
        Inferred (_,ka)  -> vars <> go_sigma emptyFVSet ka <> go_sigma emptyFVSet sigma
        FatArrow q  -> vars <> go_tau emptyFVSet q <> go_sigma emptyFVSet sigma
      go_sigma vars (RhoSg rho) = go_rho vars rho

instance HasMetaVar Rho where
  fmv = go_rho emptyFVSet
    where
      go_rho vars (TauR t) = go_tau vars t
      go_rho vars (AppR sigma phi) = go_sigma vars sigma <> go_sigma vars phi
      go_rho vars (QuantifiedR psi sigma) = case psi of
        ArrowR sg       -> go_sigma vars sg <> go_sigma vars sigma
        Required (_,ka) -> vars <> go_sigma emptyFVSet ka <> go_sigma emptyFVSet sigma

      go_tau vars (ArrowT t1 t2)  = go_tau vars t1 <> go_tau vars t2
      go_tau vars (AppT t1 t2)    = go_tau vars t1 <> go_tau vars t2
      go_tau vars (TyConT _)      = vars
      go_tau vars (AlphaT _)      = vars
      go_tau vars (TcTyVarT tctv) = case tctv of
        MetaTcTv mv -> insertFVSet mv vars
        _              -> vars

      go_sigma vars (QuantifiedSg psi sigma) = case psi of
        Specified (_,ka) -> vars <> go_sigma emptyFVSet ka <> go_sigma emptyFVSet sigma
        Inferred (_,ka)  -> vars <> go_sigma emptyFVSet ka <> go_sigma emptyFVSet sigma
        FatArrow q  -> vars <> go_tau emptyFVSet q <> go_sigma emptyFVSet sigma
      go_sigma vars (RhoSg rho) = go_rho vars rho

instance HasMetaVar Tau where
  fmv = go_tau emptyFVSet
    where
      go_tau vars (ArrowT t1 t2)  = go_tau vars t1 <> go_tau vars t2
      go_tau vars (AppT t1 t2)    = go_tau vars t1 <> go_tau vars t2
      go_tau vars (TyConT _)      = vars
      go_tau vars (AlphaT _)      = vars
      go_tau vars (TcTyVarT tctv) = case tctv of
        MetaTcTv mv -> insertFVSet mv vars
        _              -> vars


{- Note [Shallow vs Deep free type variables]
Unlike the shallow version, deep free type variables of a type also includes
the free type variables of the kinds of its bound variables.
Example:
  Type                     Shallow     Deep
  ---------------------------------
  (a : (k:Type))           {a}        {a,k}
  forall (a:(k:Type)). a   {k}        {k}
  (a:k->Type) (b:k)        {a,b}      {a,b,k}
-}

-- TODO: I think there is some intricacy about deep/shallow stuff that the code is currently
-- ignoring ...
-- See Note [Shallow vs Deep free type variables]
-- ftv :: Bool -> Sigma -> FVSet TyVar
-- ftv deepFlag = go emptyFVSet emptyFVSet
--   where
--     go bs vars (Alpha alpha)
--       | not (alpha `memberFVSet` bs)
--       = alpha `insertFVSet` vars
--       | otherwise
--       = vars
--     go bs vars (Arrow sg sigma)    = go bs vars sg <> go bs vars sigma
--     go bs vars (App sigma phi)     = go bs vars sigma <> go bs vars phi
--     go bs vars (Required b sigma)  = withFlag deepFlag b bs vars sigma
--     go bs vars (Specified b sigma) = withFlag deepFlag b bs vars sigma
--     go bs vars (Inferred b sigma)  = withFlag deepFlag b bs vars sigma
--     go bs vars (FatArrow _ sigma)  = go bs vars sigma
--     go _ vars _                    = vars

--     withFlag True  (alpha, _) bs  vars sigma = go (alpha `insertFVSet` bs) vars sigma
--     withFlag False (alpha, ka) bs vars sigma = go (alpha `insertFVSet` go bs vars ka) vars sigma

fv :: Term -> FVList Var
fv = nub . go [] []
  where
    go bs fs (TSuperLambda mchs) = go_mchs bs fs mchs
    go bs fs (TApp h aps)
      = case h of
          HVar v -> if not (v `elem` bs)
                   then go_aps bs (v : fs) aps
                   else go_aps bs fs aps
          HConLike _ -> go_aps bs fs aps
          HTerm te -> go bs fs te <> go_aps bs fs aps
          HAnn te1 te2 -> go bs fs te1 <> go bs fs te2 <> go_aps bs fs aps
          HWildCard    -> fs

    go bs fs (TCase e mchs) = go bs fs e <> go_mchs bs fs mchs
    go bs fs (TLet (MkValDecls _ vds) te)
      = let x = go_valbinds bs fs vds
        in x <> go (bs <> x) fs te
    go bs fs (TArrow te1 te2) = go bs fs te1 <> go bs fs te2
    go bs fs (TFatArrow h args te)
      = go bs fs (TApp h args) <> go bs fs te
    go bs fs (TForall _ binder te)
      = case binder of
          UnAnn v -> go (v : bs) fs te
          Ann v _ -> go (v : bs) fs te
    go bs fs (TAs _ te) = go  bs fs te
    go bs fs (TIf cond te ee) = go bs (go bs (go bs fs cond) te) ee
    go _ fs (TLit _)   = fs

    go_aps bs fs (VisArg a : as) = go bs fs a <> go_aps bs fs as
    go_aps bs fs (_ : as)        = go_aps bs fs as
    go_aps _  fs []              = fs

    go_valbinds _ fs [] = fs
    go_valbinds bs fs (vd : vds)
      = (case vd of
           FunBind v mchs -> go_mchs (v : bs) fs mchs
           PatBind p te -> go (bs <> go bs fs p) fs te)
      <> go_valbinds bs fs vds

    go_mchs _ fs [] = fs
    go_mchs bs fs (MkMatch aps e : mchs)
      = go (go_aps bs fs aps) fs e <> go_mchs bs fs mchs

fvs :: [Term] -> FVList Var
fvs = nub . foldMap fv
