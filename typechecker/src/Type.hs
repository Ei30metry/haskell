{-# LANGUAGE ViewPatterns #-}
-- | Syntax of types

module Type(
  -- A polymorphic type: \sigma. See Note [sigma types]
  -- See Note [Invariants of Sigma and Rho]
  Sigma(.., FatArrowS, SpecifiedS, InferredS,
        RequiredS, ArrowS, ArrowTS, ArrowRS,
        AppTS, AppRS, AppS, TcTyVarS, AlphaS,
        TyConS, QuantifiedS), Phi,
  -- A polymorphic "kind". See Note [kappa types]
  Kappa,
  -- A top-level monomorphic type: \rho. See Note [rho types]
  -- Note [Invariants of Sigma and Rho]
  Rho(QuantifiedR, TauR, AppR),
  -- A monomorhic type: \tau, \iota, \mathit{Q}. See Note [tau types]
  Tau(..), Iota,

  Q, -- A monomorphic type of kind \mathit{Constraint}. See Note [Q types]

  -- A \sigma that can have TcTyVars in it
  TcKappa, TcPhi, TcQ, TcRho, TcSigma, TcTau,

  typeSigma,
  typeToTypeSigma,
  constraintSigma,
  typeToConstraintSigma,

  boolTau,
  chatTau,
  constraintTau,
  eqCtQ,
  listTau,
  numCtQ,
  stringTau,
  typeTau,
  showCtQ,
  foldableCtQ,
  intTau,
  maybeTau,
  eitherTau,
  tuple2Tau,
  tuple3Tau,
  tuple4Tau,

  tauToSigma,
  rhoToSigma,
  tauToRho,
  rhoToTau,
  sigmaToTau,
  sigmaToRho,

  appHeadToArgsT,
  mkFatArrowS,
  mkInferredS,
  mkSpecifiedS,
  appArrowT,
  appPsiSigmas,
  appArrowR,
  splitAppR,
  splitAppT,
  splitArrowR,
  splitArrowT,
  splitArrowS,
  splitAppArrowT,
  splitEqPredT,
  splitFatArrowS,

  isTauTypeS,
  isTauTypeR,
  isTcTyVarT,
  isTyConT,
  isRhoTypeS,
  isSkolTcTvT,
  isEqPredQ,
  isMetaTcTvT,
  isMuTcTvT,
  isGuarded,
  eqSigma,
  eqKappa,
  eqMetaTyVar,

  -- Quantifiers: \psi
  Psi(.., ArrowP, FatArrowP
        , SpecifiedP, InferredP
        , RequiredP),
  PsiR(..), -- Quantifiers of \rho
  PsiSg(..), -- Quantifiers of \sigma
  Binding, -- Forall-bound quantifiee
  isArrowP,
  isFatArrowP,
  isForallP,
  isInferredP,
  isInvisForallP,
  isSpecifiedP,
  isRequiredP,
  isVisibleP,
  forallQuantifieP,

  appLOmegaSkPsi,
  appBOmegaSkPsi,

  appLThetaPsi,
  appBThetaPsi,

  appLOmegaPsi,
  appBOmegaPsi,

  -- A forall-bound type variable (in some phases, these could appear freely)
  TyVar(..),

  DataConType(..), -- See Note [Data constructor and pattern synonym types]
  fromDataConType,
  PatSynType(..),  -- See Note [Data constructor and pattern synonym types]
  fromPatSynType,

  -- A type variables used during checking and inference
  TcTyVar(..),
  tcTyVarLevel,
  isMuVarTcTyVar,
  isMetaVarTcTyVar,
  isSkolTcTyVar,

  metaTyVarT,
  skolTyVarT,
  muTyVarT,

  -- A meta (unification) variable
  MetaTyVar(..),
  MetaDetails(..),
  MetaInfo(..),

  -- A skolem type variable
  SkolTyVar(..),

  -- Instantiation variable: \mu
  MuTyVar(..),

  -- Type constructor: \mathit{T}
  TyCon(..),
  tyConArrow,
  tyConBool,
  tyConChar,
  tyConConstraint,
  tyConEither,
  tyConEqCt,
  tyConFoldableCt,
  tyConInt,
  tyConList,
  tyConMaybe,
  tyConNomEq,
  tyConNumCt,
  tyConShowCt,
  tyConString,
  tyConType,
  eqTyCon,
  tyConTuple2,
  tyConTuple3,
  tyConTuple4,

  -- Pretty printing functions
  pprArrow,
  pprBinding,
  pprDot,
  pprFatArrow,
  pprForall,
  pprMetaVar,
  pprMetaVars,
  pprMetaDetails,
  pprMuVar,
  pprPsi,
  pprPsis,
  pprTidiedSigma,
  pprTidiedSigmas,
  pprSigma, -- Produces untidied output. See [Tidying before printing]
  pprTau,
  pprRho,
  pprSigmas,
  pprRhos,
  pprTaus,
  pprSkol,
  pprSkols,
  pprSkolKind,
  pprSkolKinds,
  pprTcTyVar,
  pprTilde,
  pprTyCon,
  pprTyVar,
  ) where

import Tc.Subst
import BasicTypes
import {-# SOURCE #-} Tc.Types
import Data.Foldable

import Data.Text (unpack, pack, Text)
import UniqMap
import Prettyprinter
import Utils (pprList)
import Data.Kind (Type)
import Data.Bifunctor


{- *********************************************************************
*                                                                      *
                  Types: Psi, Sigma, Rho, Tau, TyCon
*                                                                      *
********************************************************************* -}

data Psi
  = PsiR PsiR
  | PsiSg PsiSg
  | PsiT Tau
  deriving (Show)

data PsiR
  = Required Binding -- forall a : ka ->
  | ArrowR Sigma     -- sg ->
  deriving (Show)

data PsiSg
  = Specified Binding -- forall a : ka.
  | Inferred Binding  -- forall {a : ka}.
  | FatArrow Q        -- Q =>
  deriving (Show)

-- TODO: Note [Alpha: Type variables]
-- See Note [Invariants of Sigma and Rho]
-- Corresponding to \sigma in the paper. See Note [sigma types]
data Sigma
  = QuantifiedSg PsiSg Sigma
  | RhoSg Rho
  deriving Show

{- Note [tau types]
A completly monomorphic type. The Arrow quantifier is the only
quantifier a tau-type can have.
-}

-- \tau: monomorphic type
-- See Note [tau types]
data Tau
  = TyConT TyCon
  | ArrowT Tau Tau
  | AppT Tau Tau
  | AlphaT TyVar
  | TcTyVarT TcTyVar
  deriving Show

{- Note [rho types]
A sigma that doesn't have a top-level:
 - forall a : ka ->
 - forall |a : ka|.
 - Q =>
-}

-- \rho: toplevel monomorphic type
-- See Note [rho types]
data Rho
  = QuantifiedRho PsiR Sigma
  | TauRho Tau
  | AppRho Sigma Sigma
  deriving Show

type Binding = (TyVar, Kappa) -- TODO: Ask Simon: Shouldn't this be a tau?


{- Note [Q types]
Conventionally used to denote Non-canonical constraints that normally
appear to the left of a FatArrow (=>).
Invariant: Q is of kind /Constraint/
-}

type Q = Tau -- See Note [Q types]

type Phi   = Sigma

{- Note [kappa types]
We conventionally use kappa in places where we expect kinds.
Invariant: kappa is of kind /Type/.
-}

type Kappa = Sigma -- See Note [kappa types]

type Iota  = Tau

-- Types that might have TcTyVars in them.
type TcSigma = Sigma
type TcPhi   = Phi
type TcKappa = Kappa
type TcTau   = Tau
type TcRho   = Rho
type TcQ     = Q

{- Note [Invariants of Sigma and Rho]
Invariants:
  - A (tau1 -> tau2) should always be stored using ArrowT
  - A (tau1 tau2) should always be stored using AppT

Considering that Sigma and Rho are mutually recursive, and that
there is a good amount of duplication in Rho and Sigma, in order to
define the sigmaToTau function as the simple pattern match:

sigmaToTau (RhoSg (TauR t)) = Right t
sigmaToTau _                = Left CantConvertSigmaToTau

We need to be very careful when dealing with constructors that are present
in both taus and rhos.

More specifically:
- AppT, AppR
- ArrowR, ArrowT

This is why we should use smart constructors that internally use the above functions and
guarantee that a tau-type is always stored as a tau in our type!
See the definitions for constructArrowS and constructAppS, ArrowS, ArrowR, AppS, AppR,
FatArrowR, FatArrowS, and QuantifiedS.
-}

-----------------------------------------------
---- Changing back and forth between types ----
-----------------------------------------------

tauToSigma :: Tau -> Sigma
tauToSigma = RhoSg . TauR

rhoToSigma :: Rho -> Sigma
rhoToSigma = RhoSg

tauToRho :: Tau -> Rho
tauToRho = TauR

rhoToTau :: Rho -> Either TcError Tau
rhoToTau (TauR t) = Right t
rhoToTau r        = Left undefined -- (CantConvertToTau (pprRho r))

sigmaToTau :: Sigma -> Either TcError Tau
sigmaToTau (RhoSg (TauR t)) = Right t
sigmaToTau s                = Left (CantConvertToTau (pprSigma s))

sigmaToRho :: Sigma -> Either TcError Rho
sigmaToRho (RhoSg r) = Right r
sigmaToRho s         = Left (CantConvertToRho (pprSigma s))

-- See Note [Invariants of Sigma and Rho]
constructArrowS :: Sigma -> Sigma -> Sigma
constructArrowS (RhoSg (TauR t1)) (RhoSg (TauR t2)) = tauToSigma (ArrowT t1 t2)
constructArrowS sg1 sg2                             = rhoToSigma (QuantifiedRho (ArrowR sg1) sg2)

-- See Note [Invariants of Sigma and Rho]
constructAppS :: Sigma -> Sigma -> Sigma
constructAppS (RhoSg (TauR t1)) (RhoSg (TauR t2)) = tauToSigma (AppT t1 t2)
constructAppS sg1 sg2                             = rhoToSigma (AppRho sg1 sg2)

-- See Note [Invariants of Sigma and Rho]
constructAppR :: Sigma -> Sigma -> Rho
constructAppR (RhoSg (TauR t1)) (RhoSg (TauR t2)) = tauToRho (AppT t1 t2)
constructAppR sg1 sg2                             = AppRho sg1 sg2

-- See Note [Invariants of Sigma and Rho]
constructQuantifiedR :: PsiR -> Sigma -> Rho
constructQuantifiedR (Required b) sg
  = QuantifiedRho (Required b) sg
constructQuantifiedR (ArrowR (RhoSg (TauR t1))) (RhoSg (TauR t2))
  = TauR (ArrowT t1 t2)
constructQuantifiedR (ArrowR sg1) sg2
  = QuantifiedRho (ArrowR sg1) sg2

{- *********************************************************************
*                                                                      *
     Pattern synonyms for Sigma, Rho, PsiR, and Psi
*                                                                      *
********************************************************************* -}

-----------------------------------------------------
---- Smart pattern synonym constructors for PsiR ----
-----------------------------------------------------

pattern TauR :: Tau -> Rho
pattern TauR t = TauRho t

-- See Note [Invariants of Sigma and Rho]
pattern AppR :: Sigma -> Sigma -> Rho
pattern AppR sg1 sg2 <- AppRho sg1 sg2
  where
    AppR = constructAppR

-- See Note [Invariants of Sigma and Rho]
pattern QuantifiedR :: PsiR -> Sigma -> Rho
pattern QuantifiedR psir sg <- QuantifiedRho psir sg
  where
    QuantifiedR = constructQuantifiedR

{-# COMPLETE QuantifiedR, AppR, TauR #-}

-- See Note [Invariants of Sigma and Rho]

----------------------------------------------------
---- Smart pattern synonym constructors for Psi ----
----------------------------------------------------

pattern ArrowP :: Sigma -> Psi
pattern ArrowP sg <- (extractArrowP -> Just sg)
  where
    ArrowP (RhoSg (TauR t)) = PsiT t
    ArrowP sg               = PsiR (ArrowR sg)

pattern FatArrowP :: Q -> Psi
pattern FatArrowP q  = PsiSg (FatArrow q)

pattern SpecifiedP :: Binding -> Psi
pattern SpecifiedP b = PsiSg (Specified b)

pattern RequiredP :: Binding -> Psi
pattern RequiredP b = PsiR (Required b)

pattern InferredP :: Binding -> Psi
pattern InferredP b = PsiSg (Inferred b)

{-# COMPLETE ArrowP, FatArrowP, SpecifiedP, RequiredP, InferredP #-}

------------------------------------------------------
---- Smart pattern synonym constructors for Sigma ----
------------------------------------------------------

-- See Note [Invariants of Sigma and Rho]
pattern QuantifiedS :: Psi -> Sigma -> Sigma
pattern QuantifiedS psi sg <- (splitToPsiAndSigma -> Just (psi, sg))
  where
    QuantifiedS (PsiSg psi) sg = QuantifiedSg psi sg
    QuantifiedS (PsiR psi) sg = RhoSg (QuantifiedR psi sg)
    QuantifiedS (PsiT tau) sg = case sg of
      (RhoSg (TauR io)) -> RhoSg (TauR (ArrowT tau io))
      _                 -> RhoSg (QuantifiedRho (ArrowR (tauToSigma tau)) sg)

pattern FatArrowS :: Q -> Sigma -> Sigma
pattern FatArrowS q sg = QuantifiedSg (FatArrow q) sg

pattern SpecifiedS :: Binding -> Sigma -> Sigma
pattern SpecifiedS b sg = QuantifiedSg (Specified b) sg

pattern InferredS :: Binding -> Sigma -> Sigma
pattern InferredS b sg = QuantifiedSg (Inferred b) sg

pattern RequiredS :: Binding -> Sigma -> Sigma
pattern RequiredS b sg = RhoSg (QuantifiedR (Required b) sg)

pattern ArrowS :: Sigma -> Sigma -> Sigma
pattern ArrowS sg1 sg2 <- (splitArrowS -> Just (sg1, sg2))
  where
    ArrowS = constructArrowS

pattern ArrowTS :: Tau -> Tau -> Sigma
pattern ArrowTS t1 t2 = RhoSg (TauR (ArrowT t1 t2))

pattern ArrowRS :: Sigma -> Sigma -> Sigma
pattern ArrowRS sg1 sg2 = RhoSg (QuantifiedR (ArrowR sg1) sg2)

pattern AppTS :: Tau -> Tau -> Sigma
pattern AppTS t1 t2 = RhoSg (TauR (AppT t1 t2))

pattern AppRS :: Sigma -> Sigma -> Sigma
pattern AppRS sg1 sg2 = RhoSg (AppR sg1 sg2)

pattern AppS :: Sigma -> Sigma  -> Sigma
pattern AppS sg1 sg2 <- (splitAppS -> Just (sg1, sg2))
  where
    AppS = constructAppS

pattern TcTyVarS :: TcTyVar -> Sigma
pattern TcTyVarS tv = RhoSg (TauR (TcTyVarT tv))

pattern AlphaS :: TyVar -> Sigma
pattern AlphaS tv = RhoSg (TauR (AlphaT tv))

pattern TyConS :: TyCon -> Sigma
pattern TyConS tc = RhoSg (TauR (TyConT tc))

{-# COMPLETE ArrowS, FatArrowS, InferredS,
             SpecifiedS, RequiredS,
             AppS, TcTyVarS, AlphaS,
             TyConS  #-}

{-# COMPLETE ArrowRS, ArrowTS, FatArrowS,
             InferredS, SpecifiedS, RequiredS,
             AppRS, AppTS, TcTyVarS, AlphaS,
             TyConS  #-}

--------------------------------------------------------------------
---- Predicates and utility functions for types and quantifiers ----
--------------------------------------------------------------------

forallQuantifieP :: Psi -> Maybe (TyVar, Kappa)
forallQuantifieP (PsiR (Required binding))   = Just binding
forallQuantifieP (PsiSg (Specified binding)) = Just binding
forallQuantifieP (PsiSg (Inferred binding))  = Just binding
forallQuantifieP _                           = Nothing

isInvisForallP :: Psi -> Bool
isInvisForallP (PsiSg (Inferred _))  = True
isInvisForallP (PsiSg (Specified _)) = True
isInvisForallP _                     = False

isForallP :: Psi -> Bool
isForallP psi = isInvisForallP psi || isRequiredP psi

isSpecifiedP :: Psi -> Bool
isSpecifiedP (PsiSg (Specified _)) = True
isSpecifiedP _                     = False

isInferredP :: Psi -> Bool
isInferredP (PsiSg (Inferred _)) = True
isInferredP _                    = False

isFatArrowP :: Psi -> Bool
isFatArrowP (PsiSg (FatArrow _)) = True
isFatArrowP _                    = False

isRequiredP :: Psi -> Bool
isRequiredP (PsiR (Required _)) = True
isRequiredP _                   = False

isArrowP :: Psi -> Bool
isArrowP (PsiT _)          = True
isArrowP (PsiR (ArrowR _)) = True
isArrowP _                 = False

isVisibleP :: Psi -> Bool
isVisibleP psi = isRequiredP psi || isArrowP psi

splitAppArrowT :: Tau -> Maybe (Tau, Tau)
splitAppArrowT (AppT t1 t2)   = Just (t1, t2)
splitAppArrowT (ArrowT t1 t2) = Just (t1, t2)
splitAppArrowT _              = Nothing

splitArrowS :: Sigma -> Maybe (Sigma, Sigma)
splitArrowS (RhoSg rho) = case rho of
  TauR (ArrowT t1 t2)          -> Just $ (tauToSigma t1, tauToSigma t2)
  QuantifiedR (ArrowR sg1) sg2 -> Just $ (sg1, sg2)
  _                            -> Nothing
splitArrowS _ = Nothing

splitAppS :: Sigma -> Maybe (Sigma, Sigma)
splitAppS (RhoSg rho) = case rho of
  TauR (AppT t1 t2) -> Just $ (tauToSigma t1, tauToSigma t2)
  AppR sg1 sg2      -> Just $ (sg1, sg2)
  _                 -> Nothing
splitAppS _ = Nothing

splitToPsiAndSigma :: Sigma -> Maybe (Psi, Sigma)
splitToPsiAndSigma (QuantifiedSg psi sg) = Just (PsiSg psi, sg)
splitToPsiAndSigma (RhoSg r)             = case r of
  QuantifiedR psi sg  -> Just (PsiR psi, sg)
  TauR (ArrowT t1 t2) -> Just (PsiT t1, tauToSigma t2)
  _                   -> Nothing

extractArrowP :: Psi -> Maybe Sigma
extractArrowP (PsiR (ArrowR sg)) = Just sg
extractArrowP (PsiT t)           = Just (tauToSigma t)
extractArrowP _                  = Nothing

-- See paper: A Quick look at impredicativity. Serrano et al, 5.3
-- See rule ArgQl-App in the spec
isGuarded :: Rho -> Bool
isGuarded (TauR t) = isGuardedTau t
  where
    isGuardedTau (ArrowT _ _)        = True
    isGuardedTau _                   = False
isGuarded (AppR sg ph) = isGuardedSigma sg &&
                         isGuardedSigma ph
  where
   isGuardedSigma (RhoSg rho) = isGuarded rho
   isGuardedSigma _           = False
isGuarded (QuantifiedR (ArrowR _) _) = True

isRhoTypeS :: Sigma -> Bool
isRhoTypeS (RhoSg _) = True
isRhoTypeS _         = False

isTauTypeS :: Sigma -> Bool
isTauTypeS (RhoSg rho) = isTauTypeR rho
isTauTypeS _ = False

isTyConT :: Tau -> Bool
isTyConT (TyConT _) = True
isTyConT _          = False

splitAppR :: Rho -> Maybe (Sigma, Sigma)
splitAppR (AppR sg1 sg2) = Just (sg1, sg2)
splitAppR (TauR t)       = bimap (RhoSg . TauR) (RhoSg . TauR) <$> splitAppT t
splitAppR _              = Nothing

splitAppT :: Tau -> Maybe (Tau, Tau)
splitAppT (AppT t1 t2) = Just (t1, t2)
splitAppT _            = Nothing

splitArrowT :: Tau -> Maybe (Tau, Tau)
splitArrowT (ArrowT arg res) = Just (arg, res)
splitArrowT _                = Nothing

splitArrowR :: Rho -> Maybe (Sigma, Sigma)
splitArrowR (QuantifiedR (ArrowR arg) res) = Just (arg, res)
splitArrowR _                              = Nothing

splitFatArrowS :: Sigma -> Maybe (Q, Sigma)
splitFatArrowS (QuantifiedSg (FatArrow q) sg) = Just (q, sg)
splitFatArrowS _                              = Nothing

mkInferredS :: [Binding] -> Sigma -> Sigma
mkInferredS bs sigma = foldr InferredS sigma bs

mkSpecifiedS :: [Binding] -> Sigma -> Sigma
mkSpecifiedS bs sigma = foldr SpecifiedS sigma bs

mkFatArrowS :: [Q] -> Sigma -> Sigma
mkFatArrowS qs sigma = foldr FatArrowS sigma qs

appArrowT :: [Tau] -> Tau -> Tau
appArrowT args res = foldr ArrowT res args

appArrowR :: [Sigma] -> Rho -> Rho
appArrowR sgs res = foldr step res sgs
  where
    step sg r = QuantifiedR (ArrowR sg) (rhoToSigma r)

appPsiSigmas :: [PsiSg] -> Sigma -> Sigma
appPsiSigmas args res = foldr QuantifiedSg res args

appHeadToArgsT :: Tau -> [Tau] -> Tau
appHeadToArgsT head = foldl AppT head

-- TODO: This requires alpha-renaming.
eqSigma :: Kappa -> Kappa -> Bool
eqSigma = undefined
-- eqSigma (Arrow sg1 ph1) (Arrow sg2 ph2) = sg1 `eqSigma` sg2 && ph1 `eqSigma` ph2
-- eqSigma (FatArrow q1 sg1) (FatArrow q2 sg2) = sg1 `eqSigma` sg2 && q1 `eqSigma`  q2 -- TODO: Fix me
-- eqSigma (Required (a1, ka1) sg1) (Required (a2, ka2) sg2) = sg1 `eqSigma` sg2 && ka1 `eqSigma` ka2 -- TODO: Fix me
-- eqSigma (Specified (a1, ka1) sg1) (Specified (a2, ka2) sg2) = sg1 `eqSigma` sg2 && ka1 `eqSigma` ka2 -- TODO: Fix me
-- eqSigma (Inferred (a1, ka1) sg1) (Inferred (a2, ka2) sg2) = sg1 `eqSigma` sg2 && ka1 `eqSigma` ka2 -- TODO: Fix me
-- eqSigma (App sg1 ph1) (App sg2 ph2) = sg1 `eqSigma` sg2 && ph1 `eqSigma` ph2
-- eqSigma (Alpha al) (Alpha be) = undefined -- TODO: Fix me
-- eqSigma (TyCon t1) (TyCon t2) = t1 == t2
-- eqSigma (TcTyVar _) (TcTyVar _) = undefined

eqKappa :: Kappa -> Kappa -> Bool
eqKappa = eqSigma

eqTau :: Tau -> Tau -> Bool
eqTau (AlphaT tv1) (AlphaT tv2)           = undefined
eqTau (TyConT tc1) (TyConT tc2)           = undefined
eqTau (ArrowT tau1 io1) (ArrowT tau2 io2) = undefined
eqTau (AppT tau1 io1) (AppT tau2 io2)     = undefined
eqTau (TcTyVarT tctv1) (TcTyVarT tctv2)   = undefined

eqRho :: Rho -> Rho -> Bool
eqRho = undefined

splitEqPredT :: TcQ -> Maybe (TcTau, TcTau)
splitEqPredT q
  | AppT (AppT (TyConT tycon) sg) ph <- q
  , tycon == tyConNomEq
  = Just (sg ,ph)
  | otherwise
  = Nothing

isEqPredQ :: TcQ -> Bool
isEqPredQ q
  | AppT (AppT _ (TyConT tyCon)) _ <- q
  , tyCon == tyConNomEq
  = True
  | otherwise
  = False

isTauTypeR :: Rho -> Bool
isTauTypeR (TauR _) = True

isTcTyVarT :: Tau -> Bool
isTcTyVarT (TcTyVarT _) = True
isTcTyVarT _           = False

isMetaTcTvT :: Tau -> Bool
isMetaTcTvT (TcTyVarT (MetaTcTv _)) = True
isMetaTcTvT _                       = False

isSkolTcTvT :: Tau -> Bool
isSkolTcTvT (TcTyVarT (SkolTcTv _)) = True
isSkolTcTvT _                       = False

isMuTcTvT :: Tau -> Bool
isMuTcTvT (TcTyVarT (MuTcTv _)) = True
isMuTcTvT _                     = False

{- *********************************************************************
*                                                                      *
     Type variables: TyVar, TcTyVar, SkolTyVar, MetaTyVar, MuTyVar
*                                                                      *
********************************************************************* -}

{- Note [Meta variables]
A Meta variable (or unification variable) is a variable that stands for a sigma (type)
-}

data TcTyVar
  = SkolTcTv SkolTyVar
  | MuTcTv MuTyVar
  | MetaTcTv MetaTyVar -- Note [Meta variables]
  deriving Show

-- Quantified type variables
-- Bound
data TyVar =
  MkTv
    { tyVarText   :: Text
    , tyVarUnique :: Unique
    }

-- Skolemised type variable
data SkolTyVar =
  MkSkolTv
    { skolTyVar :: TyVar
    , skolLvl   :: TcLevel }
    deriving Show

-- Meta/Unification variable
data MetaTyVar =
  MkMetaTv
    { metaVarText    :: Text
    , metaVarUnique  :: Unique
    , metaVarLvl     :: TcLevel
    , metaVarKind    :: TcKappa
    , metaVarInfo    :: MetaInfo
    , metaVarDetails :: TcRef MetaDetails
    }

data MetaDetails
  = Flexi
  | Indirect TcTau
  deriving Show

data MetaInfo
  = CycleBreakerTv
  | TauTv
  | TyVarTv
  deriving (Show, Eq)

-- Mu/Instantiation variable
-- TODO: Ask Simon: Should I call this MuInstVar?
data MuTyVar =
  MkMuTv
    { muVarText   :: Text
    , muVarUnique :: Unique
    , muVarKind   :: Kappa
    }
    deriving Show

instance Show TyVar where
  show (MkTv { tyVarText   = sk
             , tyVarUnique = un }) = unpack sk <> show un

instance Show MetaTyVar where
  show (MkMetaTv { metaVarText = str
                 , metaVarUnique = uniq }) = unpack str ++ show uniq


instance Eq MetaTyVar where
  al1 == al2 = getUnique al1 == getUnique al2

instance Eq MuTyVar where
  vu == mu = getUnique vu == getUnique mu

instance Eq TyVar where
  tv1 == tv2 = getUnique tv1 == getUnique tv2

instance Eq SkolTyVar where
  sk1 == sk2 = getUnique sk1 == getUnique sk2

instance Uniquable TyVar where
  getUnique = tyVarUnique

instance Uniquable MetaTyVar where
  getUnique = metaVarUnique

instance Uniquable MuTyVar where
  getUnique (MkMuTv { muVarUnique = uniq }) = uniq

instance Uniquable SkolTyVar where
  getUnique = getUnique . skolTyVar

instance Uniquable TcTyVar where
  getUnique (SkolTcTv sk)  = getUnique sk
  getUnique (MuTcTv mu)   = getUnique mu
  getUnique (MetaTcTv mv) = getUnique mv

eqTcTyVar :: TcTyVar -> TcTyVar -> Bool
eqTcTyVar (SkolTcTv sk1) (SkolTcTv sk2) = undefined
eqTcTyVar (MuTcTv sk1) (MuTcTv sk2) = undefined
eqTcTyVar (MetaTcTv sk1) (MetaTcTv sk2) = undefined

eqMetaTyVar :: MetaTyVar -> MetaTyVar -> Bool
eqMetaTyVar (MkMetaTv { metaVarUnique = u1 })
            (MkMetaTv { metaVarUnique = u2 }) = u1 == u2

isSkolTcTyVar :: TcTyVar -> Bool
isSkolTcTyVar (SkolTcTv _) = True
isSkolTcTyVar _               = False

isMetaVarTcTyVar :: TcTyVar -> Bool
isMetaVarTcTyVar (MetaTcTv _) = True
isMetaVarTcTyVar _                = False

isMuVarTcTyVar :: TcTyVar -> Bool
isMuVarTcTyVar (MuTcTv _) = True
isMuVarTcTyVar _              = False

tcTyVarLevel :: TcTyVar -> Maybe TcLevel
tcTyVarLevel (MuTcTv _)    = Nothing
tcTyVarLevel (SkolTcTv sk)    = Just $ skolLvl sk
tcTyVarLevel (MetaTcTv mv) = Just $ metaVarLvl mv

muTyVarT :: MuTyVar -> Tau
muTyVarT = TcTyVarT . MuTcTv

skolTyVarT :: SkolTyVar -> Tau
skolTyVarT = TcTyVarT . SkolTcTv

metaTyVarT :: MetaTyVar -> Tau
metaTyVarT = TcTyVarT . MetaTcTv

{- *********************************************************************
*                                                                      *
                        Type Constructors
*                                                                      *
********************************************************************* -}

-- Uniquely defined by their String name
-- All Tycons are built-on! (At least for now.)
-- \mathit{T} in the spec
newtype TyCon = MkTyCon Text
  deriving Eq

instance Show TyCon where
  show (MkTyCon x) = unpack x

-----------------------------------------
---- Tau and Sigma type constructors ----
-----------------------------------------

typeSigma :: Sigma
typeSigma = tauToSigma typeTau

typeToTypeSigma :: Sigma
typeToTypeSigma = tauToSigma (ArrowT typeTau typeTau)

constraintSigma :: Sigma
constraintSigma = tauToSigma typeTau

typeToConstraintSigma :: Sigma
typeToConstraintSigma = tauToSigma (ArrowT typeTau constraintTau)

typeTau :: Tau
typeTau = TyConT tyConType

constraintTau :: Tau
constraintTau = TyConT tyConConstraint

intTau :: Tau
intTau = TyConT tyConInt

boolTau :: Tau
boolTau = TyConT tyConBool

stringTau :: Tau
stringTau = TyConT tyConString

listTau :: Tau
listTau = TyConT tyConList

chatTau :: Tau
chatTau = TyConT tyConChar

foldableCtQ :: Q
foldableCtQ = TyConT tyConFoldableCt

eqCtQ :: Q
eqCtQ = TyConT tyConEqCt

numCtQ :: Q
numCtQ = TyConT tyConNumCt

-- See TODO: Note [Substitution might turn a tau into a sigma]
unitCtQ :: Q
unitCtQ = TyConT tyConUnitCt

showCtQ :: Q
showCtQ = TyConT tyConShowCt

eitherTau :: Tau
eitherTau = TyConT tyConEither

maybeTau :: Tau
maybeTau = TyConT tyConMaybe

tuple2Tau :: Tau
tuple2Tau = TyConT tyConTuple2

tuple3Tau :: Tau
tuple3Tau = TyConT tyConTuple3

tuple4Tau :: Tau
tuple4Tau = TyConT tyConTuple4

---------------------------
---- Type constructors ----
---------------------------

tyConType :: TyCon
tyConType = MkTyCon (pack "Type")

tyConConstraint :: TyCon
tyConConstraint = MkTyCon (pack "Constraint")

tyConNomEq :: TyCon
tyConNomEq = MkTyCon (pack "~")

tyConArrow :: TyCon
tyConArrow = MkTyCon (pack "(->)")

tyConUnitCt :: TyCon
tyConUnitCt = MkTyCon (pack "()")

tyConShowCt :: TyCon
tyConShowCt = MkTyCon (pack "Show")

tyConEqCt :: TyCon
tyConEqCt = MkTyCon (pack "Eq")

tyConFoldableCt :: TyCon
tyConFoldableCt = MkTyCon (pack "Foldable")

tyConNumCt :: TyCon
tyConNumCt = MkTyCon (pack "Num")

tyConInt :: TyCon
tyConInt = MkTyCon (pack "Int")

-- TODO: Make sure to make this a
-- type synonym when we have support for it.
tyConString :: TyCon
tyConString = MkTyCon (pack "String")

tyConBool :: TyCon
tyConBool = MkTyCon (pack "Bool")

tyConChar :: TyCon
tyConChar = MkTyCon (pack "Char")

tyConList :: TyCon
tyConList = MkTyCon (pack "List")

tyConMaybe :: TyCon
tyConMaybe = MkTyCon (pack "Maybe")

tyConEither :: TyCon
tyConEither = MkTyCon (pack "Either")

tyConTuple2 :: TyCon
tyConTuple2 = MkTyCon (pack "Tuple2")

tyConTuple3 :: TyCon
tyConTuple3 = MkTyCon (pack "Tuple3")

tyConTuple4 :: TyCon
tyConTuple4 = MkTyCon (pack "Tuple4")

-------------------------------------------------
---- TyCon: Predicates and utility functions ----
-------------------------------------------------

eqTyCon :: TyCon -> TyCon -> Bool
eqTyCon = (==)


{- *********************************************************************
*                                                                      *
  Data constructor and pattern synonym types: DataConType, PatSynType
*                                                                      *
********************************************************************* -}

{- Note [Data constructor and pattern synonym types]
Both in the rules and in the implementation, we structure data constructor
and pattern synoym types before putting them in Gamma. These special forms are much more
convinient to work with, and in the case of pattern synonyms, it's actually necessary to do so.
This is kind of similiar to GHC's rejigging algorithm.

Here is an example of a datatype declaration:

data ExTyCon a b where
  ExDataCon :: forall a : Type. a -> Int -> ExTyCon a Int

Its normal sigma type:
  ExDataCon:  forall a:Type. a -> Int -> ExTyCon a Int

And its More structurd DataConType representation:
  ExDataCon: MkDataCon { dctPsis = [(forall a:Type.), (a ->), (Int ->)]
                       , dctTyCon = ExTyCon
                       , dctArgs = [a, Int]}

Here is an example of a pattern synonym declaration:

pattern ExPatSyn :: forall a. (Num a, Show a) => Show a => (a -> a) -> a -> a
pattern ExPatSyn x <- (const id -> x)

Its Normal sigma type:
  ExPatSyn: forall a. (Num a, Show a) => Show a => (a -> a) -> a -> a

And its more structured PatSynType representation:
  ExPatSyn: MkPatSynType { pstUnivs = [a:Type]
                         , pstReqs  = [Num a, Show a]
                         , pstPsis  = [(Show a =>), ((a -> a) ->)]
                         , pstRes   = a -> a}

As we can see, the pattern synonym type declared by the user neither reveals
the arity nor the result of the match; which are both learned upon looking
at the declaration itself. This is the exact reason why we need to
-}

-- See Note [Data constructor and pattern synonym types]
data DataConType =
  MkDataConType
    { dctPsis  :: [Psi]
    , dctTyCon :: TyCon
    , dctArgs  :: [Tau]
    } deriving Show

-- See Note [Data constructor and pattern synonym types]
data PatSynType =
  MkPatSynType
    { pstUnivs :: [Binding]
    , pstReqs  :: [Q]
    , pstPsis  :: [Psi]
    , pstRes   :: Sigma
    } deriving Show

-- See Note [Data constructor and pattrern synonym types]
fromDataConType :: DataConType -> Sigma
fromDataConType (MkDataConType psis tycon args)
  = foldr QuantifiedS (tauToSigma $ foldl AppT (TyConT tycon) args) psis

-- See Note [Data constructor and pattrern synonym types]
fromPatSynType :: PatSynType -> Sigma
fromPatSynType (MkPatSynType bindings reqs psis res)
   = flip (foldr SpecifiedS) bindings
   $ flip (foldr FatArrowS) reqs
   $ foldr QuantifiedS res psis

-- TODO: Ask Simon: Should I use syb or uniplate to get rid of the boilerplate?
{-
TODO: Ask Simon: We don't have shadowing in Sigmas and Rhos, so I didn't
implement a bound-check. But this also means that if we forget to strip of the
quantifiee from the type, it would become a dangling quantifiee! bad.
-}

{- *********************************************************************
*                                                                      *
            Substitution instances for Sigma, Rho, and Tau
*                                                                      *
********************************************************************* -}

-- See Note [Substitution might turn a tau into a sigma]
handleForallInQ :: Sigma -> Tau
handleForallInQ (RhoSg (TauR t)) = t
handleForallInQ _                = unitCtQ

appBOmegaTau :: UniqSet TyVar -> BOmega -> Tau -> Sigma
appBOmegaTau bs s (ArrowT t1 t2)
  = ArrowS (appBOmegaTau bs s t1) (appBOmegaTau bs s t2)
appBOmegaTau bs s (AppT t1 t2)
  = AppS (appBOmegaTau bs s t1) (appBOmegaTau bs s t2)
appBOmegaTau _ _ t@(TcTyVarT _)
  = tauToSigma t
appBOmegaTau _ _ t@(TyConT _  )
  = tauToSigma t
appBOmegaTau bs s t@(AlphaT tv)
  = if not (tv `memberUM` bs)
      then case lookupSubst s tv of
             Nothing  -> tauToSigma t
             Just sigma -> sigma
      else tauToSigma t

appLOmegaTau :: UniqSet TyVar -> LOmega -> Tau -> Tau
appLOmegaTau bs s (ArrowT t1 t2)
  = ArrowT (appLOmegaTau bs s t1) (appLOmegaTau bs s t2)
appLOmegaTau bs s (AppT t1 t2)
  = AppT (appLOmegaTau bs s t1) (appLOmegaTau bs s t2)
appLOmegaTau _ _ t@(TcTyVarT _) = t
appLOmegaTau _ _ t@(TyConT _  ) = t
appLOmegaTau bs s t@(AlphaT tv)
  = if not (tv `memberUM` bs)
      then case lookupSubst s tv of
             Nothing  -> t
             Just tau -> tau
      else t

appBOmegaRho :: UniqSet TyVar -> BOmega -> Rho -> Sigma
appBOmegaRho bs s (TauR t)
  = appBOmegaTau bs s t
appBOmegaRho bs s (AppR sg1 sg2)
  = AppS (appBOmegaSigma bs s sg1) (appBOmegaSigma bs s sg2)
appBOmegaRho bs s (QuantifiedR psi sg) = case psi of
  ArrowR ph -> ArrowS (appBOmegaSigma bs s ph) (appBOmegaSigma bs s sg)
  Required (a, ka) -> RequiredS (a, appBOmegaSigma bs s ka)
                                (appBOmegaSigma (addToUM' bs a) s sg)

appBOmegaSigma :: UniqSet TyVar -> BOmega -> Sigma -> Sigma
appBOmegaSigma bs s (RhoSg rho) = appBOmegaRho bs s rho
appBOmegaSigma bs s (QuantifiedSg psi sg) = case psi of
  FatArrow q
    -> FatArrowS (handleForallInQ $ appBOmegaTau bs s q)
                 (appBOmegaSigma bs s sg)
  Specified (a, ka)
    -> SpecifiedS (a, appBOmegaSigma bs s ka)
                  (appBOmegaSigma (addToUM' bs a) s sg)
  Inferred (a, ka)
    -> InferredS (a, appBOmegaSigma bs s ka)
                 (appBOmegaSigma (addToUM' bs a) s sg)

appLOmegaRho :: UniqSet TyVar -> LOmega -> Rho -> Rho
appLOmegaRho bs s (TauR t)
  = tauToRho $ appLOmegaTau bs s t
appLOmegaRho bs s (AppR sg1 sg2)
  = AppR (appLOmegaSigma bs s sg1) (appLOmegaSigma bs s sg2)
appLOmegaRho bs s (QuantifiedR psi sg) = case psi of
  ArrowR ph
    -> QuantifiedR (ArrowR (appLOmegaSigma bs s ph))
                   (appLOmegaSigma bs s sg)
  Required (a, ka)
    -> QuantifiedR (Required (a, appLOmegaSigma bs s ka))
                   (appLOmegaSigma (addToUM' bs a) s sg)

appLOmegaSigma :: UniqSet TyVar -> LOmega -> Sigma -> Sigma
appLOmegaSigma bs s (RhoSg rho) = rhoToSigma $ appLOmegaRho bs s rho
appLOmegaSigma bs s (QuantifiedSg psi sg) = case psi of
  FatArrow q
    -> FatArrowS (appLOmegaTau bs s q)
                 (appLOmegaSigma bs s sg)
  Specified (a, ka)
    -> SpecifiedS (a, appLOmegaSigma bs s ka)
                  (appLOmegaSigma (addToUM' bs a) s sg)
  Inferred (a, ka)
    -> InferredS (a, appLOmegaSigma bs s ka)
                 (appLOmegaSigma (addToUM' bs a) s sg)

instance Substitutable Tau where
  appBTheta s (ArrowT t1 t2) = ArrowS (appBTheta s t1) (appBTheta s t2)
  appBTheta s (AppT t1 t2)   = AppS (appBTheta s t1) (appBTheta s t2)
  appBTheta _ t@(AlphaT _)   = tauToSigma t
  appBTheta _ t@(TyConT _  ) = tauToSigma t
  appBTheta s t@(TcTyVarT tctv)
    | MuTcTv mv <- tctv
    = case lookupSubst s mv of
        Nothing  -> tauToSigma t
        Just tau -> tau
    | otherwise
    = tauToSigma t

  appLTheta s (ArrowT t1 t2) = ArrowT (appLTheta s t1) (appLTheta s t2)
  appLTheta s (AppT t1 t2)   = AppT (appLTheta s t1) (appLTheta s t2)
  appLTheta _ t@(AlphaT _)   = t
  appLTheta _ t@(TyConT _  ) = t
  appLTheta s t@(TcTyVarT tctv)
    | MuTcTv mv <- tctv
    = case lookupSubst s mv of
        Nothing  -> t
        Just tau -> tau
    | otherwise
    = t

  appBOmega = appBOmegaTau emptyUM
  appLOmega = appLOmegaTau emptyUM

  appBOmegaSk s (ArrowT t1 t2) = ArrowS (appBOmegaSk s t1) (appBOmegaSk s t2)
  appBOmegaSk s (AppT t1 t2)   = AppS (appBOmegaSk s t1) (appBOmegaSk s t2)
  appBOmegaSk _ t@(AlphaT _)   = tauToSigma t
  appBOmegaSk _ t@(TyConT _  ) = tauToSigma t
  appBOmegaSk s t@(TcTyVarT tctv)
    | SkolTcTv sk <- tctv
    = case lookupSubst s sk of
        Nothing  -> tauToSigma t
        Just sigma -> sigma
    | otherwise
    = tauToSigma t

  appLOmegaSk s (ArrowT t1 t2) = ArrowT (appLOmegaSk s t1) (appLOmegaSk s t2)
  appLOmegaSk s (AppT t1 t2)   = AppT (appLOmegaSk s t1) (appLOmegaSk s t2)
  appLOmegaSk _ t@(AlphaT _)   = t
  appLOmegaSk _ t@(TyConT _  ) = t
  appLOmegaSk s t@(TcTyVarT tctv)
    | SkolTcTv sk <- tctv
    = case lookupSubst s sk of
        Nothing  -> t
        Just tau -> tau
    | otherwise
    = t

instance Substitutable Rho where
  appBOmega = appBOmegaRho emptyUM

  appLOmega = appLOmegaRho emptyUM

  appBTheta s (TauR t)       = appBTheta s t
  appBTheta s (AppR sg1 sg2) = AppS (appBTheta s sg1) (appBTheta s sg2)
  appBTheta s (QuantifiedR psi sg) = case psi of
    ArrowR ph -> ArrowS (appBTheta s ph) (appBTheta s sg)
    Required (a, ka) -> RequiredS (a, appBTheta s ka) (appBTheta s sg)

  appLTheta s (TauR t) = TauR (appLTheta s t)
  appLTheta s (AppR sg1 sg2) = AppR (appLTheta s sg1) (appLTheta s sg2)
  appLTheta s (QuantifiedR psi sg) = case psi of
    ArrowR ph -> QuantifiedR (ArrowR (appLTheta s ph)) (appLTheta s sg)
    Required (a, ka) -> QuantifiedR (Required (a, appLTheta s ka)) (appLTheta s sg)

  appBOmegaSk s (TauR t)       = appBOmegaSk s t
  appBOmegaSk s (AppR sg1 sg2) = AppS (appBOmegaSk s sg1) (appBOmegaSk s sg2)
  appBOmegaSk s (QuantifiedR psi sg) = case psi of
    ArrowR ph -> ArrowS (appBOmegaSk s ph) (appBOmegaSk s sg)
    Required (a, ka) -> RequiredS (a, appBOmegaSk s ka) (appBOmegaSk s sg)

  appLOmegaSk s (TauR t) = TauR (appLOmegaSk s t)
  appLOmegaSk s (AppR sg1 sg2) = AppR (appLOmegaSk s sg1) (appLOmegaSk s sg2)
  appLOmegaSk s (QuantifiedR psi sg) = case psi of
    ArrowR ph -> QuantifiedR (ArrowR (appLOmegaSk s ph)) (appLOmegaSk s sg)
    Required (a, ka) -> QuantifiedR (Required (a, appLOmegaSk s ka)) (appLOmegaSk s sg)

instance Substitutable Sigma where
  appBOmega = appBOmegaSigma emptyUM
  appLOmega = appLOmegaSigma emptyUM

  appBTheta s (RhoSg rho) = appBTheta s rho
  appBTheta s (QuantifiedSg psi sg) = case psi of
    FatArrow q
      -> FatArrowS (handleForallInQ $ appBTheta s q)
                   (appBTheta s sg)
    Specified (a, ka)
      -> SpecifiedS (a, appBTheta s ka) (appBTheta s sg)
    Inferred (a, ka)
      -> InferredS (a, appBTheta s ka) (appBTheta s sg)


  appLTheta s (RhoSg rho) = rhoToSigma $ appLTheta s rho
  appLTheta s (QuantifiedSg psi sg) = case psi of
    FatArrow q
      -> FatArrowS (appLTheta s q) (appLTheta s sg)
    Specified (a, ka)
      -> SpecifiedS (a, appLTheta s ka) (appLTheta s sg)
    Inferred (a, ka)
      -> InferredS (a, appLTheta s ka) (appLTheta s sg)


  appBOmegaSk s (RhoSg rho) = appBOmegaSk s rho
  appBOmegaSk s (QuantifiedSg psi sg) = case psi of
    FatArrow q
      -> FatArrowS (handleForallInQ $ appBOmegaSk s q)
                   (appBOmegaSk s sg)
    Specified (a, ka)
      -> SpecifiedS (a, appBOmegaSk s ka) (appBOmegaSk s sg)
    Inferred (a, ka)
      -> InferredS (a, appBOmegaSk s ka) (appBOmegaSk s sg)


  appLOmegaSk s (RhoSg rho) = rhoToSigma $ appLOmegaSk s rho
  appLOmegaSk s (QuantifiedSg psi sg) = case psi of
    FatArrow q
      -> FatArrowS (appLOmegaSk s q) (appLOmegaSk s sg)
    Specified (a, ka)
      -> SpecifiedS (a, appLOmegaSk s ka) (appLOmegaSk s sg)
    Inferred (a, ka)
      -> InferredS (a, appLOmegaSk s ka) (appLOmegaSk s sg)

appLThetaPsi :: LTheta -> Psi -> Psi
appLThetaPsi s (PsiR psi) = PsiR $ case psi of
  ArrowR sg        -> ArrowR (appLTheta s sg)
  Required (a, ka) -> Required (a, appLTheta s ka)
appLThetaPsi s (PsiSg psi) = PsiSg $ case psi of
  FatArrow q        -> FatArrow (appLTheta s q)
  Specified (a, ka) -> Specified (a, appLTheta s ka)
  Inferred (a, ka)  -> Inferred (a, appLTheta s ka)
appLThetaPsi s (PsiT tau) = PsiT (appLTheta s tau)

appBThetaPsi :: BTheta -> Psi -> Psi
appBThetaPsi s (PsiR psi) = PsiR $ case psi of
  ArrowR sg        -> ArrowR (appBTheta s sg)
  Required (a, ka) -> Required (a, appBTheta s ka)
appBThetaPsi s (PsiSg psi) = PsiSg $ case psi of
  FatArrow q        -> FatArrow (handleForallInQ $ appBTheta s q)
  Specified (a, ka) -> Specified (a, appBTheta s ka)
  Inferred (a, ka)  -> Inferred (a, appBTheta s ka)
appBThetaPsi s (PsiT tau) = case appBTheta s tau of
  RhoSg (TauR t)  -> PsiT t
  sg              -> PsiR (ArrowR sg)

appLOmegaPsi :: LOmega -> Psi -> Psi
appLOmegaPsi s (PsiR psi) = PsiR $ case psi of
  ArrowR sg        -> ArrowR (appLOmega s sg)
  Required (a, ka) -> Required (a, appLOmega s ka)
appLOmegaPsi s (PsiSg psi) = PsiSg $ case psi of
  FatArrow q        -> FatArrow (appLOmega s q)
  Specified (a, ka) -> Specified (a, appLOmega s ka)
  Inferred (a, ka)  -> Inferred (a, appLOmega s ka)
appLOmegaPsi s (PsiT tau) = PsiT (appLOmega s tau)

appBOmegaPsi :: BOmega -> Psi -> Psi
appBOmegaPsi s (PsiR psi) = PsiR $ case psi of
  ArrowR sg        -> ArrowR (appBOmega s sg)
  Required (a, ka) -> Required (a, appBOmega s ka)
appBOmegaPsi s (PsiSg psi) = PsiSg $ case psi of
  FatArrow q        -> FatArrow (handleForallInQ $ appBOmega s q)
  Specified (a, ka) -> Specified (a, appBOmega s ka)
  Inferred (a, ka)  -> Inferred (a, appBOmega s ka)
appBOmegaPsi s (PsiT tau) = case appBOmega s tau of
  RhoSg (TauR t)  -> PsiT t
  sg              -> PsiR (ArrowR sg)

appLOmegaSkPsi :: LOmegaSk -> Psi -> Psi
appLOmegaSkPsi s (PsiR psi) = PsiR $ case psi of
  ArrowR sg        -> ArrowR (appLOmegaSk s sg)
  Required (a, ka) -> Required (a, appLOmegaSk s ka)
appLOmegaSkPsi s (PsiSg psi) = PsiSg $ case psi of
  FatArrow q        -> FatArrow (appLOmegaSk s q)
  Specified (a, ka) -> Specified (a, appLOmegaSk s ka)
  Inferred (a, ka)  -> Inferred (a, appLOmegaSk s ka)
appLOmegaSkPsi s (PsiT tau) = PsiT (appLOmegaSk s tau)

appBOmegaSkPsi :: BOmegaSk -> Psi -> Psi
appBOmegaSkPsi s (PsiR psi) = PsiR $ case psi of
  ArrowR sg        -> ArrowR (appBOmegaSk s sg)
  Required (a, ka) -> Required (a, appBOmegaSk s ka)
appBOmegaSkPsi s (PsiSg psi) = PsiSg $ case psi of
  FatArrow q        -> FatArrow (handleForallInQ $ appBOmegaSk s q)
  Specified (a, ka) -> Specified (a, appBOmegaSk s ka)
  Inferred (a, ka)  -> Inferred (a, appBOmegaSk s ka)
appBOmegaSkPsi s (PsiT tau) = case appBOmegaSk s tau of
  RhoSg (TauR t)  -> PsiT t
  sg              -> PsiR (ArrowR sg)

{- *********************************************************************
*                                                                      *
                  Pretty printing functions
*                                                                      *
********************************************************************* -}

pprTyVar :: forall a. TyVar -> Doc a
pprTyVar (MkTv { tyVarText = t , tyVarUnique = i })
  = pretty t <> pprUnique i

pprTyCon :: forall a. TyCon -> Doc a
pprTyCon (MkTyCon t) = pretty t

pprArrow :: forall a. Doc a
pprArrow = pretty "->"

pprFatArrow :: forall a. Doc a
pprFatArrow = pretty "=>"

pprForall :: forall a. Doc a
pprForall = pretty "forall"

pprTilde :: forall a. Doc a
pprTilde = pretty "~"

pprBinding :: forall a. (TyVar, Kappa) -> Doc a
pprBinding (a, ka) = pprTyVar a <> pretty ':' <> pprSigma ka

pprDot :: forall a. Doc a
pprDot = pretty '.'

pprTidiedSigma :: forall a. Sigma -> Doc a
pprTidiedSigma = pprSigma . tidySigma

pprTidiedSigmas :: forall a. [Sigma] -> Doc a
pprTidiedSigmas = pprList (hsep . map pprTidiedSigma)

pprSigmas :: forall a. [Sigma] -> Doc a
pprSigmas = pprList (hsep . map pprSigma)

pprRhos :: forall a. [Rho] -> Doc a
pprRhos = pprList (hsep . map pprRho)

pprTaus :: forall a. [Tau] -> Doc a
pprTaus = pprList (hsep . map pprTau)

pprSigma :: forall a. Sigma -> Doc a
pprSigma (QuantifiedSg psi sg) = pprPsiS psi <+> pprSigma sg
pprSigma (RhoSg rho)           = pprRho rho

pprRho :: forall a. Rho -> Doc a
pprRho (TauR tau) = pprTau tau
pprRho (QuantifiedR psi sigma) = pprPsiR psi    <+> pprSigma sigma
pprRho (AppR sigma phi)        = parenIfNeededS sigma <+> parenIfNeededS phi

pprTau :: forall a. Tau -> Doc a
pprTau (ArrowT t1 t2) = parenIfNeededT t1 <+> pprArrow <+> pprTau t2
pprTau t@(AppT sg ph) -- TODO: This will break down for stuff like (Maybe (Either Int Bool)))
  | Just (lhs, rhs) <- splitEqPredT t
  = parenIfNeededT lhs <+> pprTilde <+> parenIfNeededT rhs
  | otherwise
  = parenIfNeededT sg <+> parenIfNeededT ph
pprTau (AlphaT sk)   = pprTyVar sk
pprTau (TcTyVarT sk) = pprTcTyVar sk
pprTau (TyConT tc)   = pprTyCon tc

parenIfNeededS :: forall a. Sigma -> Doc a
parenIfNeededS ph
  | needs ph  = parens (pprSigma ph)
  | otherwise = pprSigma ph
  where
    needs (RhoSg (QuantifiedR psi sg'))
      | ArrowR _ <- psi = True
      | otherwise       = needs sg'
    needs _ = False

parenIfNeededT :: forall a. Tau -> Doc a
parenIfNeededT tau
  | needs tau = parens (pprTau tau)
  | otherwise = pprTau tau
  where
    needs (ArrowT _ _) = True
    needs _            = False

-- Note [Tidying before printing]
tidySigma :: Sigma -> Sigma
tidySigma sigma = go_sigma sigma
  where
    go_sigma (QuantifiedSg psi sg) = case psi of
      Specified b -> QuantifiedSg (Specified (handleQ b)) (go_sigma sg)
      Inferred b  -> QuantifiedSg (Inferred (handleQ b)) (go_sigma sg)
      FatArrow q  -> QuantifiedSg (FatArrow (go_tau q)) (go_sigma sg)
    go_sigma (RhoSg rho) = RhoSg (go_rho rho)

    go_rho (QuantifiedR psi sg) = case psi of
      Required b ->  QuantifiedR (Required (handleQ b)) (go_sigma sg)
      ArrowR ph   -> QuantifiedR (ArrowR (go_sigma ph)) (go_sigma sg)
    go_rho (TauR t)       = TauR (go_tau t)
    go_rho (AppR sg1 sg2) = AppR (go_sigma sg1) (go_sigma sg2)

    go_tau (AppT t1 t2)   = AppT (go_tau t1) (go_tau t2)
    go_tau (ArrowT t1 t2) = ArrowT (go_tau t1) (go_tau t2)
    go_tau (AlphaT tv)    = AlphaT (handleV tv)
    go_tau t              = t

    handleQ (a, k) = (handleV a, go_sigma k)
    handleV :: TyVar -> TyVar
    handleV v
      | Just (_,var') <- find (\(var,_) -> var == v) newBinders
      = var'
      | otherwise
      = v
    newBinders = zip (reverse (split [] sigma)) freshTyVars

    freshTyVars = [ MkTv { tyVarText = pack [v], tyVarUnique = mkUnique i }
                  | i <- [1 ..]
                  , v <- ['a' .. 'z']]
    saveKVs (AlphaS v) bs = if v `elem` bs then bs else v : bs
    saveKVs _ bs         = bs

    split bs (RequiredS (alpha, ka) sg)  = split (saveKVs ka (alpha : bs)) sg
    split bs (SpecifiedS (alpha, ka) sg) = split (saveKVs ka (alpha : bs)) sg
    split bs (InferredS (alpha, ka) sg)  = split (saveKVs ka (alpha : bs)) sg
    split bs (ArrowS ph sg)              = split (bs ++ split bs ph) sg
    split bs (FatArrowS q sg)            = split (split_tau bs q) sg
    split bs _                           = bs

    split_tau bs (ArrowT t1 t2) = split_tau (split_tau bs t1) t2
    split_tau bs _ = bs

pprPsi :: Psi -> Doc b
pprPsi (PsiR psi)  = pprPsiR psi
pprPsi (PsiSg psi) = pprPsiS psi
pprPsi (PsiT t)    = parenIfNeededT t <+> pprArrow

pprPsiS :: PsiSg -> Doc b
pprPsiS (Inferred b)  = pprForall <+> braces (pprBinding b) <> pprDot
pprPsiS (Specified b) = pprForall <+> pprBinding b <> pprDot
pprPsiS (FatArrow q)  = pprTau q <+> pprFatArrow

pprPsiR :: PsiR -> Doc b
pprPsiR (Required b)  = pprForall <+> pprBinding b <+> pprArrow
pprPsiR (ArrowR sg)   = parenIfNeededS sg <+> pprArrow

pprPsis :: forall b. [Psi] -> Doc b
pprPsis = pprList (hsep . map pprPsi)

pprTcTyVar :: forall a. TcTyVar -> Doc a
pprTcTyVar (MetaTcTv mv) = pprMetaVar mv
pprTcTyVar (SkolTcTv sk) = pprSkol sk
pprTcTyVar (MuTcTv mu)   = pprMuVar mu

pprMetaDetails :: forall a. MetaDetails -> Doc a
pprMetaDetails (Indirect tau) = pretty "Indireict:" <+> pprTau tau
pprMetaDetails Flexi          = pretty "Flexi:"

pprMetaVar :: forall a. MetaTyVar -> Doc a
pprMetaVar (MkMetaTv { metaVarText = text
                     , metaVarUnique = uniq
                     , metaVarLvl = lvl
                     , metaVarKind = ka })
  = pretty text <> pprUnique uniq <>
    brackets (pprTcLevel lvl) <>
    pretty ':' <> pprSigma ka

pprMetaVars :: forall a. [MetaTyVar] -> Doc a
pprMetaVars = pprList (hsep . map pprMetaVar)

pprMuVar :: forall a. MuTyVar -> Doc a
pprMuVar (MkMuTv { muVarText   = text
                 , muVarUnique = uniq
                 , muVarKind   = ka })
  = pretty text <> pprUnique uniq <>
    pretty ':' <> pprSigma ka

pprSkol :: forall a. SkolTyVar -> Doc a
pprSkol (MkSkolTv { skolTyVar = sk
                  , skolLvl   = lvl })
  = pretty "sk" <>
    pprUnique (tyVarUnique sk) <>
    brackets (pprTcLevel lvl)

pprSkols :: forall a. [SkolTyVar] -> Doc a
pprSkols = pprList (hsep . map pprSkol)

pprSkolKind :: forall a. (SkolTyVar,Kappa) -> Doc a
pprSkolKind (sk, ka) = pprSkol sk <+> pretty ':' <+> pprSigma ka

pprSkolKinds :: forall a. [(SkolTyVar, Kappa)] -> Doc a
pprSkolKinds = pprList (hsep . map pprSkolKind)
