-- |
module Rep where

import Type
import Data.Text (pack, Text)

-- TODO: Write unit tests for the following

fooVar = MkTv $ pack "foo"

idType = Quantified (Specified (fooVar, tyConType)) (Quantified (Arrow (Alpha fooVar)) (Alpha fooVar))

idTypeToIdType = (Quantified (Arrow idType) idType)

intToIntArg = Quantified (Arrow (Quantified (Arrow tyConInt) tyConInt)) tyConBool
