notMaybeBool (Just x) = not x
notMaybeBool Nothing  = False
