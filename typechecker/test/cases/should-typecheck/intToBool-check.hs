intToBool :: Int -> Bool
intToBool 1 = True
intToBool 0 = False
intToBool _ = False
