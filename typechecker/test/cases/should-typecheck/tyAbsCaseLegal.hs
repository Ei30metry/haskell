tyAbsCaseLegal :: forall a. (a -> a) -> Int
tyAbsCaseLegal @b g = case g of
  x -> let y = 0
           z :: b -> b -- b is in scope
           z = x in y
