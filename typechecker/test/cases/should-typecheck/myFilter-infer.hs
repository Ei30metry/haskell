myFilter p Nil         = Nil
myFilter p (Cons x xs) = if p x
                         then Cons x (myFilter p xs)
                         else myFilter p xs
