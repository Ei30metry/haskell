myMap f (Cons x xs) = Cons (f x) (myMap f xs)
myMap _ Nil         = Nil
