break _ xs@Nil          =  (xs, xs)
break p xs@(Cons x xs')
  = if p x
    then (Nil,xs)
    else let (ys,zs) = break p xs' in (Cons x ys,zs)
