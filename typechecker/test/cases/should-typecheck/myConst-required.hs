myConst :: forall a -> forall b -> a -> b -> a
myConst p q x y = y :: q

myConstInt = myConst Int

myConstIntBool = myConst Int Bool
