someClosedSomeNot x       -- Gen
  = let
      f1 = x . x          -- NoGen
      f2 = id             -- Gen
      f3 = const f2       -- Gen
    in f1
