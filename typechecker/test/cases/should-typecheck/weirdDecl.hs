weirdDecl :: forall a. a -> a ~ Int => a -- The goal is to reject this,
weirdDecl @Int x = x                     -- but for now, we need to accept it
