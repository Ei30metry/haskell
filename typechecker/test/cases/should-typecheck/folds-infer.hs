myFoldr _ e Nil         = e
myFoldr f e (Cons x xs) = f x (myFoldr f e xs)

myFoldl _ e Nil         = e
myFoldl f e (Cons x xs) = myFoldl f (f e x) xs
