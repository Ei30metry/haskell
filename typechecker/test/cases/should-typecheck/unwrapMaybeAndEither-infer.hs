unwrapEitherIntBool (Right r) = Right (not r)
unwrapEitherIntBool (Left 1)  = Right True
unwrapEitherIntBool (Left 0)  = Right False
unwrapEitherIntBool (Left x)  = Left (plus x 2)

unwrapEitherIntBoolCase x
  = case x of
      Right r -> Right (not r)
      Left 1  -> Right True
      Left 0  -> Right False
      Left x  -> Left (plus x 2)

unwrapMaybeBool (Just True)  = False
unwrapMaybeBool (Just False) = True
unwrapMaybeBool Nothing      = False

idEither (Right x) = Right x
idEither (Left y)  = Left y
