addMaybeInt
  = \cases
        Nothing x         -> x
        x Nothing         -> x
        (Just x) (Just y) -> Just (x + y)

addJustInts = \(Just x) (Just y) -> Just (x + y)

addJustIntsCases = \cases (Just x) (Just y) -> Just (x + y)
