x :: forall a. a -> a
y :: Double
z :: Bool -> Either Bool Int
Just (x, y, z)
  = pure (fp 1, gp 2, hp 3)
  where
    fp _ = id                       -- Gen
    gp _ = 3                        -- Gen
    hp _ = Left                     -- Gen
