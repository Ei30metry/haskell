absMaybeMaybe :: Maybe (Maybe Int) -> Maybe Int
absMaybeMaybe (Just @(p q) _) = (Just 1) :: (p q)
absMaybeMaybe Nothing         = Nothing
