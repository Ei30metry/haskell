rankNArgAPats :: (forall a. Bool -> a -> a) -> Bool
rankNArgAPats f = f False True

res = rankNArgAPats (\cases
                         @a True -> \(x :: a) -> x
                         @b False -> \y       -> (y :: b))
