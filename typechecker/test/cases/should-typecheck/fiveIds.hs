fiveIds :: [forall a. a -> a]
fiveIds = replicate 5 id
