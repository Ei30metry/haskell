closed          -- Gen
  = let
      f = id    -- Gen (closed)
      g = f     -- Gen (closed)
    in g
