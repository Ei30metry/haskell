absMaybe :: Maybe Int -> Int
absMaybe (Just @a x) = x :: a
absMaybe Nothing     = 0
