silly1 :: forall a. Show a => a -> Int
silly1 (show -> "Matched") = 4
silly1 _                   = 0
