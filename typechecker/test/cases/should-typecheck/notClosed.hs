notClosed x =    -- Gen
  let
    f = seq x id -- NoGen (mentions x)
    g = f        -- NoGen (mentions f, which itself isn't closed)
  in g
