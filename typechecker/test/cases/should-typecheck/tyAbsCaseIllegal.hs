tyAbsCaseIllegal :: forall a. (a -> a) -> Int
tyAbsCaseIllegal g = case g of                 -- g has type 'a'
  @b x -> let y = 0 -- There is no correcsponding invisibly-specified quantifee to bound b to!
              z :: b -> b               -- 'b' is not in scope
              z = x in y
