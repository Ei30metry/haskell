myId x = x

myIdLet = let id x = x in id

myIdLambda = \x -> x

myIdBind = id
