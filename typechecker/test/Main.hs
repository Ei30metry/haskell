module Main (main) where

import Test.Tasty
import Test.Tasty.Golden
import System.FilePath
import System.Directory
import Data.ByteString.Lazy (ByteString)
import Data.ByteString.Builder (toLazyByteString, string7)
import Tc
import Parser

-- Change this to False if you want to disable tracing
traceFlag :: Bool
traceFlag = True

toOutputFile :: FilePath -> FilePath
toOutputFile = flip replaceExtension "output"

toTraceFile :: FilePath -> FilePath
toTraceFile = flip replaceExtension "trace"

tracesDir :: FilePath
tracesDir = "traces"

shouldFailDir :: FilePath
shouldFailDir = "should-fail"

shouldTypeCheckDir :: FilePath
shouldTypeCheckDir = "should-typecheck"

testParserDir :: FilePath
testParserDir = "parser"

testParserPath :: FilePath
testParserPath = "." </> "test" </> "cases" </> testParserDir

shouldFailPath :: FilePath
shouldFailPath = "." </> "test" </> "cases" </> shouldFailDir

shouldTypeCheckPath :: FilePath
shouldTypeCheckPath = "." </> "test" </> "cases" </> shouldTypeCheckDir

renewTraceDirs :: [FilePath] -> IO ()
renewTraceDirs dirs = do
  mapM_ (\d -> do exists <- doesDirectoryExist d
                  if exists
                    then removeDirectoryRecursive d >> createDirectory d
                    else createDirectory d) dirs

shouldTypeCheck :: IO TestTree
shouldTypeCheck = do
  inputFiles <- findByExtension [".hs"] shouldTypeCheckPath
  return $ testGroup "Should typecheck"
    [ goldenVsString (takeBaseName actFile)
                     outputFile
                     (typeCheckFile tracePath actFile)
    | actFile <- inputFiles
    , let outputFile = toOutputFile actFile
    , let tracePath  = if traceFlag
                         then Just $ toTraceFile ("." </> tracesDir </>
                                                shouldTypeCheckDir </>
                                                takeBaseName actFile)
                         else Nothing ]

shouldFail :: IO TestTree
shouldFail = do
  inputFiles <- findByExtension [".hs"] shouldFailPath
  return $ testGroup "Should fail"
    [ goldenVsString (takeBaseName actFile)
                     outputFile
                     (typeCheckFile tracePath actFile)
    | actFile <- inputFiles
    , let outputFile = toOutputFile actFile
    , let tracePath  = if traceFlag
                         then Just $ toTraceFile ("." </> tracesDir </>
                                                 shouldFailDir </>
                                                 takeBaseName actFile)
                         else Nothing ]

testParser :: IO TestTree
testParser = do
  inputFiles <- findByExtension [".hs"] testParserPath
  return $ testGroup "Should get parsed"
    [ goldenVsString (takeBaseName actFile)
                     outputFile
                     (parseTranslateBS actFile)
    | actFile <- inputFiles
    , let outputFile = toOutputFile actFile ]

parseTranslateBS :: FilePath -> IO ByteString
parseTranslateBS fp = do
  parsed <- parseTranslate fp
  case parsed of
    Left err -> pure . toLazyByteString . string7 $ err
    Right vds -> pure . toLazyByteString . string7 . show $ vds

main :: IO ()
main = do
  let traces = "." </> tracesDir
      typecheck = ("." </> tracesDir </> shouldTypeCheckDir)
      fail = ("." </> tracesDir </> shouldFailDir)
  renewTraceDirs [traces, typecheck, fail]
  p <- testParser
  a <- shouldTypeCheck
  f <- shouldFail
  defaultMain (testGroup "Tests" [p,a,f])
