Typing rules for Haskell
========================

In the course of writing GHC, it frequently comes to pass that we
need proper typing rules for a snippet of Haskell. These will be
written here, using [ott](https://www.cl.cam.ac.uk/~pes20/ott/).

You can access a [built version of this PDF](https://gitlab.haskell.org/Ei30metry/haskell/-/jobs/artifacts/wip/term/raw/haskell.pdf?job=build-pdf),
though that link only works roughly 30 days from the last push (it points to a CI artifact
that gets cleared out).

## Disclaimer
The paper is still very much a **draft**. The text is all over the place and some of the typing rules need refinment.

## Building from source
You can either build the `ottall.pdf` file or `haskell.pdf`, which is meant for the readers of the specification to view.

The role of the `ottall` file is to help debug parse errors and show the final result of the rules and grammars. You can build it using `make ott`. For the haskell build,`make` will do.
